<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('orgPrimaryKey')->nullable();
            $table->string('orgCode');
            $table->string('orgName')->nullable();
            $table->boolean('orgIsActive')->default(1);

            $table->boolean('orgIsConsignee')->default(false);
            $table->boolean('orgIsConsignor')->default(false);
            $table->boolean('orgIsForwarder')->default(false);
            $table->boolean('orgIsAgentWise')->default(false);

            $table->string('orgAddressLine1')->nullable();
            $table->string('orgAddressLine2')->nullable();

            $table->string('orgCity')->nullable();
            $table->string('orgPostalCode')->nullable();
            $table->string('orgUnloco')->nullable();
            $table->string('orgState')->nullable();
            $table->string('orgPhone')->nullable();
            $table->string('orgMobile')->nullable();
            $table->string('orgFax')->nullable();
            $table->string('orgEmail')->nullable();
            $table->string('orgWeb')->nullable();
            $table->string('orgLanguage')->nullable();
            $table->string('orgCountryCode')->nullable();
            $table->string('orgCountryName')->nullable();
            $table->string('orgRegistrationNumber')->nullable();

            $table->string('orgSalePerson')->nullable();
            $table->string('orgSalePersonEmail')->nullable();
            $table->string('orgSalePersonMobile')->nullable();

            $table->string('orgAccountPerson')->nullable();
            $table->string('orgAccountEmail')->nullable();
            $table->string('orgAccountMobile')->nullable();

            $table->string('orgLogo')->nullable();

            $table->dateTime('orgLastEditDateTimeUTC')->nullable();

            $table->boolean('orgCustomerFlag')->default(0);
            $table->boolean('orgEflFlag')->default(0);
            $table->boolean('orgCwFlag')->default(0);
            $table->boolean('orgStatus')->default(3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
