<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingShippingsSyncsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_shippings_syncs', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('ItemInternalId')->nullable();
            $table->string('JS_UniqueConsignRef')->nullable();
            $table->boolean('JS_IsForwardRegistered');
            $table->double('BookedPallets', '20', '4')->nullable();
            $table->double('BookedWeight', '20', '4')->nullable();
            $table->string('JS_TransportMode')->nullable();
            $table->string('JS_UnitOfWeight')->nullable();
            $table->string('JS_UnitOfVolume')->nullable();
            $table->string('JS_UnitOfActualChargeable')->nullable();
            $table->string('JS_F3_NKPackType')->nullable();
            /***
             * explanation of enum values
             * LSE - Air freight
             * ULD - Air Freight
             * FCL - Sea Freight
             * LCL - Sea freight
             * LRO - Road Freight
             * FRO - Road Freight
             * FTL - Road Freight
             * COU - Courier
             * LRA - Rail Freight
             * FRA - Rail Freight
             ***/
            $table->string('JS_RS_NKServiceLevel')->nullable();

            // $table->enum('ContainerDelivery', ['CFS/CFS', 'CY/CY', 'CY/CFS', 'CFS/CY', 'DO/DO', 'CY/DO', 'DO/CY'])->nullable();

            $table->string('LoadPort')->nullable();

            $table->string('DischargePort')->nullable();

            $table->dateTime('JS_SystemLastEditTimeUtc')->nullable();

            $table->string('JS_INCO')->nullable();

            /***
             * explanation of enum values
             * CFR - Cost and Freight
             * CIF - Cost,Insurance and Freight
             * CIP - Carriage and Insurance Paid To
             * CPT - Carriage Paid To
             * DAF - Delivered At Frontier
             * DAP - Delivered At Place
             * DAT - Delivered At Terminal
             * DDP - Delivered Duty Paid
             * DDU - Delivered Duty Unpaid
             * DEQ - Delivered Ex Quay
             * DES - Delivered Ex Ship
             * EXW - Ex Works
             * FAS - Free Alongside Ship
             * FCA - Free Carrier
             * FOB - Free On Board
             ***/

            // $table->enum('JS_PackingMode', ['LSE', 'ULD', 'CON', 'BCN', 'FCL', 'LCL', 'BLK', 'LQD', 'BBK', 'ROR'])->nullable();

            $table->string('JS_PackingMode')->nullable();
            $table->string('JS_ShipmentType')->nullable();
            $table->string('Carrier_CODE')->nullable();
            /***
             * explanation of enum values
             * LSE - Loose
             * ULD - Unit Load Device
             * CON - Agent Consolidation
             * BCN - Buyer's Consolidation
             * FCL - Full Container Load
             * LCL - Less Container Load
             * BLK - Bulk
             * BLK - Bulk
             * BBK - Break Bulk
             * BCN - Buyer's Consolidation
             * ROR - Roll On/Roll Off
             ***/

            $table->string('GoodsDescription')->nullable();
            $table->string('Voyage_Flight')->nullable();

            $table->string('vessels')->nullable();

            $table->dateTime('ETD')->nullable();
            $table->dateTime('ETA')->nullable();

            $table->string('ExpoRepresentative')->nullable();

            $table->string('ShipperRepresentative')->nullable();

            $table->string('JS_RL_NKOrigin')->nullable();

            $table->string('JS_RL_NKDestination')->nullable();

            // $table->double('TotalWeight', '20', '4')->nullable();
            $table->double('JS_ActualChargeable', '20', '4')->nullable();
            // $table->double('TotalPallets', '20', '4')->nullable();
            $table->double('BookedTEU', '20', '4')->nullable();

            $table->string('ShippersRef')->nullable();
            $table->string('VehicleNo')->nullable();
            $table->string('DriverName')->nullable();

            $table->dateTime('Received_Date')->nullable();
            $table->dateTime('CustomsCheck')->nullable();
            $table->text('MarksNumber')->nullable();

            $table->string('Remarks')->nullable();
            $table->string('JD_OrderNumber')->nullable();
            $table->string('JK_MasterBillNum')->nullable();

            // $table->string('carriers')->nullable();

            $table->string('ConsignorCode');

            $table->string('LocalClient');

            // $table->dateTime('JS_SystemCreateTimeUtc');

            $table->string('ConsigneeCode');

            $table->dateTime('ATD')->nullable();
            $table->dateTime('ATA')->nullable();

            $table->string('ConfirmatAirline')->nullable();
            $table->double('JS_ActualWeight', '20', '4')->nullable();
            $table->double('JS_ActualVolume', '20', '4')->nullable();
            $table->double('JS_OuterPacks', '20', '4')->nullable();

            $table->string('HAWB')->nullable();
            $table->boolean('is_active')->default(true);
            $table->dateTime('JS_SystemCreateTimeUtc')->nullable();
            $table->string('CarrierName')->nullable();
            $table->string('SendAgentCode')->nullable();
            $table->string('SendAgentName')->nullable();
            $table->string('RecvAgentCode')->nullable();
            $table->string('RecvAgentName')->nullable();
            $table->dateTime('ActualDeliverydate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_shippings_syncs');
    }
}
