<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_events', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('shippingsId');
            $table->foreign('shippingsId')->references('id')->on('shipments');

            $table->string('evenType')->nullable();
            $table->dateTime('eventDate')->nullable();
            $table->string('eventDetail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_events');
    }
}
