<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadXmlFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('read_xml_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fileName')->unique();
            $table->tinyInteger('readStatus')->comment('0-Failed, 1-Half-Read, 2-Success');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('read_xml_files');
    }
}
