<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_events', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('bookingShippingsId');
            $table->foreign('bookingShippingsId')->references('id')->on('booking_shippings_syncs');

            $table->string('evenType')->nullable();
            $table->dateTime('eventDate')->nullable();
            $table->string('eventDetail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_events');
    }
}
