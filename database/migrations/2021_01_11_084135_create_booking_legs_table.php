<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingLegsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_legs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('bookingShippingsId');
            $table->foreign('bookingShippingsId')->references('id')->on('booking_shippings_syncs');

            $table->string('Leg_Order1')->nullable();
            $table->string('VoyageFlight1')->nullable();
            $table->string('Vessel1')->nullable();
            $table->dateTime('legETD1')->nullable();
            $table->dateTime('legETA1')->nullable();
            $table->dateTime('legATD1')->nullable();
            $table->dateTime('legATA1')->nullable();
            $table->string('leg1load')->nullable();
            $table->string('leg1disc')->nullable();
            $table->string('mode1')->nullable();

            $table->string('Leg_Order2')->nullable();
            $table->string('VoyageFlight2')->nullable();
            $table->string('Vessel2')->nullable();
            $table->dateTime('legETD2')->nullable();
            $table->dateTime('legETA2')->nullable();
            $table->dateTime('legATD2')->nullable();
            $table->dateTime('legATA2')->nullable();
            $table->string('leg2load')->nullable();
            $table->string('leg2disc')->nullable();
            $table->string('mode2')->nullable();

            $table->string('Leg_Order3')->nullable();
            $table->string('VoyageFlight3')->nullable();
            $table->string('Vessel3')->nullable();
            $table->dateTime('legETD3')->nullable();
            $table->dateTime('legETA3')->nullable();
            $table->dateTime('legATD3')->nullable();
            $table->dateTime('legATA3')->nullable();
            $table->string('leg3load')->nullable();
            $table->string('leg3disc')->nullable();
            $table->string('mode3')->nullable();

            $table->string('Leg_Order4')->nullable();
            $table->string('VoyageFlight4')->nullable();
            $table->string('Vessel4')->nullable();
            $table->dateTime('legETD4')->nullable();
            $table->dateTime('legETA4')->nullable();
            $table->dateTime('legATD4')->nullable();
            $table->dateTime('legATA4')->nullable();
            $table->string('leg4load')->nullable();
            $table->string('leg4disc')->nullable();
            $table->string('mode4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_legs');
    }
}
