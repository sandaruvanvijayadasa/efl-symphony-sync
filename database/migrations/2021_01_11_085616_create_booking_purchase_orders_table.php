<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('bookingShippingsId');
            $table->foreign('bookingShippingsId')->references('id')->on('booking_shippings_syncs');

            $table->string('orderNumber')->nullable();

            $table->string('buyerCode')->nullable();

            $table->string('supplierCode')->nullable();

            $table->string('confirmNumber')->nullable();
            $table->string('invoiceNumber')->nullable();

            $table->date('oderNumberDate')->nullable();
            $table->date('invoiceNumberDate')->nullable();

            $table->date('confirmNumberDate')->nullable();
            $table->date('followUpDate')->nullable();
            $table->date('requireInstoDate')->nullable();

            $table->string('orderStatus')->nullable();

            $table->date('requireExWorkDate')->nullable();
            $table->text('orderGoodDescription')->nullable();

            $table->string('currency')->nullable();

            $table->string('orderServiceLevel')->nullable();

            $table->string('orderINCOTerm')->nullable();

            $table->string('AdditionalTerms')->nullable();

            $table->string('OrderTransMode')->nullable();

            $table->string('OrderCountryOrigin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_purchase_orders');
    }
}
