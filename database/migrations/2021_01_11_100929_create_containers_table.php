<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('containers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('shippingsId');
            $table->foreign('shippingsId')->references('id')->on('shipments');

            $table->string('Containercount')->nullable();
            $table->string('Container_Line_No')->nullable();
            $table->string('ContainerNum')->nullable();
            $table->string('SealNum')->nullable();
            $table->string('ContainerMode')->nullable();
            $table->string('StorageClass')->nullable();
            $table->string('DeliveryMode')->nullable();
            $table->string('ContainerType')->nullable();
            $table->string('GrossWeight')->nullable();
            $table->string('TareWeight')->nullable();

            $table->dateTime('DepartureEstimatedPickup')->nullable();
            $table->dateTime('EmptyRequired')->nullable();
            $table->dateTime('ContainerYardEmptyPickupGateOut')->nullable();
            $table->dateTime('DepartureCartageAdvised')->nullable();
            $table->dateTime('DepartureCartageComplete')->nullable();
            $table->dateTime('ArrivalSlotDateTime')->nullable();
            $table->dateTime('ArrivalEstimatedDelivery')->nullable();
            $table->dateTime('ArrivalDeliveryRequiredBy')->nullable();
            $table->dateTime('ArrivalCartageAdvised')->nullable();
            $table->dateTime('ArrivalCartageComplete')->nullable();
            $table->dateTime('FCLWharfGateIn')->nullable();
            $table->dateTime('FCLOnBoardVessel')->nullable();
            $table->dateTime('FCLUnloadFromVessel')->nullable();
            $table->dateTime('FCLAvailable')->nullable();
            $table->dateTime('FCLWharfGateOut')->nullable();
            $table->dateTime('ArrivalCTOStorageStartDate')->nullable();
            $table->dateTime('LCLUnpack')->nullable();
            $table->dateTime('LCLAvailable')->nullable();
            $table->dateTime('EmptyReadyForReturn')->nullable();
            $table->dateTime('EmptyReturnedBy')->nullable();
            $table->dateTime('ContainerYardEmptyReturnGateIn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('containers');
    }
}
