<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_lines', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('poNumber');
            $table->foreign('poNumber')->references('id')->on('shipment_purchase_orders');

            $table->string('jlPk')->nullable();
            $table->string('lineNumber')->nullable();
            $table->text('lineDescription')->nullable();
            $table->double('lineOuterPacks', '20', '4')->nullable();
            $table->string('lineType')->nullable();
            $table->double('LineActualWeight', '20', '4')->nullable();
            $table->string('LineUnitofweight')->nullable();
            $table->double('LineVolume', '20', '4')->nullable();
            $table->string('LineUnitofVolume')->nullable();
            $table->double('LineWidth', '20', '4')->nullable();
            $table->double('LineHeight', '20', '4')->nullable();
            $table->double('LineLength', '20', '4')->nullable();
            $table->string('PackJCContainerNum')->nullable();
            $table->string('PackJCSealNum')->nullable();
            $table->string('PackRCCode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_lines');
    }
}
