<?php

namespace App\Jobs;

use App\Services\DataSyncProcessManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ShipmentSyncingProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $singleShipment;

    /**
     * Create a new job instance.
     *
     * @param $singleShipment
     */
    public function __construct($singleShipment)
    {
        $this->singleShipment = $singleShipment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new DataSyncProcessManager($this->singleShipment))->processDataSync();
    }
}
