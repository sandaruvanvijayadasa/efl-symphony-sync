<?php

namespace App\Jobs;

use App\Services\DataSyncProcessManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessXmlSynchronization implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $dataSyncProcessManager;

    /**
     * Create a new job instance.
     *
     * @param DataSyncProcessManager $dataSyncProcessManager
     */
    public function __construct(DataSyncProcessManager $dataSyncProcessManager)
    {
        $this->dataSyncProcessManager = $dataSyncProcessManager;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->dataSyncProcessManager->processDataSync();
    }
}
