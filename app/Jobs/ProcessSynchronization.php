<?php

namespace App\Jobs;

use App\Repository\SyncFromSQLDbRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessSynchronization implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $consignee;
    private $fromDate;
    private $toDate;

    /**
     * Create a new job instance.
     * @param $consignee
     * @param $fromDate
     * @param $toDate
     */
    public function __construct($consignee, $fromDate = null, $toDate = null)
    {
        $this->consignee = $consignee;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new SyncFromSQLDbRepository())->getShipmentsAndSyncToDB($this->consignee, $this->fromDate, $this->toDate);
    }
}
