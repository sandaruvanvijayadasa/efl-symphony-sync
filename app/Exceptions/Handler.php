<?php

namespace App\Exceptions;

use App\Helpers\VariableHelper;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;
use Exception;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Exception $e, $request) {
            return $this->handleException($request, $e);
        });
    }

    public function handleException($request, Exception $exception): \Illuminate\Http\JsonResponse
    {
        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            VariableHelper::$unauthorized['message'] = __('Unauthorised');
            return response()->json(VariableHelper::$unauthorized, Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof RouteNotFoundException) {
            VariableHelper::$error['message'] = __('The specified URL cannot be found.');
            return response()->json(VariableHelper::$error, Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof UnauthorizedException) {
            VariableHelper::$forbidden['message'] = __('User have not permission for this page access.');
            return response()->json(VariableHelper::$forbidden, Response::HTTP_FORBIDDEN);
        }

    }
}
