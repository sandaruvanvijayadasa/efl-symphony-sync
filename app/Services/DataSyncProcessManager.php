<?php


namespace App\Services;


use App\Models\Booking\BookingLines;
use App\Models\Booking\BookingPurchaseOrders;
use App\Models\Booking\BookingShippingsSyncs;
use App\Models\DataSyncModels\Booking\BookingEventsSynchronization;
use App\Models\DataSyncModels\Booking\BookingLegSynchronization;
use App\Models\DataSyncModels\Booking\BookingLinesSynchronization;
use App\Models\DataSyncModels\Booking\BookingPurchaseOrderSynchronization;
use App\Models\DataSyncModels\Booking\BookingShipmentsSynchronization;
use App\Models\DataSyncModels\Containers\ContainersSynchronization;
use App\Models\DataSyncModels\Shipments\ShipmentEventsSynchronization;
use App\Models\DataSyncModels\Shipments\ShipmentLegSynchronization;
use App\Models\DataSyncModels\Shipments\ShipmentLinesSynchronization;
use App\Models\DataSyncModels\Shipments\ShipmentPurchaseOrderSynchronization;
use App\Models\DataSyncModels\Shipments\ShipmentsSynchronization;
use App\Repository\BookingShipmentDataStoreRepository;
use App\Repository\ShipmentDataStoreRepository;
use App\Repository\SyncFromSQLDbRepository;
use App\Services\Booking\BookingEventsDataMapper;
use App\Services\Booking\BookingLegDataMapper;
use App\Services\Booking\BookingLineDataMapper;
use App\Services\Booking\BookingPurchaseOrderDataMapper;
use App\Services\Booking\BookingShipmentsDataMapper;
use App\Services\Containers\ContainersDataMapper;
use App\Services\Shipments\ShipmentEventsDataMapper;
use App\Services\Shipments\ShipmentLegDataMapper;
use App\Services\Shipments\ShipmentLineDataMapper;
use App\Services\Shipments\ShipmentPurchaseOrderDataMapper;
use App\Services\Shipments\ShipmentsDataMapper;
use Illuminate\Support\Facades\Log;

class DataSyncProcessManager
{

    private $singleResponseRow;

    public function __construct($singleResponseRow)
    {
        $this->singleResponseRow = $singleResponseRow;
    }

    public function processDataSync()
    {
        $JS_UniqueConsignRef = $this->singleResponseRow->JS_UniqueConsignRef;
        if ($this->singleResponseRow->JS_IsForwardRegistered == "true") {
            $masterShipmentData = $this->mappingShipmentData();

            $mainShipmentData = $masterShipmentData['main_shipment_data'];
            $shipmentLegData = $masterShipmentData['leg_data'];
            $purchaseOrderData = $masterShipmentData['purchase_order_data'];
            $lineLevelData = $masterShipmentData['line_level_data'];
            $containersData = $masterShipmentData['containers_data'];
            $shipmentEventData = $masterShipmentData['shipment_event_data'];

            //save all shipment data
            (new ShipmentDataStoreRepository($JS_UniqueConsignRef, $mainShipmentData, $shipmentLegData, $purchaseOrderData, $lineLevelData, $containersData, $shipmentEventData))->saveShipment();
        } else {
            $masterBookingShipmentData = $this->mappingBookingShippingData();

            $mainBookingShipmentData = $masterBookingShipmentData['main_shipment_data'];
            $bookingLegData = $masterBookingShipmentData['leg_data'];
            $bookingPurchaseOrderData = $masterBookingShipmentData['purchase_order_data'];
            $bookingLineLevelData = $masterBookingShipmentData['line_level_data'];
            $bookingEventData = $masterBookingShipmentData['shipment_event_data'];

            //save all booking shipment data
            (new BookingShipmentDataStoreRepository($JS_UniqueConsignRef, $mainBookingShipmentData, $bookingLegData, $bookingPurchaseOrderData, $bookingLineLevelData, $bookingEventData))->saveBookingShipment();
        }
    }

    public function mappingShipmentData(): array
    {
        $masterShipmentData = array();
        //set main shipment data
        $mainShipmentData = (new ShipmentsDataMapper(new ShipmentsSynchronization(), $this->singleResponseRow))->map();
        $masterShipmentData['main_shipment_data'] = $mainShipmentData;

        //set shipment leg data
        $shipmentLegData = (new ShipmentLegDataMapper(new ShipmentLegSynchronization(), $this->singleResponseRow))->map();
        $masterShipmentData['leg_data'] = $shipmentLegData;

        //set purchase order & line data
        $poAndLineData = $this->setShipmentPurchaseOrderAndLineData();
        $masterShipmentData['purchase_order_data'] = $poAndLineData['purchase_order_data'];
        $masterShipmentData['line_level_data'] = $poAndLineData['line_level_data'];

        //set shipment containers data
        $masterShipmentData['containers_data'] = $this->setShipmentContainersData();

        //set shipment event data
        $masterShipmentData['shipment_event_data'] = $this->setShipmentEventsData();

        return $masterShipmentData;
    }

    public function mappingBookingShippingData(): array
    {
        $masterBookingShipmentData = array();
        //set main booking shipment data
        $mainBookingShipmentData = (new BookingShipmentsDataMapper(new BookingShipmentsSynchronization(), $this->singleResponseRow))->map();
        $masterBookingShipmentData['main_shipment_data'] = $mainBookingShipmentData;

        //set booking shipment leg data
        $bookingShipmentLegData = (new BookingLegDataMapper(new BookingLegSynchronization(), $this->singleResponseRow))->map();
        $masterBookingShipmentData['leg_data'] = $bookingShipmentLegData;

        //set booking purchase order & line data
        $poAndLineData = $this->setBookingShipmentPurchaseOrderAndLineData();
        $masterBookingShipmentData['purchase_order_data'] = $poAndLineData['purchase_order_data'];
        $masterBookingShipmentData['line_level_data'] = $poAndLineData['line_level_data'];

        //set booking shipment event data
        $masterBookingShipmentData['shipment_event_data'] = $this->setBookingShipmentEventsData();

        return $masterBookingShipmentData;
    }

    /**
     * shipment po & line data mapping
     *
     * @return array[]
     */
    public function setShipmentPurchaseOrderAndLineData(): array
    {
        $purchaseOrderDataArray = array();
        $lineLevelDataArray = array();
        $purchase_order_index = 0;
        $line_level_index = 0;
        $purchaseOrder = null;

        $purchaseOrder = (new ShipmentPurchaseOrderDataMapper(new ShipmentPurchaseOrderSynchronization(), $this->singleResponseRow))->map();
        $purchaseOrderDataArray[$purchase_order_index] = $purchaseOrder;

        $lineLevel = (new ShipmentLineDataMapper(new ShipmentLinesSynchronization(), $this->singleResponseRow))->map();
        $lineLevelDataArray[$line_level_index] = $lineLevel;

        return [
            'purchase_order_data' => $purchaseOrderDataArray,
            'line_level_data' => $lineLevelDataArray
        ];
    }

    /**
     * booking shipment po & line data mapping
     *
     * @return array[]
     */
    public function setBookingShipmentPurchaseOrderAndLineData(): array
    {
        $purchaseOrderDataArray = array();
        $lineLevelDataArray = array();
        $purchase_order_index = 0;
        $line_level_index = 0;
        $purchaseOrder = null;

        Log::info('booking mapping', ['job_id' => $this->singleResponseRow->JS_UniqueConsignRef]);

        $purchaseOrder = (new BookingPurchaseOrderDataMapper(new BookingPurchaseOrderSynchronization(), $this->singleResponseRow))->map();
        $purchaseOrderDataArray[$purchase_order_index] = $purchaseOrder;

        if($this->singleResponseRow->OrderLineCount > 0){
            $lineLevel = (new BookingLineDataMapper(new BookingLinesSynchronization(), $this->singleResponseRow))->map();
            $lineLevelDataArray[$line_level_index] = $lineLevel;
        }

        return [
            'purchase_order_data' => $purchaseOrderDataArray,
            'line_level_data' => $lineLevelDataArray
        ];
    }

    /**
     * shipment containers data mapping
     *
     * @return array
     */
    public function setShipmentContainersData(): array
    {
        $containersData = array();
        $container_index = 0;
        $shipmentNo = $this->singleResponseRow->JS_UniqueConsignRef;
        $containers = (new SyncFromSQLDbRepository())->getContainers($shipmentNo);
        $containerCount = count($containers);
        $containerLineNo = 1;
        if (!is_null($containers) && $containerCount > 0) {
            foreach ($containers as $containerDetail) {
                if (intval($containerDetail->Containercount) > 0) {
                    $containerData = (new ContainersDataMapper(new ContainersSynchronization(), $containerDetail, $containerLineNo))->map();
                    $containersData[$container_index] = $containerData;
                    $container_index++;
                    $containerLineNo++;
                }
            }
            return $containersData;
        } else {
            return $containersData;
        }
    }

    /**
     * shipment events data mapping
     *
     * @return array
     */
    public function setShipmentEventsData(): array
    {
        $eventsData = array();
        $eventIndex = 0;
        $shipmentNo = $this->singleResponseRow->JS_UniqueConsignRef;
        $events = (new SyncFromSQLDbRepository())->getEvents($shipmentNo);
        if (!is_null($events) && count($events) > 0) {
            foreach ($events as $event) {
                $eventData = (new ShipmentEventsDataMapper(new ShipmentEventsSynchronization(), $event))->map();
                $eventsData[$eventIndex] = $eventData;
                $eventIndex++;
            }
            return $eventsData;
        } else {
            return $eventsData;
        }
    }

    /**
     * booking shipment events data mapping
     *
     * @return array
     */
    public function setBookingShipmentEventsData(): array
    {
        $eventsData = array();
        $eventIndex = 0;
        $shipmentNo = $this->singleResponseRow->JS_UniqueConsignRef;
        $events = (new SyncFromSQLDbRepository())->getEvents($shipmentNo);
        if (!is_null($events) && count($events) > 0) {
            foreach ($events as $event) {
                $eventData = (new BookingEventsDataMapper(new BookingEventsSynchronization(), $event))->map();
                $eventsData[$eventIndex] = $eventData;
                $eventIndex++;
            }
            return $eventsData;
        } else {
            return $eventsData;
        }
    }
}
