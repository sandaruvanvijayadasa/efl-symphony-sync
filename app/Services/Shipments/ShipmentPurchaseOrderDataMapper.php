<?php


namespace App\Services\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentPurchaseOrderSynchronization;

class ShipmentPurchaseOrderDataMapper
{

    private $shipmentPurchaseOrderSynchronization;
    private $poPayload;

    public function __construct(ShipmentPurchaseOrderSynchronization $shipmentPurchaseOrderSynchronization, $poPayload)
    {
        $this->shipmentPurchaseOrderSynchronization = $shipmentPurchaseOrderSynchronization;
        $this->poPayload = $poPayload;
    }

    public function map(): ShipmentPurchaseOrderSynchronization
    {
        $this->shipmentPurchaseOrderSynchronization->setOrderNumber($this->poPayload->OrderNo);
        $this->shipmentPurchaseOrderSynchronization->setBuyerCode($this->poPayload->BuyerCode);
        $this->shipmentPurchaseOrderSynchronization->setSupplierCode($this->poPayload->SupplierCode);
        $this->shipmentPurchaseOrderSynchronization->setConfirmNumber($this->poPayload->ConfirmNo);
        $this->shipmentPurchaseOrderSynchronization->setInvoiceNumber($this->poPayload->InvoiceNo);
        $this->shipmentPurchaseOrderSynchronization->setOderNumberDate($this->poPayload->OrderNoDate);
        $this->shipmentPurchaseOrderSynchronization->setInvoiceNumberDate($this->poPayload->InvoiceNoDate);
        $this->shipmentPurchaseOrderSynchronization->setConfirmNumberDate($this->poPayload->ConfirmNoDate);
        $this->shipmentPurchaseOrderSynchronization->setFollowUpDate($this->poPayload->Followupdate);
        $this->shipmentPurchaseOrderSynchronization->setRequireInstoDate($this->poPayload->Requiredinstoredate);
        $this->shipmentPurchaseOrderSynchronization->setOrderStatus($this->poPayload->OrderStatus);
        $this->shipmentPurchaseOrderSynchronization->setRequireExWorkDate($this->poPayload->ReqExWorkDate);
        $this->shipmentPurchaseOrderSynchronization->setOrderGoodDescription($this->poPayload->OrderGoodsDescription);
        $this->shipmentPurchaseOrderSynchronization->setCurrency($this->poPayload->Currency);
        $this->shipmentPurchaseOrderSynchronization->setOrderServiceLevel($this->poPayload->OrderServiceLevel);
        $this->shipmentPurchaseOrderSynchronization->setOrderINCOTerm($this->poPayload->OrderINCOTerm);
        $this->shipmentPurchaseOrderSynchronization->setAdditionalTerms($this->poPayload->AdditionalTerms);
        $this->shipmentPurchaseOrderSynchronization->setOrderTransMode($this->poPayload->OrderTransMode);
        $this->shipmentPurchaseOrderSynchronization->setOrderCountryOrigin($this->poPayload->OrderCountryOrigin);

        return $this->shipmentPurchaseOrderSynchronization;
    }

}
