<?php


namespace App\Services\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentEventsSynchronization;

class ShipmentEventsDataMapper
{

    private $shipmentEventsSynchronization;
    private $eventPayload;

    public function __construct(ShipmentEventsSynchronization $shipmentEventsSynchronization, $eventPayload)
    {
        $this->shipmentEventsSynchronization = $shipmentEventsSynchronization;
        $this->eventPayload = $eventPayload;
    }

    public function map(): array
    {
        $allEventData = array();
        $x = 0;

        $arrivalEventCount = $this->eventPayload->ArrivalEventCount;
        if (intval($arrivalEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('ArrivalEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->ArrivalEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->ArrivalEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $cargoAvailableEventCount = $this->eventPayload->CargoAvailableEventCount;
        if (intval($cargoAvailableEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('CargoAvailableEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->CargoAvailableEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->CargoAvailableEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $cutOffDateEventCount = $this->eventPayload->CutOffDateEventCount;
        if (intval($cutOffDateEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('CutOffDateEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->CutOffDateEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->CutOffDateEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $departureEventCount = $this->eventPayload->DepartureEventCount;
        if (intval($departureEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('DepartureEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->DepartureEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->DepartureEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $dehireEventCount = $this->eventPayload->DehireEventCount;
        if (intval($dehireEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('DehireEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->DehireEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->DehireEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $deliveredEventCount = $this->eventPayload->DeliveredEventCount;
        if (intval($deliveredEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('DeliveredEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->DeliveredEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->DeliveredEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $freightLoadedCount = $this->eventPayload->FreightLoadedCount;
        if (intval($freightLoadedCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('FreightLoadedEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->FreightLoadedEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->FreightLoadedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $freightUnloadedEventCount = $this->eventPayload->FreightUnloadedEventCount;
        if (intval($freightUnloadedEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('FreightUnloadedEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->FreightUnloadedEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->FreightUnloadedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $gateInEventCount = $this->eventPayload->GateInEventCount;
        if (intval($gateInEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('GateInEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->GateInEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->GateInEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $gateOutEventCount = $this->eventPayload->GateOutEventCount;
        if (intval($gateOutEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('GateOutEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->GateOutEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->GateOutEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $handedOverEventCount = $this->eventPayload->HandedOverEventCount;
        if (intval($handedOverEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('HandedOverEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->HandedOverEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->HandedOverEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $quantityVerifiedEventCount = $this->eventPayload->QuantityVerifiedEventCount;
        if (intval($quantityVerifiedEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('QuantityVerifiedEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->QuantityVerifiedEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->QuantityVerifiedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $receivedEventCount = $this->eventPayload->ReceivedEventCount;
        if (intval($receivedEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('ReceivedEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->ReceivedEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->ReceivedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $releasedEventCount = $this->eventPayload->ReleasedEventCount;
        if (intval($releasedEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('ReleasedEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->ReleasedEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->ReleasedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $clearanceCompletedEventCount = $this->eventPayload->ClearanceCompletedEventCount;
        if (intval($clearanceCompletedEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('ClearanceCompletedEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->ClearanceCompletedEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->ClearanceCompletedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $heldEventCount = $this->eventPayload->HeldEventCount;
        if (intval($heldEventCount) > 0) {
            $this->shipmentEventsSynchronization->setEvenType('HeldEvent');
            $this->shipmentEventsSynchronization->setEventDate($this->eventPayload->HeldEventdate);
            $this->shipmentEventsSynchronization->setEventDetail($this->eventPayload->HeldEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->shipmentEventsSynchronization->getEvenType(),
                'event_date' => $this->shipmentEventsSynchronization->getEventDate(),
                'event_detail' => $this->shipmentEventsSynchronization->getEventDetail()
            ];
        }

        return $allEventData;
    }

}
