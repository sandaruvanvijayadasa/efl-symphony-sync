<?php


namespace App\Services\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentLegSynchronization;

class ShipmentLegDataMapper
{

    private $shipmentLegSynchronization;
    private $shipmentPayload;

    public function __construct(ShipmentLegSynchronization $shipmentLegSynchronization, $shipmentPayload)
    {
        $this->shipmentLegSynchronization = $shipmentLegSynchronization;
        $this->shipmentPayload = $shipmentPayload;
    }

    public function map(): ShipmentLegSynchronization
    {
        $this->shipmentLegSynchronization->setLegOrder1($this->shipmentPayload->Leg_Order1);
        $this->shipmentLegSynchronization->setVoyageFlight1($this->shipmentPayload->VoyageFlight1);
        $this->shipmentLegSynchronization->setVessel1($this->shipmentPayload->Vessel1);
        $this->shipmentLegSynchronization->setLegETD1($this->shipmentPayload->legETD1);
        $this->shipmentLegSynchronization->setLegETA1($this->shipmentPayload->legETA1);
        $this->shipmentLegSynchronization->setLegATD1($this->shipmentPayload->legATD1);
        $this->shipmentLegSynchronization->setLegATA1($this->shipmentPayload->legATA1);
        $this->shipmentLegSynchronization->setLeg1load($this->shipmentPayload->leg1load);
        $this->shipmentLegSynchronization->setLeg1disc($this->shipmentPayload->leg1disc);
        $this->shipmentLegSynchronization->setMode1($this->shipmentPayload->Mode1);

        $this->shipmentLegSynchronization->setLegOrder2($this->shipmentPayload->Leg_Order2);
        $this->shipmentLegSynchronization->setVoyageFlight2($this->shipmentPayload->VoyageFlight2);
        $this->shipmentLegSynchronization->setVessel2($this->shipmentPayload->Vessel2);
        $this->shipmentLegSynchronization->setLegETD2($this->shipmentPayload->legETD2);
        $this->shipmentLegSynchronization->setLegETA2($this->shipmentPayload->legETA2);
        $this->shipmentLegSynchronization->setLegATD2($this->shipmentPayload->legATD2);
        $this->shipmentLegSynchronization->setLegATA2($this->shipmentPayload->legATA2);
        $this->shipmentLegSynchronization->setLeg2load($this->shipmentPayload->leg2load);
        $this->shipmentLegSynchronization->setLeg2disc($this->shipmentPayload->leg2disc);
        $this->shipmentLegSynchronization->setMode2($this->shipmentPayload->Mode2);

        $this->shipmentLegSynchronization->setLegOrder3($this->shipmentPayload->Leg_Order3);
        $this->shipmentLegSynchronization->setVoyageFlight3($this->shipmentPayload->VoyageFlight3);
        $this->shipmentLegSynchronization->setVessel3($this->shipmentPayload->Vessel3);
        $this->shipmentLegSynchronization->setLegETD3($this->shipmentPayload->legETD3);
        $this->shipmentLegSynchronization->setLegETA3($this->shipmentPayload->legETA3);
        $this->shipmentLegSynchronization->setLegATD3($this->shipmentPayload->legATD3);
        $this->shipmentLegSynchronization->setLegATA3($this->shipmentPayload->legATA3);
        $this->shipmentLegSynchronization->setLeg3load($this->shipmentPayload->leg3load);
        $this->shipmentLegSynchronization->setLeg3disc($this->shipmentPayload->leg3disc);
        $this->shipmentLegSynchronization->setMode3($this->shipmentPayload->Mode3);

        $this->shipmentLegSynchronization->setLegOrder4($this->shipmentPayload->Leg_Order4);
        $this->shipmentLegSynchronization->setVoyageFlight4($this->shipmentPayload->VoyageFlight4);
        $this->shipmentLegSynchronization->setVessel4($this->shipmentPayload->Vessel4);
        $this->shipmentLegSynchronization->setLegETD4($this->shipmentPayload->legETD4);
        $this->shipmentLegSynchronization->setLegETA4($this->shipmentPayload->legETA4);
        $this->shipmentLegSynchronization->setLegATD4($this->shipmentPayload->legATD4);
        $this->shipmentLegSynchronization->setLegATA4($this->shipmentPayload->legATA4);
        $this->shipmentLegSynchronization->setLeg4load($this->shipmentPayload->leg4load);
        $this->shipmentLegSynchronization->setLeg4disc($this->shipmentPayload->leg4disc);
        $this->shipmentLegSynchronization->setMode4($this->shipmentPayload->Mode4);

        return $this->shipmentLegSynchronization;
    }
}
