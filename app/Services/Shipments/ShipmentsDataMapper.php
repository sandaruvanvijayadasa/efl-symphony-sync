<?php


namespace App\Services\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentsSynchronization;

class ShipmentsDataMapper
{

    private $shipmentsSynchronization;
    private $shipmentPayload;

    public function __construct(ShipmentsSynchronization $shipmentsSynchronization, $shipmentPayload)
    {
        $this->shipmentsSynchronization = $shipmentsSynchronization;
        $this->shipmentPayload = $shipmentPayload;
    }

    public function map(): ShipmentsSynchronization
    {
        $this->shipmentsSynchronization->setUniqueConsignRef($this->shipmentPayload->JS_UniqueConsignRef);
        $this->shipmentsSynchronization->setIsForwardRegistered($this->shipmentPayload->JS_IsForwardRegistered);
        $this->shipmentsSynchronization->setBookedPallets($this->shipmentPayload->BookedPallets);
        $this->shipmentsSynchronization->setBookedWeight($this->shipmentPayload->BookedWeight);
        $this->shipmentsSynchronization->setTransportMode($this->shipmentPayload->JS_TransportMode);
        $this->shipmentsSynchronization->setUnitOfWeight($this->shipmentPayload->JS_UnitOfWeight);
        $this->shipmentsSynchronization->setUnitOfVolume($this->shipmentPayload->JS_UnitOfVolume);
        $this->shipmentsSynchronization->setUnitOfActualChargeable($this->shipmentPayload->JS_UnitOfActualChargeable);
        $this->shipmentsSynchronization->setF3NkPackType($this->shipmentPayload->JS_F3_NKPackType);
        $this->shipmentsSynchronization->setRsNkServiceLevel($this->shipmentPayload->JS_RS_NKServiceLevel);
        $this->shipmentsSynchronization->setLoadPort($this->shipmentPayload->LoadPort);
        $this->shipmentsSynchronization->setDischargePort($this->shipmentPayload->DischargePort);
        $this->shipmentsSynchronization->setSystemLastEditTimeUtc($this->shipmentPayload->JS_SystemLastEditTimeUtc);
        $this->shipmentsSynchronization->setIncoTerm($this->shipmentPayload->JS_INCO);
        $this->shipmentsSynchronization->setPackingMode($this->shipmentPayload->JS_PackingMode);
        $this->shipmentsSynchronization->setShipmentType($this->shipmentPayload->JS_ShipmentType);
        $this->shipmentsSynchronization->setCarrierCode($this->shipmentPayload->Carrier_CODE);
        $this->shipmentsSynchronization->setGoodsDescription($this->shipmentPayload->GoodsDescription);
        $this->shipmentsSynchronization->setVoyageFlight($this->shipmentPayload->VoyageFlight);
        $this->shipmentsSynchronization->setVessels($this->shipmentPayload->Vessel);
        $this->shipmentsSynchronization->setETD($this->shipmentPayload->ETD);
        $this->shipmentsSynchronization->setETA($this->shipmentPayload->ETA);
        $this->shipmentsSynchronization->setExpoRepresentative($this->shipmentPayload->ExpoRepresentative);
        $this->shipmentsSynchronization->setShipperRepresentative($this->shipmentPayload->ShipperRepresentative);
        $this->shipmentsSynchronization->setRlNkOrigin($this->shipmentPayload->JS_RL_NKOrigin);
        $this->shipmentsSynchronization->setRlNkDestination($this->shipmentPayload->JS_RL_NKDestination);
        $this->shipmentsSynchronization->setActualChargeable($this->shipmentPayload->JS_ActualChargeable);
        $this->shipmentsSynchronization->setBookedTEU($this->shipmentPayload->TEU);
        $this->shipmentsSynchronization->setShippersRef($this->shipmentPayload->ShippersRef);
        $this->shipmentsSynchronization->setVehicleNo($this->shipmentPayload->VehicleNo);
        $this->shipmentsSynchronization->setDriverName($this->shipmentPayload->DriverName);
        $this->shipmentsSynchronization->setReceivedDate($this->shipmentPayload->Received_Date);
        $this->shipmentsSynchronization->setCustomsCheck($this->shipmentPayload->CustomsCheck);
        $this->shipmentsSynchronization->setMarksNumber($this->shipmentPayload->MarksNumber);
        $this->shipmentsSynchronization->setRemarks($this->shipmentPayload->Remarks);
        $this->shipmentsSynchronization->setOrderNumber($this->shipmentPayload->OrderNo);
        $this->shipmentsSynchronization->setMasterBillNum($this->shipmentPayload->JK_MasterBillNum);
        $this->shipmentsSynchronization->setConsignorCode($this->shipmentPayload->ConsignorCode);
        $this->shipmentsSynchronization->setLocalClient($this->shipmentPayload->LocalClientCode);
        $this->shipmentsSynchronization->setConsigneeCode($this->shipmentPayload->ConsigneeCode);
        $this->shipmentsSynchronization->setATD($this->shipmentPayload->ATD);
        $this->shipmentsSynchronization->setATA($this->shipmentPayload->ATA);
        $this->shipmentsSynchronization->setConfirmatAirline($this->shipmentPayload->ConfirmatAirline);
        $this->shipmentsSynchronization->setActualWeight($this->shipmentPayload->JS_ActualWeight);
        $this->shipmentsSynchronization->setActualVolume($this->shipmentPayload->JS_ActualVolume);
        $this->shipmentsSynchronization->setOuterPacks($this->shipmentPayload->JS_OuterPacks);
        $this->shipmentsSynchronization->setHAWB($this->shipmentPayload->HAWB);
        $this->shipmentsSynchronization->setSystemCreateTimeUtc($this->shipmentPayload->JS_SystemCreateTimeUtc);
        $this->shipmentsSynchronization->setCarrierName($this->shipmentPayload->CarrierName);
        $this->shipmentsSynchronization->setSendAgentCode($this->shipmentPayload->SendAgentCode);
        $this->shipmentsSynchronization->setSendAgentName($this->shipmentPayload->SendAgentName);
        $this->shipmentsSynchronization->setRecvAgentCode($this->shipmentPayload->RecvAgentCode);
        $this->shipmentsSynchronization->setRecvAgentName($this->shipmentPayload->RecvAgentName);
        $this->shipmentsSynchronization->setActualDeliveryDate($this->shipmentPayload->ActualDeliverydate);

        return $this->shipmentsSynchronization;
    }
}
