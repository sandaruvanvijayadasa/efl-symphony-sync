<?php


namespace App\Services\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentLinesSynchronization;

class ShipmentLineDataMapper
{

    private $shipmentLinesSynchronization;
    private $linePayload;

    public function __construct(ShipmentLinesSynchronization $shipmentLinesSynchronization, $linePayload)
    {
        $this->shipmentLinesSynchronization = $shipmentLinesSynchronization;
        $this->linePayload = $linePayload;
    }

    public function map(): ShipmentLinesSynchronization
    {
        $this->shipmentLinesSynchronization->setJlPk($this->linePayload->JL_PK);
        $this->shipmentLinesSynchronization->setLineDescription($this->linePayload->OrderLineDescription);
        $this->shipmentLinesSynchronization->setLineOuterPacks($this->linePayload->OrderLineOutterPacks);
        $this->shipmentLinesSynchronization->setLineType($this->linePayload->OrderLineType);
        $this->shipmentLinesSynchronization->setLineActualWeight($this->linePayload->OrderLineActualWeight);
        $this->shipmentLinesSynchronization->setLineUnitofweight($this->linePayload->OrderLineUnitofweight);
        $this->shipmentLinesSynchronization->setLineVolume($this->linePayload->OrderLineVolume);
        $this->shipmentLinesSynchronization->setLineUnitofVolume($this->linePayload->OrderLineUnitofVolume);
        $this->shipmentLinesSynchronization->setLineWidth($this->linePayload->OrderLineWidth);
        $this->shipmentLinesSynchronization->setLineHeight($this->linePayload->OrderLineHeight);
        $this->shipmentLinesSynchronization->setLineLength($this->linePayload->OrderLineLength);
        $this->shipmentLinesSynchronization->setPackJCContainerNum($this->linePayload->Pack_JC_ContainerNum);
        $this->shipmentLinesSynchronization->setPackJCSealNum($this->linePayload->Pack_JC_SealNum);
        $this->shipmentLinesSynchronization->setPackRCCode($this->linePayload->Pack_RC_Code);

        return $this->shipmentLinesSynchronization;
    }

}
