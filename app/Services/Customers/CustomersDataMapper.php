<?php


namespace App\Services\Customers;


use App\Models\DataSyncModels\Customers\CustomersSynchronization;

class CustomersDataMapper
{

    private $customersSynchronization;
    private $customerPayload;

    public function __construct(CustomersSynchronization $customersSynchronization, $customerPayload)
    {
        $this->customersSynchronization = $customersSynchronization;
        $this->customerPayload = $customerPayload;
    }

    public function map()
    {

    }

}
