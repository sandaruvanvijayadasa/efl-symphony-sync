<?php


namespace App\Services\Booking;



use App\Models\DataSyncModels\Booking\BookingShipmentsSynchronization;

class BookingShipmentsDataMapper
{

    private $bookingShipmentsSynchronization;
    private $bookingShipmentPayload;

    public function __construct(BookingShipmentsSynchronization $bookingShipmentsSynchronization, $bookingShipmentPayload)
    {
        $this->bookingShipmentsSynchronization = $bookingShipmentsSynchronization;
        $this->bookingShipmentPayload = $bookingShipmentPayload;
    }

    public function map(): BookingShipmentsSynchronization
    {
        $this->bookingShipmentsSynchronization->setUniqueConsignRef($this->bookingShipmentPayload->JS_UniqueConsignRef);
        $this->bookingShipmentsSynchronization->setIsForwardRegistered($this->bookingShipmentPayload->JS_IsForwardRegistered);
        $this->bookingShipmentsSynchronization->setBookedPallets($this->bookingShipmentPayload->BookedPallets);
        $this->bookingShipmentsSynchronization->setBookedWeight($this->bookingShipmentPayload->BookedWeight);
        $this->bookingShipmentsSynchronization->setTransportMode($this->bookingShipmentPayload->JS_TransportMode);
        $this->bookingShipmentsSynchronization->setUnitOfWeight($this->bookingShipmentPayload->JS_UnitOfWeight);
        $this->bookingShipmentsSynchronization->setUnitOfVolume($this->bookingShipmentPayload->JS_UnitOfVolume);
        $this->bookingShipmentsSynchronization->setUnitOfActualChargeable($this->bookingShipmentPayload->JS_UnitOfActualChargeable);
        $this->bookingShipmentsSynchronization->setF3NkPackType($this->bookingShipmentPayload->JS_F3_NKPackType);
        $this->bookingShipmentsSynchronization->setRsNkServiceLevel($this->bookingShipmentPayload->JS_RS_NKServiceLevel);
        $this->bookingShipmentsSynchronization->setLoadPort($this->bookingShipmentPayload->LoadPort);
        $this->bookingShipmentsSynchronization->setDischargePort($this->bookingShipmentPayload->DischargePort);
        $this->bookingShipmentsSynchronization->setSystemLastEditTimeUtc($this->bookingShipmentPayload->JS_SystemLastEditTimeUtc);
        $this->bookingShipmentsSynchronization->setIncoTerm($this->bookingShipmentPayload->JS_INCO);
        $this->bookingShipmentsSynchronization->setPackingMode($this->bookingShipmentPayload->JS_PackingMode);
        $this->bookingShipmentsSynchronization->setShipmentType($this->bookingShipmentPayload->JS_ShipmentType);
        $this->bookingShipmentsSynchronization->setCarrierCode($this->bookingShipmentPayload->Carrier_CODE);
        $this->bookingShipmentsSynchronization->setGoodsDescription($this->bookingShipmentPayload->GoodsDescription);
        $this->bookingShipmentsSynchronization->setVoyageFlight($this->bookingShipmentPayload->VoyageFlight);
        $this->bookingShipmentsSynchronization->setVessels($this->bookingShipmentPayload->Vessel);
        $this->bookingShipmentsSynchronization->setETD($this->bookingShipmentPayload->ETD);
        $this->bookingShipmentsSynchronization->setETA($this->bookingShipmentPayload->ETA);
        $this->bookingShipmentsSynchronization->setExpoRepresentative($this->bookingShipmentPayload->ExpoRepresentative);
        $this->bookingShipmentsSynchronization->setShipperRepresentative($this->bookingShipmentPayload->ShipperRepresentative);
        $this->bookingShipmentsSynchronization->setRlNkOrigin($this->bookingShipmentPayload->JS_RL_NKOrigin);
        $this->bookingShipmentsSynchronization->setRlNkDestination($this->bookingShipmentPayload->JS_RL_NKDestination);
        $this->bookingShipmentsSynchronization->setActualChargeable($this->bookingShipmentPayload->JS_ActualChargeable);
        $this->bookingShipmentsSynchronization->setBookedTEU($this->bookingShipmentPayload->TEU);
        $this->bookingShipmentsSynchronization->setShippersRef($this->bookingShipmentPayload->ShippersRef);
        $this->bookingShipmentsSynchronization->setVehicleNo($this->bookingShipmentPayload->VehicleNo);
        $this->bookingShipmentsSynchronization->setDriverName($this->bookingShipmentPayload->DriverName);
        $this->bookingShipmentsSynchronization->setReceivedDate($this->bookingShipmentPayload->Received_Date);
        $this->bookingShipmentsSynchronization->setCustomsCheck($this->bookingShipmentPayload->CustomsCheck);
        $this->bookingShipmentsSynchronization->setMarksNumber($this->bookingShipmentPayload->MarksNumber);
        $this->bookingShipmentsSynchronization->setRemarks($this->bookingShipmentPayload->Remarks);
        $this->bookingShipmentsSynchronization->setOrderNumber($this->bookingShipmentPayload->OrderNo);
        $this->bookingShipmentsSynchronization->setMasterBillNum($this->bookingShipmentPayload->JK_MasterBillNum);
        $this->bookingShipmentsSynchronization->setConsignorCode($this->bookingShipmentPayload->ConsignorCode);
        $this->bookingShipmentsSynchronization->setLocalClient($this->bookingShipmentPayload->LocalClientCode);
        $this->bookingShipmentsSynchronization->setConsigneeCode($this->bookingShipmentPayload->ConsigneeCode);
        $this->bookingShipmentsSynchronization->setATD($this->bookingShipmentPayload->ATD);
        $this->bookingShipmentsSynchronization->setATA($this->bookingShipmentPayload->ATA);
        $this->bookingShipmentsSynchronization->setConfirmatAirline($this->bookingShipmentPayload->ConfirmatAirline);
        $this->bookingShipmentsSynchronization->setActualWeight($this->bookingShipmentPayload->JS_ActualWeight);
        $this->bookingShipmentsSynchronization->setActualVolume($this->bookingShipmentPayload->JS_ActualVolume);
        $this->bookingShipmentsSynchronization->setOuterPacks($this->bookingShipmentPayload->JS_OuterPacks);
        $this->bookingShipmentsSynchronization->setHAWB($this->bookingShipmentPayload->HAWB);
        $this->bookingShipmentsSynchronization->setSystemCreateTimeUtc($this->bookingShipmentPayload->JS_SystemCreateTimeUtc);
        $this->bookingShipmentsSynchronization->setCarrierName($this->bookingShipmentPayload->CarrierName);
        $this->bookingShipmentsSynchronization->setSendAgentCode($this->bookingShipmentPayload->SendAgentCode);
        $this->bookingShipmentsSynchronization->setSendAgentName($this->bookingShipmentPayload->SendAgentName);
        $this->bookingShipmentsSynchronization->setRecvAgentCode($this->bookingShipmentPayload->RecvAgentCode);
        $this->bookingShipmentsSynchronization->setRecvAgentName($this->bookingShipmentPayload->RecvAgentName);
        $this->bookingShipmentsSynchronization->setActualDeliveryDate($this->bookingShipmentPayload->ActualDeliverydate);

        return $this->bookingShipmentsSynchronization;
    }
}
