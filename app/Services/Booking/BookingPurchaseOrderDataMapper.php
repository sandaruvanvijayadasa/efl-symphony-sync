<?php


namespace App\Services\Booking;


use App\Models\DataSyncModels\Booking\BookingPurchaseOrderSynchronization;

class BookingPurchaseOrderDataMapper
{

    private $bookingPurchaseOrderSynchronization;
    private $poPayload;

    public function __construct(BookingPurchaseOrderSynchronization $bookingPurchaseOrderSynchronization, $poPayload)
    {
        $this->bookingPurchaseOrderSynchronization = $bookingPurchaseOrderSynchronization;
        $this->poPayload = $poPayload;
    }

    public function map(): BookingPurchaseOrderSynchronization
    {
        $this->bookingPurchaseOrderSynchronization->setOrderNumber($this->poPayload->OrderNo);
        $this->bookingPurchaseOrderSynchronization->setBuyerCode($this->poPayload->BuyerCode);
        $this->bookingPurchaseOrderSynchronization->setSupplierCode($this->poPayload->SupplierCode);
        $this->bookingPurchaseOrderSynchronization->setConfirmNumber($this->poPayload->ConfirmNo);
        $this->bookingPurchaseOrderSynchronization->setInvoiceNumber($this->poPayload->InvoiceNo);
        $this->bookingPurchaseOrderSynchronization->setOderNumberDate($this->poPayload->OrderNoDate);
        $this->bookingPurchaseOrderSynchronization->setInvoiceNumberDate($this->poPayload->InvoiceNoDate);
        $this->bookingPurchaseOrderSynchronization->setConfirmNumberDate($this->poPayload->ConfirmNoDate);
        $this->bookingPurchaseOrderSynchronization->setFollowUpDate($this->poPayload->Followupdate);
        $this->bookingPurchaseOrderSynchronization->setRequireInstoDate($this->poPayload->Requiredinstoredate);
        $this->bookingPurchaseOrderSynchronization->setOrderStatus($this->poPayload->OrderStatus);
        $this->bookingPurchaseOrderSynchronization->setRequireExWorkDate($this->poPayload->ReqExWorkDate);
        $this->bookingPurchaseOrderSynchronization->setOrderGoodDescription($this->poPayload->OrderGoodsDescription);
        $this->bookingPurchaseOrderSynchronization->setCurrency($this->poPayload->Currency);
        $this->bookingPurchaseOrderSynchronization->setOrderServiceLevel($this->poPayload->OrderServiceLevel);
        $this->bookingPurchaseOrderSynchronization->setOrderINCOTerm($this->poPayload->OrderINCOTerm);
        $this->bookingPurchaseOrderSynchronization->setAdditionalTerms($this->poPayload->AdditionalTerms);
        $this->bookingPurchaseOrderSynchronization->setOrderTransMode($this->poPayload->OrderTransMode);
        $this->bookingPurchaseOrderSynchronization->setOrderCountryOrigin($this->poPayload->OrderCountryOrigin);

        return $this->bookingPurchaseOrderSynchronization;
    }
}
