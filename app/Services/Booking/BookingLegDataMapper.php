<?php


namespace App\Services\Booking;


use App\Models\DataSyncModels\Booking\BookingLegSynchronization;

class BookingLegDataMapper
{

    private $bookingLegSynchronization;
    private $bookingShipmentPayload;

    public function __construct(BookingLegSynchronization $bookingLegSynchronization, $bookingShipmentPayload)
    {
        $this->bookingLegSynchronization = $bookingLegSynchronization;
        $this->bookingShipmentPayload = $bookingShipmentPayload;
    }

    public function map(): BookingLegSynchronization
    {
        $this->bookingLegSynchronization->setLegOrder1($this->bookingShipmentPayload->Leg_Order1);
        $this->bookingLegSynchronization->setVoyageFlight1($this->bookingShipmentPayload->VoyageFlight1);
        $this->bookingLegSynchronization->setVessel1($this->bookingShipmentPayload->Vessel1);
        $this->bookingLegSynchronization->setLegETD1($this->bookingShipmentPayload->legETD1);
        $this->bookingLegSynchronization->setLegETA1($this->bookingShipmentPayload->legETA1);
        $this->bookingLegSynchronization->setLegATD1($this->bookingShipmentPayload->legATD1);
        $this->bookingLegSynchronization->setLegATA1($this->bookingShipmentPayload->legATA1);
        $this->bookingLegSynchronization->setLeg1load($this->bookingShipmentPayload->leg1load);
        $this->bookingLegSynchronization->setLeg1disc($this->bookingShipmentPayload->leg1disc);
        $this->bookingLegSynchronization->setMode1($this->bookingShipmentPayload->Mode1);

        $this->bookingLegSynchronization->setLegOrder2($this->bookingShipmentPayload->Leg_Order2);
        $this->bookingLegSynchronization->setVoyageFlight2($this->bookingShipmentPayload->VoyageFlight2);
        $this->bookingLegSynchronization->setVessel2($this->bookingShipmentPayload->Vessel2);
        $this->bookingLegSynchronization->setLegETD2($this->bookingShipmentPayload->legETD2);
        $this->bookingLegSynchronization->setLegETA2($this->bookingShipmentPayload->legETA2);
        $this->bookingLegSynchronization->setLegATD2($this->bookingShipmentPayload->legATD2);
        $this->bookingLegSynchronization->setLegATA2($this->bookingShipmentPayload->legATA2);
        $this->bookingLegSynchronization->setLeg2load($this->bookingShipmentPayload->leg2load);
        $this->bookingLegSynchronization->setLeg2disc($this->bookingShipmentPayload->leg2disc);
        $this->bookingLegSynchronization->setMode2($this->bookingShipmentPayload->Mode2);

        $this->bookingLegSynchronization->setLegOrder3($this->bookingShipmentPayload->Leg_Order3);
        $this->bookingLegSynchronization->setVoyageFlight3($this->bookingShipmentPayload->VoyageFlight3);
        $this->bookingLegSynchronization->setVessel3($this->bookingShipmentPayload->Vessel3);
        $this->bookingLegSynchronization->setLegETD3($this->bookingShipmentPayload->legETD3);
        $this->bookingLegSynchronization->setLegETA3($this->bookingShipmentPayload->legETA3);
        $this->bookingLegSynchronization->setLegATD3($this->bookingShipmentPayload->legATD3);
        $this->bookingLegSynchronization->setLegATA3($this->bookingShipmentPayload->legATA3);
        $this->bookingLegSynchronization->setLeg3load($this->bookingShipmentPayload->leg3load);
        $this->bookingLegSynchronization->setLeg3disc($this->bookingShipmentPayload->leg3disc);
        $this->bookingLegSynchronization->setMode3($this->bookingShipmentPayload->Mode3);

        $this->bookingLegSynchronization->setLegOrder4($this->bookingShipmentPayload->Leg_Order4);
        $this->bookingLegSynchronization->setVoyageFlight4($this->bookingShipmentPayload->VoyageFlight4);
        $this->bookingLegSynchronization->setVessel4($this->bookingShipmentPayload->Vessel4);
        $this->bookingLegSynchronization->setLegETD4($this->bookingShipmentPayload->legETD4);
        $this->bookingLegSynchronization->setLegETA4($this->bookingShipmentPayload->legETA4);
        $this->bookingLegSynchronization->setLegATD4($this->bookingShipmentPayload->legATD4);
        $this->bookingLegSynchronization->setLegATA4($this->bookingShipmentPayload->legATA4);
        $this->bookingLegSynchronization->setLeg4load($this->bookingShipmentPayload->leg4load);
        $this->bookingLegSynchronization->setLeg4disc($this->bookingShipmentPayload->leg4disc);
        $this->bookingLegSynchronization->setMode4($this->bookingShipmentPayload->Mode4);

        return $this->bookingLegSynchronization;
    }
}
