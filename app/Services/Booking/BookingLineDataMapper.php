<?php


namespace App\Services\Booking;


use App\Models\DataSyncModels\Booking\BookingLinesSynchronization;

class BookingLineDataMapper
{

    private $bookingLinesSynchronization;
    private $linePayload;

    public function __construct(BookingLinesSynchronization $bookingLinesSynchronization, $linePayload)
    {
        $this->bookingLinesSynchronization = $bookingLinesSynchronization;
        $this->linePayload = $linePayload;
    }

    public function map(): BookingLinesSynchronization
    {
        $this->bookingLinesSynchronization->setJlPk($this->linePayload->JL_PK);
        $this->bookingLinesSynchronization->setLineDescription($this->linePayload->OrderLineDescription);
        $this->bookingLinesSynchronization->setLineOuterPacks($this->linePayload->OrderLineOutterPacks);
        $this->bookingLinesSynchronization->setLineType($this->linePayload->OrderLineType);
        $this->bookingLinesSynchronization->setLineActualWeight($this->linePayload->OrderLineActualWeight);
        $this->bookingLinesSynchronization->setLineUnitofweight($this->linePayload->OrderLineUnitofweight);
        $this->bookingLinesSynchronization->setLineVolume($this->linePayload->OrderLineVolume);
        $this->bookingLinesSynchronization->setLineUnitofVolume($this->linePayload->OrderLineUnitofVolume);
        $this->bookingLinesSynchronization->setLineWidth($this->linePayload->OrderLineWidth);
        $this->bookingLinesSynchronization->setLineHeight($this->linePayload->OrderLineHeight);
        $this->bookingLinesSynchronization->setLineLength($this->linePayload->OrderLineLength);
        $this->bookingLinesSynchronization->setPackJCContainerNum($this->linePayload->Pack_JC_ContainerNum);
        $this->bookingLinesSynchronization->setPackJCSealNum($this->linePayload->Pack_JC_SealNum);
        $this->bookingLinesSynchronization->setPackRCCode($this->linePayload->Pack_RC_Code);

        return $this->bookingLinesSynchronization;
    }

}
