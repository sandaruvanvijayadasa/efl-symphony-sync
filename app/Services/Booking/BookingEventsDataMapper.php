<?php


namespace App\Services\Booking;


use App\Models\DataSyncModels\Booking\BookingEventsSynchronization;

class BookingEventsDataMapper
{

    private $bookingEventsSynchronization;
    private $eventPayload;

    public function __construct(BookingEventsSynchronization $bookingEventsSynchronization, $eventPayload)
    {
        $this->bookingEventsSynchronization = $bookingEventsSynchronization;
        $this->eventPayload = $eventPayload;
    }

    public function map(): array
    {
        $allEventData = array();
        $x = 0;

        $arrivalEventCount = $this->eventPayload->ArrivalEventCount;
        if (intval($arrivalEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('ArrivalEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->ArrivalEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->ArrivalEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $cargoAvailableEventCount = $this->eventPayload->CargoAvailableEventCount;
        if (intval($cargoAvailableEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('CargoAvailableEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->CargoAvailableEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->CargoAvailableEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $cutOffDateEventCount = $this->eventPayload->CutOffDateEventCount;
        if (intval($cutOffDateEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('CutOffDateEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->CutOffDateEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->CutOffDateEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $departureEventCount = $this->eventPayload->DepartureEventCount;
        if (intval($departureEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('DepartureEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->DepartureEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->DepartureEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $dehireEventCount = $this->eventPayload->DehireEventCount;
        if (intval($dehireEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('DehireEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->DehireEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->DehireEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $deliveredEventCount = $this->eventPayload->DeliveredEventCount;
        if (intval($deliveredEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('DeliveredEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->DeliveredEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->DeliveredEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $freightLoadedCount = $this->eventPayload->FreightLoadedCount;
        if (intval($freightLoadedCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('FreightLoadedEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->FreightLoadedEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->FreightLoadedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $freightUnloadedEventCount = $this->eventPayload->FreightUnloadedEventCount;
        if (intval($freightUnloadedEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('FreightUnloadedEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->FreightUnloadedEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->FreightUnloadedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $gateInEventCount = $this->eventPayload->GateInEventCount;
        if (intval($gateInEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('GateInEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->GateInEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->GateInEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $gateOutEventCount = $this->eventPayload->GateOutEventCount;
        if (intval($gateOutEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('GateOutEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->GateOutEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->GateOutEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $handedOverEventCount = $this->eventPayload->HandedOverEventCount;
        if (intval($handedOverEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('HandedOverEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->HandedOverEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->HandedOverEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $quantityVerifiedEventCount = $this->eventPayload->QuantityVerifiedEventCount;
        if (intval($quantityVerifiedEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('QuantityVerifiedEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->QuantityVerifiedEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->QuantityVerifiedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $receivedEventCount = $this->eventPayload->ReceivedEventCount;
        if (intval($receivedEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('ReceivedEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->ReceivedEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->ReceivedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $releasedEventCount = $this->eventPayload->ReleasedEventCount;
        if (intval($releasedEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('ReleasedEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->ReleasedEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->ReleasedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $clearanceCompletedEventCount = $this->eventPayload->ClearanceCompletedEventCount;
        if (intval($clearanceCompletedEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('ClearanceCompletedEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->ClearanceCompletedEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->ClearanceCompletedEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        $heldEventCount = $this->eventPayload->HeldEventCount;
        if (intval($heldEventCount) > 0) {
            $this->bookingEventsSynchronization->setEvenType('HeldEvent');
            $this->bookingEventsSynchronization->setEventDate($this->eventPayload->HeldEventdate);
            $this->bookingEventsSynchronization->setEventDetail($this->eventPayload->HeldEventDetail);

            $allEventData[$x] = [
                'event_type' => $this->bookingEventsSynchronization->getEvenType(),
                'event_date' => $this->bookingEventsSynchronization->getEventDate(),
                'event_detail' => $this->bookingEventsSynchronization->getEventDetail()
            ];
            $x = $x + 1;
        }

        return $allEventData;
    }

}
