<?php


namespace App\Services\Containers;


use App\Models\DataSyncModels\Containers\ContainersSynchronization;

class ContainersDataMapper
{

    private $containersSynchronization;
    private $containersPayload;
    private $containerLineNo;

    public function __construct(ContainersSynchronization $containersSynchronization, $containersPayload, $containerLineNo)
    {
        $this->containersSynchronization = $containersSynchronization;
        $this->containersPayload = $containersPayload;
        $this->containerLineNo = $containerLineNo;
    }

    public function map(): ContainersSynchronization
    {
        $this->containersSynchronization->setContainerCount($this->containersPayload->Containercount);
        $this->containersSynchronization->setContainerLineNo($this->containerLineNo);
        $this->containersSynchronization->setContainerNum($this->containersPayload->ContainerNum);
        $this->containersSynchronization->setSealNum($this->containersPayload->SealNum);
        $this->containersSynchronization->setContainerMode($this->containersPayload->ContainerMode);
        $this->containersSynchronization->setStorageClass($this->containersPayload->StorageClass);
        $this->containersSynchronization->setDeliveryMode($this->containersPayload->DeliveryMode);
        $this->containersSynchronization->setContainerType($this->containersPayload->ContainerType);
        $this->containersSynchronization->setGrossWeight($this->containersPayload->GrossWeight);
        $this->containersSynchronization->setTareWeight($this->containersPayload->TareWeight);
        $this->containersSynchronization->setDepartureEstimatedPickup($this->containersPayload->DepartureEstimatedPickup);
        $this->containersSynchronization->setEmptyRequired($this->containersPayload->EmptyRequired);
        $this->containersSynchronization->setContainerYardEmptyPickupGateOut($this->containersPayload->ContainerYardEmptyPickupGateOut);
        $this->containersSynchronization->setDepartureCartageAdvised($this->containersPayload->DepartureCartageAdvised);
        $this->containersSynchronization->setDepartureCartageComplete($this->containersPayload->DepartureCartageComplete);
        $this->containersSynchronization->setArrivalSlotDateTime($this->containersPayload->ArrivalSlotDateTime);
        $this->containersSynchronization->setArrivalEstimatedDelivery($this->containersPayload->ArrivalEstimatedDelivery);
        $this->containersSynchronization->setArrivalDeliveryRequiredBy($this->containersPayload->ArrivalDeliveryRequiredBy);
        $this->containersSynchronization->setArrivalCartageAdvised($this->containersPayload->ArrivalCartageAdvised);
        $this->containersSynchronization->setArrivalCartageComplete($this->containersPayload->ArrivalCartageComplete);
        $this->containersSynchronization->setFclWharfGateIn($this->containersPayload->FCLWharfGateIn);
        $this->containersSynchronization->setFclOnBoardVessel($this->containersPayload->FCLOnBoardVessel);
        $this->containersSynchronization->setFclUnloadFromVessel($this->containersPayload->FCLUnloadFromVessel);
        $this->containersSynchronization->setFclAvailable($this->containersPayload->FCLAvailable);
        $this->containersSynchronization->setFclWharfGateOut($this->containersPayload->FCLWharfGateOut);
        $this->containersSynchronization->setArrivalCTOStorageStartDate($this->containersPayload->ArrivalCTOStorageStartDate);
        $this->containersSynchronization->setLclUnpack($this->containersPayload->LCLUnpack);
        $this->containersSynchronization->setLclAvailable($this->containersPayload->LCLAvailable);
        $this->containersSynchronization->setEmptyReadyForReturn($this->containersPayload->EmptyReadyForReturn);
        $this->containersSynchronization->setEmptyReturnedBy($this->containersPayload->EmptyReturnedBy);
        $this->containersSynchronization->setContainerYardEmptyReturnGateIn($this->containersPayload->ContainerYardEmptyReturnGateIn);

        return $this->containersSynchronization;
    }

}
