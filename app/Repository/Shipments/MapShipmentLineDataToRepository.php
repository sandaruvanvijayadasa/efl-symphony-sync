<?php


namespace App\Repository\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentLinesSynchronization;
use App\Models\Shipments\ShipmentLines;
use Exception;
use Illuminate\Support\Facades\Log;

class MapShipmentLineDataToRepository
{

    private $shipmentLineData;
    private $poNumber;
    private $shipmentNo;

    public function __construct(ShipmentLinesSynchronization $shipmentLinesSynchronization, $poNumber, $shipmentNo)
    {
        $this->shipmentLineData = $shipmentLinesSynchronization;
        $this->poNumber = $poNumber;
        $this->shipmentNo = $shipmentNo;
    }

    public function updateOrCreate()
    {
        try {
            $lineNo = null;
            $lastLineByPo = ShipmentLines::where([['poNumber', $this->poNumber]])->orderBy('id', 'DESC')->first();
            $sameLine = ShipmentLines::where([['jlPk', $this->shipmentLineData->getJlPk()]])->first();
            if (!is_null($lastLineByPo)) {
                if (is_null($sameLine))
                    $lineNo = intval($lastLineByPo->lineNumber) + 1;
                else
                    $lineNo = $sameLine->lineNumber;
            } else {
                $lineNo = 1;
            }

            $line = ShipmentLines::updateOrCreate(
                ['poNumber' => $this->poNumber, 'jlPk' => $this->shipmentLineData->getJlPk()],
                [
                    'poNumber' => $this->poNumber,
                    'jlPk' => $this->shipmentLineData->getJlPk(),
                    'lineNumber' => $lineNo,
                    'lineDescription' => $this->shipmentLineData->getLineDescription(),
                    'lineOuterPacks' => doubleval($this->shipmentLineData->getLineOuterPacks()),
                    'lineType' => $this->shipmentLineData->getLineType(),
                    'LineActualWeight' => doubleval($this->shipmentLineData->getLineActualWeight()),
                    'LineUnitofweight' => $this->shipmentLineData->getLineUnitofweight(),
                    'LineVolume' => doubleval($this->shipmentLineData->getLineVolume()),
                    'LineUnitofVolume' => $this->shipmentLineData->getLineUnitofVolume(),
                    'LineWidth' => doubleval($this->shipmentLineData->getLineWidth()),
                    'LineHeight' => doubleval($this->shipmentLineData->getLineHeight()),
                    'LineLength' => doubleval($this->shipmentLineData->getLineLength()),
                    'PackJCContainerNum' => $this->shipmentLineData->getPackJCContainerNum(),
                    'PackJCSealNum' => $this->shipmentLineData->getPackJCSealNum(),
                    'PackRCCode' => $this->shipmentLineData->getPackRCCode(),
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Shipment line data updateOrCreate', [$this->shipmentNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
