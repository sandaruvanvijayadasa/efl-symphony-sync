<?php


namespace App\Repository\Shipments;

use App\Models\DataSyncModels\Shipments\ShipmentLegSynchronization;
use App\Models\Shipments\ShipmentLegs;
use Exception;
use Illuminate\Support\Facades\Log;

class MapShipmentLegDataToRepository
{

    private $shipmentLegData;
    private $shipmentId;
    private $shipmentNo;

    public function __construct(ShipmentLegSynchronization $shipmentLegSynchronization, $shipmentId, $shipmentNo)
    {
        $this->shipmentLegData = $shipmentLegSynchronization;
        $this->shipmentId = $shipmentId;
        $this->shipmentNo = $shipmentNo;
    }

    public function updateOrCreate()
    {
        try {
            $leg = ShipmentLegs::updateOrCreate(
                ['shippingsId' => $this->shipmentId],
                [
                    'shippingsId' => $this->shipmentId,
                    'Leg_Order1' => $this->shipmentLegData->getLegOrder1(),
                    'VoyageFlight1' => $this->shipmentLegData->getVoyageFlight1(),
                    'Vessel1' => $this->shipmentLegData->getVessel1(),
                    'legETD1' => $this->shipmentLegData->getLegETD1(),
                    'legETA1' => $this->shipmentLegData->getLegETA1(),
                    'legATD1' => $this->shipmentLegData->getLegATD1(),
                    'legATA1' => $this->shipmentLegData->getLegATA1(),
                    'leg1load' => $this->shipmentLegData->getLeg1load(),
                    'leg1disc' => $this->shipmentLegData->getLeg1disc(),
                    'mode1' => $this->shipmentLegData->getMode1(),
                    'Leg_Order2' => $this->shipmentLegData->getLegOrder2(),
                    'VoyageFlight2' => $this->shipmentLegData->getVoyageFlight2(),
                    'Vessel2' => $this->shipmentLegData->getVessel2(),
                    'legETD2' => $this->shipmentLegData->getLegETD2(),
                    'legETA2' => $this->shipmentLegData->getLegETA2(),
                    'legATD2' => $this->shipmentLegData->getLegATD2(),
                    'legATA2' => $this->shipmentLegData->getLegATA2(),
                    'leg2load' => $this->shipmentLegData->getLeg2load(),
                    'leg2disc' => $this->shipmentLegData->getLeg2disc(),
                    'mode2' => $this->shipmentLegData->getMode2(),
                    'Leg_Order3' => $this->shipmentLegData->getLegOrder3(),
                    'VoyageFlight3' => $this->shipmentLegData->getVoyageFlight3(),
                    'Vessel3' => $this->shipmentLegData->getVessel3(),
                    'legETD3' => $this->shipmentLegData->getLegETD3(),
                    'legETA3' => $this->shipmentLegData->getLegETA3(),
                    'legATD3' => $this->shipmentLegData->getLegATD3(),
                    'legATA3' => $this->shipmentLegData->getLegATA3(),
                    'leg3load' => $this->shipmentLegData->getLeg3load(),
                    'leg3disc' => $this->shipmentLegData->getLeg3disc(),
                    'mode3' => $this->shipmentLegData->getMode3(),
                    'Leg_Order4' => $this->shipmentLegData->getLegOrder4(),
                    'VoyageFlight4' => $this->shipmentLegData->getVoyageFlight4(),
                    'Vessel4' => $this->shipmentLegData->getVessel4(),
                    'legETD4' => $this->shipmentLegData->getLegETD4(),
                    'legETA4' => $this->shipmentLegData->getLegETA4(),
                    'legATD4' => $this->shipmentLegData->getLegATD4(),
                    'legATA4' => $this->shipmentLegData->getLegATA4(),
                    'leg4load' => $this->shipmentLegData->getLeg4load(),
                    'leg4disc' => $this->shipmentLegData->getLeg4disc(),
                    'mode4' => $this->shipmentLegData->getMode4(),
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Shipment leg data updateOrCreate', [$this->shipmentNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
