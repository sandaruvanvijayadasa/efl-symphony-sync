<?php


namespace App\Repository\Shipments;


use App\Models\Shipments\ShipmentEvents;
use Exception;
use Illuminate\Support\Facades\Log;

class MapShipmentEventsDataToRepository
{

    private $shipmentEventData;
    private $shipmentId;
    private $shipmentNo;

    public function __construct($shipmentEventsSynchronization, $shipmentId, $shipmentNo)
    {
        $this->shipmentEventData = $shipmentEventsSynchronization;
        $this->shipmentId = $shipmentId;
        $this->shipmentNo = $shipmentNo;
    }

    public function updateOrCreate()
    {
        try {
            ShipmentEvents::updateOrCreate(
                [
                    'shippingsId' => $this->shipmentId,
                    'evenType' => $this->shipmentEventData['event_type'],
                    'eventDate' => $this->shipmentEventData['event_date'],
                    'eventDetail' => $this->shipmentEventData['event_detail']
                ],
                [
                    'shippingsId' => $this->shipmentId,
                    'evenType' => $this->shipmentEventData['event_type'],
                    'eventDate' => $this->shipmentEventData['event_date'],
                    'eventDetail' => $this->shipmentEventData['event_detail']
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Shipment events data updateOrCreate', [$this->shipmentNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
