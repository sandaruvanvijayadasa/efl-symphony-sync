<?php


namespace App\Repository\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentPurchaseOrderSynchronization;
use App\Models\Shipments\ShipmentPurchaseOrders;
use Exception;
use Illuminate\Support\Facades\Log;

class MapShipmentPurchaseOrderDataToRepository
{

    private $shipmentPurchaseOrderData;
    private $shipmentId;
    private $shipmentNo;

    public function __construct(ShipmentPurchaseOrderSynchronization $shipmentPurchaseOrderSynchronization, $shipmentId, $shipmentNo)
    {
        $this->shipmentPurchaseOrderData = $shipmentPurchaseOrderSynchronization;
        $this->shipmentId = $shipmentId;
        $this->shipmentNo = $shipmentNo;
    }

    public function updateOrCreate()
    {
        try {
            $purchaseOrder = ShipmentPurchaseOrders::updateOrCreate(
                ['shippingsId' => $this->shipmentId, 'orderNumber' => $this->shipmentPurchaseOrderData->getOrderNumber()],
                [
                    'shippingsId' => $this->shipmentId,
                    'orderNumber' => $this->shipmentPurchaseOrderData->getOrderNumber(),
                    'buyerCode' => $this->shipmentPurchaseOrderData->getBuyerCode(),
                    'supplierCode' => $this->shipmentPurchaseOrderData->getSupplierCode(),
                    'confirmNumber' => $this->shipmentPurchaseOrderData->getConfirmNumber(),
                    'invoiceNumber' => $this->shipmentPurchaseOrderData->getInvoiceNumber(),
                    'oderNumberDate' => $this->shipmentPurchaseOrderData->getOderNumberDate(),
                    'invoiceNumberDate' => $this->shipmentPurchaseOrderData->getInvoiceNumberDate(),
                    'confirmNumberDate' => $this->shipmentPurchaseOrderData->getConfirmNumberDate(),
                    'followUpDate' => $this->shipmentPurchaseOrderData->getFollowUpDate(),
                    'requireInstoDate' => $this->shipmentPurchaseOrderData->getRequireInstoDate(),
                    'orderStatus' => $this->shipmentPurchaseOrderData->getOrderStatus(),
                    'requireExWorkDate' => $this->shipmentPurchaseOrderData->getRequireExWorkDate(),
                    'orderGoodDescription' => $this->shipmentPurchaseOrderData->getOrderGoodDescription(),
                    'currency' => $this->shipmentPurchaseOrderData->getCurrency(),
                    'orderServiceLevel' => $this->shipmentPurchaseOrderData->getOrderServiceLevel(),
                    'orderINCOTerm' => $this->shipmentPurchaseOrderData->getOrderINCOTerm(),
                    'AdditionalTerms' => $this->shipmentPurchaseOrderData->getAdditionalTerms(),
                    'OrderTransMode' => $this->shipmentPurchaseOrderData->getOrderTransMode(),
                    'OrderCountryOrigin' => $this->shipmentPurchaseOrderData->getOrderCountryOrigin()
                ]
            );
            return $purchaseOrder->id;
        } catch (Exception $e) {
            Log::error('Exception in Shipment purchase order data updateOrCreate', [$this->shipmentNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
