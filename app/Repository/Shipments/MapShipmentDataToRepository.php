<?php


namespace App\Repository\Shipments;


use App\Models\DataSyncModels\Shipments\ShipmentsSynchronization;
use App\Models\Shipments\Shipments;
use Exception;
use Illuminate\Support\Facades\Log;

class MapShipmentDataToRepository
{

    private $shipmentDetail;

    public function __construct(ShipmentsSynchronization $shipmentsSynchronization)
    {
        $this->shipmentDetail = $shipmentsSynchronization;
    }

    public function updateOrCreate()
    {
        try {
            $shipment = Shipments::updateOrCreate(
                ['JS_UniqueConsignRef' => $this->shipmentDetail->getUniqueConsignRef()],
                [
                    'JS_UniqueConsignRef' => $this->shipmentDetail->getUniqueConsignRef(),
                    'JS_IsForwardRegistered' => $this->shipmentDetail->getIsForwardRegistered(),
                    'BookedPallets' => doubleval($this->shipmentDetail->getBookedPallets()),
                    'BookedWeight' => doubleval($this->shipmentDetail->getBookedWeight()),
                    'JS_TransportMode' => $this->shipmentDetail->getTransportMode(),
                    'JS_UnitOfWeight' => $this->shipmentDetail->getUnitOfWeight(),
                    'JS_UnitOfVolume' => $this->shipmentDetail->getUnitOfVolume(),
                    'JS_UnitOfActualChargeable' => $this->shipmentDetail->getUnitOfActualChargeable(),
                    'JS_F3_NKPackType' => $this->shipmentDetail->getF3NkPackType(),
                    'JS_RS_NKServiceLevel' => $this->shipmentDetail->getRsNkServiceLevel(),
                    'LoadPort' => $this->shipmentDetail->getLoadPort(),
                    'DischargePort' => $this->shipmentDetail->getDischargePort(),
                    'JS_SystemLastEditTimeUtc' => $this->shipmentDetail->getSystemLastEditTimeUtc(),
                    'JS_INCO' => $this->shipmentDetail->getIncoTerm(),
                    'JS_PackingMode' => $this->shipmentDetail->getPackingMode(),
                    'JS_ShipmentType' => $this->shipmentDetail->getShipmentType(),
                    'Carrier_CODE' => $this->shipmentDetail->getCarrierCode(),
                    'GoodsDescription' => $this->shipmentDetail->getGoodsDescription(),
                    'Voyage_Flight' => $this->shipmentDetail->getVoyageFlight(),
                    'vessels' => $this->shipmentDetail->getVessels(),
                    'ETD' => $this->shipmentDetail->getETD(),
                    'ETA' => $this->shipmentDetail->getETA(),
                    'ExpoRepresentative' => $this->shipmentDetail->getExpoRepresentative(),
                    'ShipperRepresentative' => $this->shipmentDetail->getShipperRepresentative(),
                    'JS_RL_NKOrigin' => $this->shipmentDetail->getRlNkOrigin(),
                    'JS_RL_NKDestination' => $this->shipmentDetail->getRlNkDestination(),
                    'JS_ActualChargeable' => doubleval($this->shipmentDetail->getActualChargeable()),
                    'BookedTEU' => doubleval($this->shipmentDetail->getBookedTEU()),
                    'ShippersRef' => $this->shipmentDetail->getShippersRef(),
                    'VehicleNo' => $this->shipmentDetail->getVehicleNo(),
                    'DriverName' => $this->shipmentDetail->getDriverName(),
                    'Received_Date' => $this->shipmentDetail->getReceivedDate(),
                    'CustomsCheck' => $this->shipmentDetail->getCustomsCheck(),
                    'MarksNumber' => $this->shipmentDetail->getMarksNumber(),
                    'Remarks' => $this->shipmentDetail->getRemarks(),
                    'JD_OrderNumber' => $this->shipmentDetail->getOrderNumber(),
                    'JK_MasterBillNum' => $this->shipmentDetail->getMasterBillNum(),
                    'ConsignorCode' => $this->shipmentDetail->getConsignorCode(),
                    'LocalClient' => $this->shipmentDetail->getLocalClient(),
                    'ConsigneeCode' => $this->shipmentDetail->getConsigneeCode(),
                    'ATD' => $this->shipmentDetail->getATD(),
                    'ATA' => $this->shipmentDetail->getATA(),
                    'ConfirmatAirline' => $this->shipmentDetail->getConfirmatAirline(),
                    'JS_ActualWeight' => doubleval($this->shipmentDetail->getActualWeight()),
                    'JS_ActualVolume' => doubleval($this->shipmentDetail->getActualVolume()),
                    'JS_OuterPacks' => doubleval($this->shipmentDetail->getOuterPacks()),
                    'HAWB' => $this->shipmentDetail->getHAWB(),
                    'JS_SystemCreateTimeUtc' => $this->shipmentDetail->getSystemCreateTimeUtc(),
                    'CarrierName' => $this->shipmentDetail->getCarrierName(),
                    'SendAgentCode' => $this->shipmentDetail->getSendAgentCode(),
                    'SendAgentName' => $this->shipmentDetail->getSendAgentName(),
                    'RecvAgentCode' => $this->shipmentDetail->getRecvAgentCode(),
                    'RecvAgentName' => $this->shipmentDetail->getRecvAgentName(),
                    'ActualDeliverydate' => $this->shipmentDetail->getActualDeliveryDate()
                ]
            );
            return $shipment->id;
        } catch (Exception $e) {
            Log::error('Shipments data updateOrCreate', [$this->shipmentDetail->getUniqueConsignRef(), $e->getMessage()]);
        }
    }

}
