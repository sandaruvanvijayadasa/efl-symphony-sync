<?php


namespace App\Repository;


use App\Repository\Containers\MapContainerDataToRepository;
use App\Repository\Shipments\MapShipmentDataToRepository;
use App\Repository\Shipments\MapShipmentEventsDataToRepository;
use App\Repository\Shipments\MapShipmentLegDataToRepository;
use App\Repository\Shipments\MapShipmentLineDataToRepository;
use App\Repository\Shipments\MapShipmentPurchaseOrderDataToRepository;
use Illuminate\Support\Facades\DB;

class ShipmentDataStoreRepository
{

    private $mainShipmentData;
    private $shipmentLegData;
    private $purchaseOrderData;
    private $lineData;
    private $containersData;
    private $shipmentEventData;
    private $shipmentNo;

    public function __construct($shipmentNo, $mainShipmentData, $shipmentLegData, $purchaseOrderData, $lineData, $containersData, $shipmentEventData)
    {
        $this->shipmentNo = $shipmentNo;
        $this->mainShipmentData = $mainShipmentData;
        $this->shipmentLegData = $shipmentLegData;
        $this->purchaseOrderData = $purchaseOrderData;
        $this->lineData = $lineData;
        $this->containersData = $containersData;
        $this->shipmentEventData = $shipmentEventData;
    }

    public function saveShipment()
    {
        DB::transaction(function () {
            //save shipment basic data
            $shipmentId = (new MapShipmentDataToRepository($this->mainShipmentData))->updateOrCreate();

            //save shipment leg data
            (new MapShipmentLegDataToRepository($this->shipmentLegData, $shipmentId, $this->shipmentNo))->updateOrCreate();

            //save purchase order & line data
            $purchaseOrderId = null;
            if (count($this->purchaseOrderData) > 0) {
                foreach ($this->purchaseOrderData as $po) {
                    $purchaseOrderId = (new MapShipmentPurchaseOrderDataToRepository($po, $shipmentId, $this->shipmentNo))->updateOrCreate();
                    //save line data
                    if (count($this->lineData) > 0) {
                        foreach ($this->lineData as $line) {
                            (new MapShipmentLineDataToRepository($line, $purchaseOrderId, $this->shipmentNo))->updateOrCreate();
                        }
                    }
                }
            }

            //save container data
            if (count($this->containersData) > 0) {
                foreach ($this->containersData as $container) {
                    (new MapContainerDataToRepository($container, $shipmentId, $this->shipmentNo))->updateOrCreate();
                }
            }

            //save event data
            if (count($this->shipmentEventData) > 0) {
                foreach ($this->shipmentEventData as $events) {
                    if (count($events) > 0) {
                        foreach ($events as $event) {
                            (new MapShipmentEventsDataToRepository($event, $shipmentId, $this->shipmentNo))->updateOrCreate();
                        }
                    }
                }
            }
        });
    }

}
