<?php


namespace App\Repository;

use App\Jobs\ShipmentSyncingProcess;
use App\Models\Customers\Customers;
use App\Models\Customers\CustomerUsers;
use App\Services\DataSyncProcessManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class SyncFromSQLDbRepository
{

    /**
     * retrieve shipment given all active consignees.
     * this function scheduled hourly.
     *
     * @param array $consignees
     * @param $fromDate
     * @return string
     */
    public function retrieveShipments(array $consignees, $fromDate)
    {
        try {
            $toDate = Carbon::now()->format('Y-m-d H:i:s');
            if (count($consignees) > 0) {
                $response = null;
                foreach ($consignees as $consignee) {
                    //get shipment by date range
                    $response = $this->getShipmentsByDateRange($consignee, $fromDate, $toDate);
                    Log::info("SYNCING DATA | : ",  ['consignee' => $consignee, 'response' =>$response, 'timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
                    if (!is_null($response) && count($response) > 0) {
                        foreach ($response as $item) {
                            ShipmentSyncingProcess::dispatch($item)->onQueue('shipment-process-queue');
                            //(new DataSyncProcessManager($item))->processDataSync();
                        }
                    }
                }
            }

        } catch (Exception $exception) {
            Log::error('Exception in getShipments SyncFromSQLDbRepository', [$exception->getMessage()]);
            return $exception->getMessage();
        }
    }


    /**
     * retrieve shipments by given consignee & from date
     * after sync data to mysql database
     *
     * @param $consignee
     * @param null $fromDate
     * @param null $toDate
     * @return string
     */
    public function getShipmentsAndSyncToDB($consignee, $fromDate = null, $toDate = null)
    {
        try {
            if ($fromDate == null && $toDate == null) {
                $response = $this->getAllShipments($consignee);
            } else {
                $fromDate = Carbon::parse($fromDate)->format('Y-m-d H:i:s');
                if ($toDate == null)
                    $toDate = Carbon::now()->format('Y-m-d H:i:s');
                else
                    $toDate = Carbon::parse($toDate)->format('Y-m-d H:i:s');
                $response = $this->getShipmentsByDateRange($consignee, $fromDate, $toDate);
            }
            if (!is_null($response) && count($response) > 0) {
                foreach ($response as $item) {
                    (new DataSyncProcessManager($item))->processDataSync();
                }
            }

        } catch (Exception $exception) {
            Log::error('Exception in getShipments SyncFromSQLDbRepository', [$exception->getMessage()]);
            return $exception->getMessage();
        }
    }

    /**
     * retrieve all shipments & bookings by given consignee & date range
     * after sync data to mysql database
     *
     * @param $consignee
     * @param null $fromDate
     * @param null $toDate
     * @return array|string
     */
    public function getAllShipmentsByConsigneeAndDateRange($consignee, $fromDate = null, $toDate = null)
    {
        try {
            if ($fromDate == null && $toDate == null) {
                $response = $this->getAllShipments($consignee);
            } else {
                $fromDate = Carbon::parse($fromDate)->format('Y-m-d H:i:s');
                if ($toDate == null)
                    $toDate = Carbon::now()->format('Y-m-d H:i:s');
                else
                    $toDate = Carbon::parse($toDate)->format('Y-m-d H:i:s');
                $response = $this->getShipmentsByDateRange($consignee, $fromDate, $toDate);
            }
            return $response;

        } catch (Exception $exception) {
            Log::error('Exception in getShipments SyncFromSQLDbRepository', [$exception->getMessage()]);
            return $exception->getMessage();
        }
    }

    public function getShipmentHeaderList($consignee, $fromDate = null, $toDate = null)
    {
        try {
            if ($fromDate == null && $toDate == null) {
                $response = $this->getAllShipments($consignee);
            } else {
                $fromDate = Carbon::parse($fromDate)->format('Y-m-d H:i:s');
                if ($toDate == null)
                    $toDate = Carbon::now()->format('Y-m-d H:i:s');
                else
                    $toDate = Carbon::parse($toDate)->format('Y-m-d H:i:s');
                $response = $this->getShipmentsByDateRange($consignee, $fromDate, $toDate);
            }

            return $response;
        } catch (Exception $exception) {
            Log::error('Exception in getShipments SyncFromSQLDbRepository', [$exception->getMessage()]);
        }
    }


    /**
     * get all shipments
     *
     * @param $consignee
     * @return array
     */
    public function getAllShipments($consignee)
    {
        return DB::connection('sqlsrv')
            ->select("select   distinct
                    JS_UniqueConsignRef,          -- Shipmentlevel xml-- --Worked one

                    isnull(Joborderheader.JD_OrderNumber, '' ) as OrderNo,
                    isnull(jobOrgheader.OH_Code, '') as BuyerCode,
                    isnull(OrgHeaderSupplier.OH_Code, '') as SupplierCode,
                    isnull(JD_BookingConfRef, '') as ConfirmNo,
                    isnull(JD_InvoiceNumber, '') as InvoiceNo,
                    isnull(JD_OrderDate, '') as OrderNoDate,
                    isnull(JD_InvoiceDate, '') as InvoiceNoDate,
                    isnull(JD_BookingConfDate, '') as ConfirmNoDate,
                    isnull(JD_FollowUpDate, '') as Followupdate,
                    isnull(JD_DeliveryRequiredBy, '') as Requiredinstoredate,
                    ISNULL(CAST(JD_OrderStatus AS varchar), '')as OrderStatus,
                    isnull(JD_ExWorksRequiredBy, '') as ReqExWorkDate,
                    isnull(JD_OrderGoodsDescription, '') as OrderGoodsDescription,
                    isnull(JD_RX_NKOrderCurrency, '') as Currency,
                    isnull(JD_RS_NKServiceLevel_NI, '') as OrderServiceLevel,
                    ----isnull(JD_IncoTerm, '') as OrderINCOTerm,
                    ISNULL(CAST(JD_IncoTerm AS varchar), '')as OrderINCOTerm,
                    isnull(JD_AdditionalTerms, '') as AdditionalTerms,
                    ISNULL(CAST(JD_TransportMode AS varchar), '')as OrderTransMode,
                    isnull(JD_RN_NKCountryOfSupply, '') as OrderCountryOrigin,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then Ordercount.OrdercountOrder
                    when Joborderheader.JD_OrderNumber IS NULL then PackingOrder.ContainerPackingOrder
                    end ), '') as OrderLineCount,

                    --isnull(PackingOrder.ContainerPackingOrder, '') as OrderLineCount,
                    ----PackingLine

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then CAST(JO_PK AS varchar (max))
                    when Joborderheader.JD_OrderNumber IS NULL then CAST(PackingLine.JL_PK AS varchar (max))
                    end ), '') as JL_PK,

                    --ISNULL(CAST(PackingLine.JL_PK AS varchar (max)), '')as JL_PK,
                    ----ISNULL(CAST(PackingLine.JL_PK AS varchar), '')as JL_PK,
                    ----isnull(PackingLine.JL_PK, '') as JL_PK,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_Description
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Description
                    end ), '') as OrderLineDescription,

                    --isnull(PackingLine.JL_Description, '') as OrderLineDescription,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacks
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_PackageCount
                    end )AS NUMERIC(10,2)), 0)  as OrderLineOutterPacks,

                    ----isnull(JO_OuterPacks, NULL) as OrderLineOutterPacks,   isnull(CAST('JO_OuterPacks' AS DECIMAL(22,8)), '')
                    ----isnull(JO_OuterPacks, 0) as OrderLineOutterPacks,
                    --isnull(PackingLine.JL_PackageCount, 0) as OrderLineOutterPacks,     --JL_PackageCount

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacksUQ
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_F3_NKPackType
                    end ), '') as OrderLineType,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualWeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeight
                    end )AS DECIMAL(18, 2)), 0) as OrderLineActualWeight,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfWeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeightUQ
                    end ), '') as OrderLineUnitofweight,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualVolume
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolume
                    end )AS DECIMAL(18, 2)), 0) as OrderLineVolume,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfVolume
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolumeUQ
                    end ), '') as OrderLineUnitofVolume,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackWidth
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Width
                    end )AS DECIMAL(18, 2)), 0) as OrderLineWidth,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackHeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Height
                    end )AS DECIMAL(18, 2)), 0) as OrderLineHeight,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackLength
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Length
                    end )AS DECIMAL(18, 2)), 0) as OrderLineLength,

                    isnull(JC_ContainerNum, '') as Pack_JC_ContainerNum,
                    isnull(JC_SealNum, '') as Pack_JC_SealNum,
                    isnull(RC_Code, '') as Pack_RC_Code,

                    isnull((case when JS_IsForwardRegistered = 'True' then 'true'
                    when JS_IsForwardRegistered = 'true' then 'true'
                    when JS_IsForwardRegistered = 'False' then 'false'
                    when JS_IsForwardRegistered = 'false' then 'false'
                    end ), '') as JS_IsForwardRegistered,

                    ----JS_OuterPacks AS BookedPallets,
                    ----JS_ActualWeight AS BookedWeight1,
                    isnull(Bkdqty.Quantity1, '') as BookedPallets,
                    ----Bkdwgt.Weight1 as BookedWeight,
                    ----JS_ActualWeight,
                    isnull(Case when Bkdqty.Quantity1 IS NULL then JS_ActualWeight
                    when Bkdqty.Quantity1 IS NOT NULL then Bkdqty.Quantity1
                    end, 0) as BookedWeight,

                    isnull(JS_ActualVolume , 0) as  JS_ActualVolume,   --BookedVolume,
                    isnull(JS_UnitOfVolume, 0) as JS_UnitOfVolume,

                    ISNULL(CAST('1' AS varchar), '')as OrganizationType,

                    ISNULL(CAST(JS_TransportMode AS varchar), '')as JS_TransportMode,
                    isnull(JS_RS_NKServiceLevel, '') as JS_RS_NKServiceLevel,
                    isnull(JS_ShipmentType, '') as JS_ShipmentType,
                    isnull(JS_HouseBill, '') as HAWB,
                    isnull(JS_SystemLastEditTimeUtc, '') as JS_SystemLastEditTimeUtc,
                    ISNULL(CAST(JS_INCO AS varchar), '')as JS_INCO,
                    ISNULL(CAST(JS_PackingMode AS varchar), '')as JS_PackingMode,
                    isnull(JS_GoodsDescription, '') AS GoodsDescription,
                    isnull(SaleRep.GS_FullName, '') as ExpoRepresentative,
                    isnull(SaleRep.GS_EmailAddress, '') as ExpoRepresentativeEmail,
                    isnull(TerminalUser.OC_ContactName, '') as ShipperRepresentative,
                    isnull(TerminalUser.OC_Email, '') as ShipperRepresentativeEmail,
                    isnull(JS_RL_NKOrigin, '') as JS_RL_NKOrigin,
                    isnull(JS_RL_NKDestination, '') as JS_RL_NKDestination,
                    isnull(JS_RL_NKLoadPort, '') As LoadPort,
                    isnull(JS_RL_NKDischargePort, '') As DischargePort,

                    isnull(JV_RV_NKVessel, '') AS Vessel,
                    isnull(JV_VoyageFlight, '') AS VoyageFlight,

                    isnull(ActualDelivery.CartageCompleted, '') as ActualDeliverydate,

                    ----isnull(JS_E_DEP, '') as ETD,
                    ----isnull(JS_E_ARV, '') As ETA,

                    ----isnull(jwtransport.ETD, '') as ETD,
                    ----isnull(jwtransport.ETA, '') As ETA,
                    ----isnull(jwtransport.ATD, '') As ATD,
                    ----isnull(jwtransport.ATA, '') As ATA,

                    isnull(case when JS_IsForwardRegistered = '0' then JS_E_DEP
                    when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NOT NULL  then jwtransport.ETD
                    when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NULL Then JS_E_DEP
                    end, '') as ETD,

                    isnull(case when JS_IsForwardRegistered = '0' then JS_E_ARV
                    when JS_IsForwardRegistered = '1' and  jwtransport.ETA IS NOT NULL then jwtransport.ETA
                    when JS_IsForwardRegistered = '1'  and jwtransport.ETA IS NULL then JS_E_ARV
                    end, '') as ETA,

                    isnull(jwtransport.ATD, '') As ATD,
                    isnull(jwtransport.ATA, '') As ATA,

                    isnull(orgHeaderCarrier.OH_Code, '') AS Carrier_CODE,
                    isnull(orgHeaderCarrier.OH_FullName, '') AS CarrierName,

                    isnull(SendingAgent.OH_Code, '') AS SendAgentCode,
                    isnull(SendingAgent.OH_FullName, '') AS SendAgentName,

                    isnull(ReceivingAgent.OH_Code, '') AS RecvAgentCode,
                    isnull(ReceivingAgent.OH_FullName, '') AS RecvAgentName,

                    isnull(JS_ActualWeight, 0) as    JS_ActualWeight,  --TotalWeight,
                    isnull(JS_UnitOfWeight, 0) as JS_UnitOfWeight,
                    isnull(JS_ActualChargeable, 0) as JS_ActualChargeable,
                    isnull(case when JS_TransportMode = 'AIR' then JS_UnitOfWeight
                    when JS_TransportMode = 'SEA' then JS_UnitOfVolume
                    end, 0) as JS_UnitOfActualChargeable,
                    isnull(JS_OuterPacks, '') AS JS_OuterPacks,      --TotalPallets,
                    isnull(JS_F3_NKPackType, '') AS JS_F3_NKPackType,
                    isnull(JS_BookingReference, '') as ShippersRef,
                    isnull(CusEntryNum.CE_EntryNum, '') as VehicleNo,
                    isnull(DRCusEn.CE_EntryNum, '') as DriverName,

                    isnull(JS_A_RCV, '') as Received_Date,
                    isnull(CustomsCheck1.completedDate, '') as CustomsCheck,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Terminal App Remarks'), '') As Remarks,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobConsol.JK_PK and ST_Table = 'JobConsol' AND ST_Description = 'Terminal App Note'), '') As ConfirmatAirline,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Marks & Numbers'), '')  As MarksNumber ,
                    ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Quantity')  As Booked_Quantity,
                    ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Weight')  As Booked_Weight,

                    isnull(JK_MasterBillNum, '') as JK_MasterBillNum,

                    isnull (CAST(CASE
                        WHEN JS_PackingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        Else  NULL
                    END AS DECIMAL(18, 2)), 0) As TEU,

                    ----isnull(JobShipmentOrgs.JS_E2_OA_OH_NKConsignor, '') As ConsignorCode,
                    isnull(ConsignorOrg.OH_Code, '') as ConsignorCode,
                    isnull(ConsignorOrg.FullName, '') as ConsignorFullName,
                    isnull(ConsignorOrg.Address1, '') as ConsignorFullAddress,
                    isnull(ConsignorOrg.City, '') as ConsignorCity,
                    isnull(ConsignorOrg.State, '') as ConsignorState,
                    isnull(ConsignorOrg.PostCode, '') as ConsignorPostCode,

                    isnull(ConsigneeOrg.OH_Code, '') as ConsigneeCode,
                    isnull(ConsigneeOrg.FullName, '') as ConsigneeFullName,
                    isnull(ConsigneeOrg.Address1, '') as ConsigneeFullAddress,
                    isnull(ConsigneeOrg.City, '') as ConsigneeCity,
                    isnull(ConsigneeOrg.State, '') as ConsigneeState,
                    isnull(ConsigneeOrg.PostCode, '') as ConsigneePostCode,

                    isnull('', '')as LocalClientCode,
                    isnull('', '')as LocalClientFullName,
                    isnull('', '')as LocalClientFullAddress,
                    isnull('', '')as LocalClientCity,
                    isnull('', '')as LocalClientState,
                    isnull('', '')as LocalClientPostCode,

                    isnull(JS_SystemCreateTimeUtc, '') as JS_SystemCreateTimeUtc,

                    isnull(det1.JW_LegOrder, 0) as Leg_Order1,
                    ISNULL(CAST(det1.JW_TransportMode AS varchar), '')as Mode1,
                    isnull(det1.JW_VoyageFlight, '') as VoyageFlight1,   --JW_Vessel
                    isnull(det1.JW_Vessel, '') as Vessel1,
                    --isnull(det1.JW_ETD, '') as legETD1,

                    isnull(FORMAT(convert(datetime2,det1.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD1,

                    --isnull(det1.JW_ETA, '') as legETA1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA1,
                    --isnull(det1.JW_ATD, '') as legATD1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD1,
                    --isnull(det1.JW_ATA, '') as legATA1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA1,
                    isnull(det1.JW_RL_NKLoadPort, '') as leg1load,
                    isnull(det1.JW_RL_NKDiscPort, '') as leg1disc,
                    --isnull(det1.JW_LegNotes, '') as leg1note,

                    isnull(det2.JW_LegOrder, 0) as Leg_Order2,
                    ISNULL(CAST(det2.JW_TransportMode AS varchar), '')as Mode2,
                    isnull(det2.JW_VoyageFlight, '') as VoyageFlight2,
                    isnull(det2.JW_Vessel, '') as Vessel2,
                    --isnull(det2.JW_ETD, '') as legETD2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD2,
                    --isnull(det2.JW_ETA, '') as legETA2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA2,
                    --isnull(det2.JW_ATD, '') as legATD2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD2,
                    --isnull(det2.JW_ATA, '') as legATA2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA2,
                    isnull(det2.JW_RL_NKLoadPort, '') as leg2load,
                    isnull(det2.JW_RL_NKDiscPort, '') as leg2disc,
                    --isnull(det2.JW_LegNotes, '') as leg2note,

                    isnull(det3.JW_LegOrder, 0) as Leg_Order3,
                    ISNULL(CAST(det3.JW_TransportMode AS varchar), '')as Mode3,
                    isnull(det3.JW_VoyageFlight, '') as VoyageFlight3,
                    isnull(det3.JW_Vessel, '') as Vessel3,
                    --isnull(det3.JW_ETD, '') as legETD3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD3,
                    --isnull(det3.JW_ETA, '') as legETA3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA3,
                    --isnull(det3.JW_ATD, '') as legATD3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD3,
                    --isnull(det3.JW_ATA, '') as legATA3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA3,
                    isnull(det3.JW_RL_NKLoadPort, '') as leg3load,
                    isnull(det3.JW_RL_NKDiscPort, '') as leg3disc,
                    --isnull(det3.JW_LegNotes, '') as leg3note,

                    isnull(det4.JW_LegOrder, 0) as Leg_Order4,
                    ISNULL(CAST(det4.JW_TransportMode AS varchar), '')as Mode4,
                    isnull(det4.JW_VoyageFlight, '') as VoyageFlight4,
                    isnull(det4.JW_Vessel, '') as Vessel4,
                    --isnull(det4.JW_ETD, '') as legETD4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD4,
                    --isnull(det4.JW_ETA, '') as legETA4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA4,
                    --isnull(det4.JW_ATD, '') as legATD4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD4,
                    --isnull(det4.JW_ATA, '') as legATA4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA4,
                    isnull(det4.JW_RL_NKLoadPort, '') as leg4load,
                    isnull(det4.JW_RL_NKDiscPort, '') as leg4disc  --S17119077583,S17119077599
                    ----isnull(det4.JW_LegNotes, '') as leg4note

                    --INTO dbo.Final_BulkAccount_MACWELVNS_Destination    --LULUSAGVP

                    --INTO dbo.NewTest_Account_ConsigneeOrG_DKIKA_withPO_22

                    --select top 100 JobShipment.JS_UniqueConsignRef
                    from jobshipment
                    LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                    LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                    left join JobConsolTransport on JW_ParentGUID = JK_PK
                    LEFT JOIN JobHeader on JH_ParentID = JS_PK
                    left JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                    LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK

                    Left join cvw_JobShipmentOrgs JobShipmentOrgs on JobShipmentOrgs.JS_PK =  JobShipment.JS_PK

                    Left join(SELECT
                                  JS_PK                = E2_ParentID,

                                  OH_Code                    = OH_Code,
                                  OH_PK                = OH_PK,
                                          deliveryunloco        =OH_RL_NKClosestPort,
                                  FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                  City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                  [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                  PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                  Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                  Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                  Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                    FROM
                          dbo.JobDocAddress

                    Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                    LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                    LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                    WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CRD' ----Consignor
                           AND E2_ParentTableCode = 'JS'

                           )ConsignorOrg on ConsignorOrg.JS_PK =     CAST(JobShipment.JS_PK AS Varchar(100))

                    Left join(SELECT
                                  JS_PK                = E2_ParentID,

                                  OH_Code                    = OH_Code,
                                  OH_PK                = OH_PK,
                                          deliveryunloco        =OH_RL_NKClosestPort,
                                  FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                  City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                  [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                  PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                  Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                  Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                  Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                    FROM
                           dbo.JobDocAddress

                    Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                    LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                    LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                    WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CED' --Consignee
                           AND E2_ParentTableCode = 'JS' )ConsigneeOrg on ConsigneeOrg.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100))

                    Left join (select JS_PK,
                    JL_PK,
                    JL_Description,
                    JL_PackageCount,
                    JL_F3_NKPackType,
                    JL_ActualWeight,
                    JL_ActualWeightUQ,
                    JL_ActualVolume,
                    JL_ActualVolumeUQ,
                    JL_Width,
                    JL_Height,
                    JL_Length,
                    JC_ContainerNum,
                    JC_SealNum,
                    RC_Code

                    FROM dbo.JobShipment

                    Left Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                    Left JOIN JobContainerPackPivot ON JL_PK = J6_JL
                    Left JOIN JobContainer ON JC_PK = JobContainerPackPivot.J6_JC
                    Left JOIN dbo.RefContainer ON RC_PK = JobContainer.JC_RC
                    --where JS_UniqueConsignRef = 'S12221003242'
                    )as PackingLine on  PackingLine.JS_PK = JobShipment.JS_PK

                    Left join (select JS_PK,count(JL_PK) as ContainerPackingOrder      --  PackingOrder.ContainerPackingOrder  ConCount1
                    from JobShipment
                    inner Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                    --where JS_UniqueConsignRef     = 'S13820078187'
                    group by JS_PK)as PackingOrder on  PackingOrder.JS_PK = JobShipment.JS_PK

                    Left join (select JS_PK,JD_OrderNumber,count(JO_PK) as OrdercountOrder      --  Ordercount.OrdercountOrder  as orderslevelCount
                    from JobShipment
                    Inner JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                    LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK
                    --where JS_UniqueConsignRef = 'S18020038197'
                    group by JS_PK,JD_OrderNumber)as Ordercount on  Ordercount.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100)) and Joborderheader.JD_OrderNumber = Ordercount.JD_OrderNumber

                    Left Join CusEntryNum on CE_ParentID = JobShipment.JS_PK and CE_ParentTable = 'JobShipment' and CE_EntryType = 'VEH'
                    Left Join CusEntryNum DRCusEn on DRCusEn.CE_ParentID = JobShipment.JS_PK and DRCusEn.CE_ParentTable = 'JobShipment' and DRCusEn.CE_EntryType = 'DRV'

                    LEFT JOIN OrgAddress orgAddressCarrier ON orgAddressCarrier.OA_PK = JobConsol.JK_OA_ShippingLineAddress
                    --LEFT JOIN OrgHeader orgHeaderCarrier ON orgHeaderCarrier.OH_PK = orgAddressCarrier.OA_OH
                    LEFT JOIN OrgHeader orgHeaderCarrier ON CAST(orgHeaderCarrier.OH_PK AS Varchar(100)) = orgAddressCarrier.OA_OH

                    --LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CarrierOrgHeader.OH_PK
                    LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CAST(CarrierOrgHeader.OH_PK AS Varchar(100))

                    LEFT JOIN dbo.JobSailing  on JobShipment.JS_JX = JobSailing.JX_PK
                    LEFT JOIN dbo.JobVoyOrigin  on JobSailing.JX_JA = JobVoyOrigin.JA_PK
                    LEFT JOIN dbo.JobVoyDestination  on JobSailing.JX_JB = JobVoyDestination.JB_PK
                    LEFT JOIN dbo.JobVoyage  on JobVoyOrigin.JA_JV = JobVoyage.JV_PK and JobVoyDestination.JB_JV = JobVoyage.JV_PK

                    left join
                    (Select JS_PK,JP_DeliveryCartageCompleted   as CartageCompleted   -- ActualDelivery.CartageCompleted as ActualDeliverydate
                    FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                    INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                    ) ActualDelivery ON ActualDelivery.JS_PK = JobShipment.JS_PK

                    left join
                    (Select JS_PK,MAX(ES_Completed) As completedDate FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                    INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                    INNER JOIN dbo.JobService  ON ES_ParentID = JP_PK
                    Where ES_ServiceCode = 'CUC'   --CUC
                    group by JS_PK) CustomsCheck1 ON CustomsCheck1.JS_PK = JobShipment.JS_PK

                    left join
                    (select Distinct JS_PK,JS_E2_OA_OH_Consignee,GS_FullName,GS_EmailAddress     --SaleRep.GS_FullName as ExpoRepresentative
                    from OrgStaffAssignments
                    Inner JOIN cvw_JobShipmentOrgs ON CAST(O8_OH AS Varchar(100)) = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                    Inner join GlbStaff on GlbStaff.GS_Code = O8_GS_NKPersonResponsible
                    where O8_Role = 'CUS') SaleRep on SaleRep.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                    left join
                    (select Distinct JS_PK,JS_E2_OA_OH_Consignee,OC_ContactName,OC_Email     --TerminalUser.OC_ContactName as ShipperRepresentative
                    from OrgContact
                    LEFT JOIN cvw_JobShipmentOrgs ON OC_OH = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                    where OC_Title = 'Terminal App') TerminalUser on TerminalUser.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                    Left join (select Distinct jobshipment.JS_PK, XV_Data as Quantity1    --Bkdqty.Quantity1 as BookedQuantity
                    from Jobshipment
                    inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Quantity') Bkdqty on Bkdqty.JS_PK = jobshipment.JS_PK

                    Left join (select Distinct jobshipment.JS_PK, XV_Data as Weight1                --Bkdwgt.Weight1 as BookedWeight
                    from Jobshipment
                    inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Weight')  Bkdwgt on Bkdwgt.JS_PK = jobshipment.JS_PK

                    Left JOIN OrgAddress jobOrgAddress on jobOrgAddress.OA_PK = JobOrderHeader.JD_OA_BuyerAddress
                    LEFT JOIN OrgHeader jobOrgheader on jobOrgheader.OH_PK = jobOrgAddress.OA_OH

                    LEFT JOIN OrgAddress OrgAddressSupplier on OrgAddressSupplier.OA_PK = JobOrderHeader.JD_OA_SupplierAddress
                    LEFT JOIN OrgHeader OrgHeaderSupplier on OrgHeaderSupplier.OH_PK = OrgAddressSupplier.OA_OH

                    LEFT JOIN dbo.OrgAddress AS SendingAgentAddress ON SendingAgentAddress.OA_PK = JK_OA_SendingForwarderAddress
                    --LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON SendingAgent.OH_PK = SendingAgentAddress.OA_OH
                    LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON CAST(SendingAgent.OH_PK AS Varchar(100)) = SendingAgentAddress.OA_OH

                    LEFT JOIN dbo.OrgAddress AS ReceivingAgentAddress ON ReceivingAgentAddress.OA_PK = JK_OA_ReceivingForwarderAddress
                    LEFT JOIN dbo.OrgHeader  AS ReceivingAgent        ON CAST(ReceivingAgent.OH_PK AS Varchar(100)) = ReceivingAgentAddress.OA_OH

                    Left Join (select  JobShipment.JS_PK,JK_PK,MIN(JW_ETD) As ETD,MAX(JW_ETA) As ETA,MIN(JW_ATD)As ATD ,MAX(JW_ATA) As ATA
                                FROM JobShipment
                                                                    LEFT JOIN JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                                LEFT JOIN JOBCONSOL JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                                INNER JOIN --[EXKPRD.WISEGRID.NET].[OdysseyEXKPRD].[dbo].
                                                                    [vw_ConsolTransports]  ON vw_ConsolTransports.JW_JK = JOBCONSOL.JK_PK
                                group by JOBCONSOL.JK_PK,JobShipment.JS_PK)jwtransport on jwtransport.JS_PK = JobShipment.JS_PK

                    LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                           from JobShipment
                           LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                           LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                           left join JobConsolTransport on JW_ParentGUID = JK_PK
                           where JW_LegOrder = '1' ) det1 on det1.JS_PK= JobShipment.JS_PK
                                LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='2' ) det2 on det2.JS_PK= JobShipment.JS_PK
                                LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='3' ) det3 on det3.JS_PK= JobShipment.JS_PK
                               LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='4' ) det4 on det4.JS_PK= JobShipment.JS_PK

                    --Left join (Select JS_PK,count(JC_ContainerNum) as ConCount1--,Containercount1.ConCount1 as Containercount
                    --FROM dbo.JobShipment
                    --INNER JOIN dbo.JobConShipLink  ON JS_PK = JN_JS
                    --INNER JOIN dbo.JobConsol  ON JN_JK = JK_PK
                    --INNER JOIN dbo.JobContainer  ON JK_PK = JC_JK
                    --INNER JOIN dbo.RefContainer on JC_RC = RefContainer.RC_PK
                    --group by JS_PK)Containercount1 on Containercount1.JS_PK = JobShipment.JS_PK

                    where ConsigneeOrg.OH_Code in ('" . $consignee . "')
                    and JS_IsCancelled = '0'
                    order by JS_UniqueConsignRef,OrderNo asc;");

    }

    /**
     * get all shipments
     *
     * @param $consignee
     * @param $fromDate
     * @param $toDate
     */
    public function getShipmentsByDateRange($consignee, $fromDate, $toDate)
    {
        return DB::connection('sqlsrv')
            ->select("select   distinct
                    JS_UniqueConsignRef,          -- Shipmentlevel xml-- --Worked one

                    isnull(Joborderheader.JD_OrderNumber, '' ) as OrderNo,
                    isnull(jobOrgheader.OH_Code, '') as BuyerCode,
                    isnull(OrgHeaderSupplier.OH_Code, '') as SupplierCode,
                    isnull(JD_BookingConfRef, '') as ConfirmNo,
                    isnull(JD_InvoiceNumber, '') as InvoiceNo,
                    isnull(JD_OrderDate, '') as OrderNoDate,
                    isnull(JD_InvoiceDate, '') as InvoiceNoDate,
                    isnull(JD_BookingConfDate, '') as ConfirmNoDate,
                    isnull(JD_FollowUpDate, '') as Followupdate,
                    isnull(JD_DeliveryRequiredBy, '') as Requiredinstoredate,
                    ISNULL(CAST(JD_OrderStatus AS varchar), '')as OrderStatus,
                    isnull(JD_ExWorksRequiredBy, '') as ReqExWorkDate,
                    isnull(JD_OrderGoodsDescription, '') as OrderGoodsDescription,
                    isnull(JD_RX_NKOrderCurrency, '') as Currency,
                    isnull(JD_RS_NKServiceLevel_NI, '') as OrderServiceLevel,
                    ----isnull(JD_IncoTerm, '') as OrderINCOTerm,
                    ISNULL(CAST(JD_IncoTerm AS varchar), '')as OrderINCOTerm,
                    isnull(JD_AdditionalTerms, '') as AdditionalTerms,
                    ISNULL(CAST(JD_TransportMode AS varchar), '')as OrderTransMode,
                    isnull(JD_RN_NKCountryOfSupply, '') as OrderCountryOrigin,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then Ordercount.OrdercountOrder
                    when Joborderheader.JD_OrderNumber IS NULL then PackingOrder.ContainerPackingOrder
                    end ), '') as OrderLineCount,

                    --isnull(PackingOrder.ContainerPackingOrder, '') as OrderLineCount,
                    ----PackingLine

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then CAST(JO_PK AS varchar (max))
                    when Joborderheader.JD_OrderNumber IS NULL then CAST(PackingLine.JL_PK AS varchar (max))
                    end ), '') as JL_PK,

                    --ISNULL(CAST(PackingLine.JL_PK AS varchar (max)), '')as JL_PK,
                    ----ISNULL(CAST(PackingLine.JL_PK AS varchar), '')as JL_PK,
                    ----isnull(PackingLine.JL_PK, '') as JL_PK,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_Description
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Description
                    end ), '') as OrderLineDescription,

                    --isnull(PackingLine.JL_Description, '') as OrderLineDescription,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacks
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_PackageCount
                    end )AS NUMERIC(10,2)), 0)  as OrderLineOutterPacks,

                    ----isnull(JO_OuterPacks, NULL) as OrderLineOutterPacks,   isnull(CAST('JO_OuterPacks' AS DECIMAL(22,8)), '')
                    ----isnull(JO_OuterPacks, 0) as OrderLineOutterPacks,
                    --isnull(PackingLine.JL_PackageCount, 0) as OrderLineOutterPacks,     --JL_PackageCount

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacksUQ
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_F3_NKPackType
                    end ), '') as OrderLineType,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualWeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeight
                    end )AS DECIMAL(18, 2)), 0) as OrderLineActualWeight,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfWeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeightUQ
                    end ), '') as OrderLineUnitofweight,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualVolume
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolume
                    end )AS DECIMAL(18, 2)), 0) as OrderLineVolume,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfVolume
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolumeUQ
                    end ), '') as OrderLineUnitofVolume,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackWidth
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Width
                    end )AS DECIMAL(18, 2)), 0) as OrderLineWidth,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackHeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Height
                    end )AS DECIMAL(18, 2)), 0) as OrderLineHeight,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackLength
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Length
                    end )AS DECIMAL(18, 2)), 0) as OrderLineLength,

                    isnull(JC_ContainerNum, '') as Pack_JC_ContainerNum,
                    isnull(JC_SealNum, '') as Pack_JC_SealNum,
                    isnull(RC_Code, '') as Pack_RC_Code,

                    isnull((case when JS_IsForwardRegistered = 'True' then 'true'
                    when JS_IsForwardRegistered = 'true' then 'true'
                    when JS_IsForwardRegistered = 'False' then 'false'
                    when JS_IsForwardRegistered = 'false' then 'false'
                    end ), '') as JS_IsForwardRegistered,

                    ----JS_OuterPacks AS BookedPallets,
                    ----JS_ActualWeight AS BookedWeight1,
                    isnull(Bkdqty.Quantity1, '') as BookedPallets,
                    ----Bkdwgt.Weight1 as BookedWeight,
                    ----JS_ActualWeight,
                    isnull(Case when Bkdqty.Quantity1 IS NULL then JS_ActualWeight
                    when Bkdqty.Quantity1 IS NOT NULL then Bkdqty.Quantity1
                    end, 0) as BookedWeight,

                    isnull(JS_ActualVolume , 0) as  JS_ActualVolume,   --BookedVolume,
                    isnull(JS_UnitOfVolume, 0) as JS_UnitOfVolume,

                    ISNULL(CAST('1' AS varchar), '')as OrganizationType,

                    ISNULL(CAST(JS_TransportMode AS varchar), '')as JS_TransportMode,
                    isnull(JS_RS_NKServiceLevel, '') as JS_RS_NKServiceLevel,
                    isnull(JS_ShipmentType, '') as JS_ShipmentType,
                    isnull(JS_HouseBill, '') as HAWB,
                    isnull(JS_SystemLastEditTimeUtc, '') as JS_SystemLastEditTimeUtc,
                    ISNULL(CAST(JS_INCO AS varchar), '')as JS_INCO,
                    ISNULL(CAST(JS_PackingMode AS varchar), '')as JS_PackingMode,
                    isnull(JS_GoodsDescription, '') AS GoodsDescription,
                    isnull(SaleRep.GS_FullName, '') as ExpoRepresentative,
                    isnull(SaleRep.GS_EmailAddress, '') as ExpoRepresentativeEmail,
                    isnull(TerminalUser.OC_ContactName, '') as ShipperRepresentative,
                    isnull(TerminalUser.OC_Email, '') as ShipperRepresentativeEmail,
                    isnull(JS_RL_NKOrigin, '') as JS_RL_NKOrigin,
                    isnull(JS_RL_NKDestination, '') as JS_RL_NKDestination,
                    isnull(JS_RL_NKLoadPort, '') As LoadPort,
                    isnull(JS_RL_NKDischargePort, '') As DischargePort,

                    isnull(JV_RV_NKVessel, '') AS Vessel,
                    isnull(JV_VoyageFlight, '') AS VoyageFlight,

                    isnull(ActualDelivery.CartageCompleted, '') as ActualDeliverydate,

                    ----isnull(JS_E_DEP, '') as ETD,
                    ----isnull(JS_E_ARV, '') As ETA,

                    ----isnull(jwtransport.ETD, '') as ETD,
                    ----isnull(jwtransport.ETA, '') As ETA,
                    ----isnull(jwtransport.ATD, '') As ATD,
                    ----isnull(jwtransport.ATA, '') As ATA,

                    isnull(case when JS_IsForwardRegistered = '0' then JS_E_DEP
                    when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NOT NULL  then jwtransport.ETD
                    when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NULL Then JS_E_DEP
                    end, '') as ETD,

                    isnull(case when JS_IsForwardRegistered = '0' then JS_E_ARV
                    when JS_IsForwardRegistered = '1' and  jwtransport.ETA IS NOT NULL then jwtransport.ETA
                    when JS_IsForwardRegistered = '1'  and jwtransport.ETA IS NULL then JS_E_ARV
                    end, '') as ETA,

                    isnull(jwtransport.ATD, '') As ATD,
                    isnull(jwtransport.ATA, '') As ATA,

                    isnull(orgHeaderCarrier.OH_Code, '') AS Carrier_CODE,
                    isnull(orgHeaderCarrier.OH_FullName, '') AS CarrierName,

                    isnull(SendingAgent.OH_Code, '') AS SendAgentCode,
                    isnull(SendingAgent.OH_FullName, '') AS SendAgentName,

                    isnull(ReceivingAgent.OH_Code, '') AS RecvAgentCode,
                    isnull(ReceivingAgent.OH_FullName, '') AS RecvAgentName,

                    isnull(JS_ActualWeight, 0) as    JS_ActualWeight,  --TotalWeight,
                    isnull(JS_UnitOfWeight, 0) as JS_UnitOfWeight,
                    isnull(JS_ActualChargeable, 0) as JS_ActualChargeable,
                    isnull(case when JS_TransportMode = 'AIR' then JS_UnitOfWeight
                    when JS_TransportMode = 'SEA' then JS_UnitOfVolume
                    end, 0) as JS_UnitOfActualChargeable,
                    isnull(JS_OuterPacks, '') AS JS_OuterPacks,      --TotalPallets,
                    isnull(JS_F3_NKPackType, '') AS JS_F3_NKPackType,
                    isnull(JS_BookingReference, '') as ShippersRef,
                    isnull(CusEntryNum.CE_EntryNum, '') as VehicleNo,
                    isnull(DRCusEn.CE_EntryNum, '') as DriverName,

                    isnull(JS_A_RCV, '') as Received_Date,
                    isnull(CustomsCheck1.completedDate, '') as CustomsCheck,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Terminal App Remarks'), '') As Remarks,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobConsol.JK_PK and ST_Table = 'JobConsol' AND ST_Description = 'Terminal App Note'), '') As ConfirmatAirline,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Marks & Numbers'), '')  As MarksNumber ,
                    ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Quantity')  As Booked_Quantity,
                    ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Weight')  As Booked_Weight,

                    isnull(JK_MasterBillNum, '') as JK_MasterBillNum,

                    isnull (CAST(CASE
                        WHEN JS_PackingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        Else  NULL
                    END AS DECIMAL(18, 2)), 0) As TEU,

                    ----isnull(JobShipmentOrgs.JS_E2_OA_OH_NKConsignor, '') As ConsignorCode,
                    isnull(ConsignorOrg.OH_Code, '') as ConsignorCode,
                    isnull(ConsignorOrg.FullName, '') as ConsignorFullName,
                    isnull(ConsignorOrg.Address1, '') as ConsignorFullAddress,
                    isnull(ConsignorOrg.City, '') as ConsignorCity,
                    isnull(ConsignorOrg.State, '') as ConsignorState,
                    isnull(ConsignorOrg.PostCode, '') as ConsignorPostCode,

                    isnull(ConsigneeOrg.OH_Code, '') as ConsigneeCode,
                    isnull(ConsigneeOrg.FullName, '') as ConsigneeFullName,
                    isnull(ConsigneeOrg.Address1, '') as ConsigneeFullAddress,
                    isnull(ConsigneeOrg.City, '') as ConsigneeCity,
                    isnull(ConsigneeOrg.State, '') as ConsigneeState,
                    isnull(ConsigneeOrg.PostCode, '') as ConsigneePostCode,

                    isnull('', '')as LocalClientCode,
                    isnull('', '')as LocalClientFullName,
                    isnull('', '')as LocalClientFullAddress,
                    isnull('', '')as LocalClientCity,
                    isnull('', '')as LocalClientState,
                    isnull('', '')as LocalClientPostCode,

                    isnull(JS_SystemCreateTimeUtc, '') as JS_SystemCreateTimeUtc,

                    isnull(det1.JW_LegOrder, 0) as Leg_Order1,
                    ISNULL(CAST(det1.JW_TransportMode AS varchar), '')as Mode1,
                    isnull(det1.JW_VoyageFlight, '') as VoyageFlight1,   --JW_Vessel
                    isnull(det1.JW_Vessel, '') as Vessel1,
                    --isnull(det1.JW_ETD, '') as legETD1,

                    isnull(FORMAT(convert(datetime2,det1.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD1,

                    --isnull(det1.JW_ETA, '') as legETA1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA1,
                    --isnull(det1.JW_ATD, '') as legATD1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD1,
                    --isnull(det1.JW_ATA, '') as legATA1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA1,
                    isnull(det1.JW_RL_NKLoadPort, '') as leg1load,
                    isnull(det1.JW_RL_NKDiscPort, '') as leg1disc,
                    --isnull(det1.JW_LegNotes, '') as leg1note,

                    isnull(det2.JW_LegOrder, 0) as Leg_Order2,
                    ISNULL(CAST(det2.JW_TransportMode AS varchar), '')as Mode2,
                    isnull(det2.JW_VoyageFlight, '') as VoyageFlight2,
                    isnull(det2.JW_Vessel, '') as Vessel2,
                    --isnull(det2.JW_ETD, '') as legETD2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD2,
                    --isnull(det2.JW_ETA, '') as legETA2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA2,
                    --isnull(det2.JW_ATD, '') as legATD2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD2,
                    --isnull(det2.JW_ATA, '') as legATA2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA2,
                    isnull(det2.JW_RL_NKLoadPort, '') as leg2load,
                    isnull(det2.JW_RL_NKDiscPort, '') as leg2disc,
                    --isnull(det2.JW_LegNotes, '') as leg2note,

                    isnull(det3.JW_LegOrder, 0) as Leg_Order3,
                    ISNULL(CAST(det3.JW_TransportMode AS varchar), '')as Mode3,
                    isnull(det3.JW_VoyageFlight, '') as VoyageFlight3,
                    isnull(det3.JW_Vessel, '') as Vessel3,
                    --isnull(det3.JW_ETD, '') as legETD3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD3,
                    --isnull(det3.JW_ETA, '') as legETA3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA3,
                    --isnull(det3.JW_ATD, '') as legATD3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD3,
                    --isnull(det3.JW_ATA, '') as legATA3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA3,
                    isnull(det3.JW_RL_NKLoadPort, '') as leg3load,
                    isnull(det3.JW_RL_NKDiscPort, '') as leg3disc,
                    --isnull(det3.JW_LegNotes, '') as leg3note,

                    isnull(det4.JW_LegOrder, 0) as Leg_Order4,
                    ISNULL(CAST(det4.JW_TransportMode AS varchar), '')as Mode4,
                    isnull(det4.JW_VoyageFlight, '') as VoyageFlight4,
                    isnull(det4.JW_Vessel, '') as Vessel4,
                    --isnull(det4.JW_ETD, '') as legETD4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD4,
                    --isnull(det4.JW_ETA, '') as legETA4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA4,
                    --isnull(det4.JW_ATD, '') as legATD4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD4,
                    --isnull(det4.JW_ATA, '') as legATA4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA4,
                    isnull(det4.JW_RL_NKLoadPort, '') as leg4load,
                    isnull(det4.JW_RL_NKDiscPort, '') as leg4disc  --S17119077583,S17119077599
                    ----isnull(det4.JW_LegNotes, '') as leg4note

                    --INTO dbo.Final_BulkAccount_MACWELVNS_Destination    --LULUSAGVP

                    --INTO dbo.NewTest_Account_ConsigneeOrG_DKIKA_withPO_22

                    --select top 100 JobShipment.JS_UniqueConsignRef
                    from jobshipment
                    LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                    LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                    left join JobConsolTransport on JW_ParentGUID = JK_PK
                    LEFT JOIN JobHeader on JH_ParentID = JS_PK
                    left JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                    LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK

                    Left join cvw_JobShipmentOrgs JobShipmentOrgs on JobShipmentOrgs.JS_PK =  JobShipment.JS_PK

                    Left join(SELECT
                                  JS_PK                = E2_ParentID,

                                  OH_Code                    = OH_Code,
                                  OH_PK                = OH_PK,
                                          deliveryunloco        =OH_RL_NKClosestPort,
                                  FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                  City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                  [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                  PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                  Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                  Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                  Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                    FROM
                          dbo.JobDocAddress

                    Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                    LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                    LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                    WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CRD' ----Consignor
                           AND E2_ParentTableCode = 'JS'

                           )ConsignorOrg on ConsignorOrg.JS_PK =     CAST(JobShipment.JS_PK AS Varchar(100))

                    Left join(SELECT
                                  JS_PK                = E2_ParentID,

                                  OH_Code                    = OH_Code,
                                  OH_PK                = OH_PK,
                                          deliveryunloco        =OH_RL_NKClosestPort,
                                  FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                  City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                  [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                  PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                  Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                  Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                  Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                    FROM
                           dbo.JobDocAddress

                    Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                    LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                    LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                    WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CED' --Consignee
                           AND E2_ParentTableCode = 'JS' )ConsigneeOrg on ConsigneeOrg.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100))

                    Left join (select JS_PK,
                    JL_PK,
                    JL_Description,
                    JL_PackageCount,
                    JL_F3_NKPackType,
                    JL_ActualWeight,
                    JL_ActualWeightUQ,
                    JL_ActualVolume,
                    JL_ActualVolumeUQ,
                    JL_Width,
                    JL_Height,
                    JL_Length,
                    JC_ContainerNum,
                    JC_SealNum,
                    RC_Code

                    FROM dbo.JobShipment

                    Left Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                    Left JOIN JobContainerPackPivot ON JL_PK = J6_JL
                    Left JOIN JobContainer ON JC_PK = JobContainerPackPivot.J6_JC
                    Left JOIN dbo.RefContainer ON RC_PK = JobContainer.JC_RC
                    --where JS_UniqueConsignRef = 'S12221003242'
                    )as PackingLine on  PackingLine.JS_PK = JobShipment.JS_PK

                    Left join (select JS_PK,count(JL_PK) as ContainerPackingOrder      --  PackingOrder.ContainerPackingOrder  ConCount1
                    from JobShipment
                    inner Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                    --where JS_UniqueConsignRef     = 'S13820078187'
                    group by JS_PK)as PackingOrder on  PackingOrder.JS_PK = JobShipment.JS_PK

                    Left join (select JS_PK,JD_OrderNumber,count(JO_PK) as OrdercountOrder      --  Ordercount.OrdercountOrder  as orderslevelCount
                    from JobShipment
                    Inner JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                    LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK
                    --where JS_UniqueConsignRef = 'S18020038197'
                    group by JS_PK,JD_OrderNumber)as Ordercount on  Ordercount.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100)) and Joborderheader.JD_OrderNumber = Ordercount.JD_OrderNumber

                    Left Join CusEntryNum on CE_ParentID = JobShipment.JS_PK and CE_ParentTable = 'JobShipment' and CE_EntryType = 'VEH'
                    Left Join CusEntryNum DRCusEn on DRCusEn.CE_ParentID = JobShipment.JS_PK and DRCusEn.CE_ParentTable = 'JobShipment' and DRCusEn.CE_EntryType = 'DRV'

                    LEFT JOIN OrgAddress orgAddressCarrier ON orgAddressCarrier.OA_PK = JobConsol.JK_OA_ShippingLineAddress
                    --LEFT JOIN OrgHeader orgHeaderCarrier ON orgHeaderCarrier.OH_PK = orgAddressCarrier.OA_OH
                    LEFT JOIN OrgHeader orgHeaderCarrier ON CAST(orgHeaderCarrier.OH_PK AS Varchar(100)) = orgAddressCarrier.OA_OH

                    --LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CarrierOrgHeader.OH_PK
                    LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CAST(CarrierOrgHeader.OH_PK AS Varchar(100))

                    LEFT JOIN dbo.JobSailing  on JobShipment.JS_JX = JobSailing.JX_PK
                    LEFT JOIN dbo.JobVoyOrigin  on JobSailing.JX_JA = JobVoyOrigin.JA_PK
                    LEFT JOIN dbo.JobVoyDestination  on JobSailing.JX_JB = JobVoyDestination.JB_PK
                    LEFT JOIN dbo.JobVoyage  on JobVoyOrigin.JA_JV = JobVoyage.JV_PK and JobVoyDestination.JB_JV = JobVoyage.JV_PK

                    left join
                    (Select JS_PK,JP_DeliveryCartageCompleted   as CartageCompleted   -- ActualDelivery.CartageCompleted as ActualDeliverydate
                    FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                    INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                    ) ActualDelivery ON ActualDelivery.JS_PK = JobShipment.JS_PK

                    left join
                    (Select JS_PK,MAX(ES_Completed) As completedDate FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                    INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                    INNER JOIN dbo.JobService  ON ES_ParentID = JP_PK
                    Where ES_ServiceCode = 'CUC'   --CUC
                    group by JS_PK) CustomsCheck1 ON CustomsCheck1.JS_PK = JobShipment.JS_PK

                    left join
                    (select Distinct JS_PK,JS_E2_OA_OH_Consignee,GS_FullName,GS_EmailAddress     --SaleRep.GS_FullName as ExpoRepresentative
                    from OrgStaffAssignments
                    Inner JOIN cvw_JobShipmentOrgs ON CAST(O8_OH AS Varchar(100)) = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                    Inner join GlbStaff on GlbStaff.GS_Code = O8_GS_NKPersonResponsible
                    where O8_Role = 'CUS') SaleRep on SaleRep.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                    left join
                    (select Distinct JS_PK,JS_E2_OA_OH_Consignee,OC_ContactName,OC_Email     --TerminalUser.OC_ContactName as ShipperRepresentative
                    from OrgContact
                    LEFT JOIN cvw_JobShipmentOrgs ON OC_OH = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                    where OC_Title = 'Terminal App') TerminalUser on TerminalUser.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                    Left join (select Distinct jobshipment.JS_PK, XV_Data as Quantity1    --Bkdqty.Quantity1 as BookedQuantity
                    from Jobshipment
                    inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Quantity') Bkdqty on Bkdqty.JS_PK = jobshipment.JS_PK

                    Left join (select Distinct jobshipment.JS_PK, XV_Data as Weight1                --Bkdwgt.Weight1 as BookedWeight
                    from Jobshipment
                    inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Weight')  Bkdwgt on Bkdwgt.JS_PK = jobshipment.JS_PK

                    Left JOIN OrgAddress jobOrgAddress on jobOrgAddress.OA_PK = JobOrderHeader.JD_OA_BuyerAddress
                    LEFT JOIN OrgHeader jobOrgheader on jobOrgheader.OH_PK = jobOrgAddress.OA_OH

                    LEFT JOIN OrgAddress OrgAddressSupplier on OrgAddressSupplier.OA_PK = JobOrderHeader.JD_OA_SupplierAddress
                    LEFT JOIN OrgHeader OrgHeaderSupplier on OrgHeaderSupplier.OH_PK = OrgAddressSupplier.OA_OH

                    LEFT JOIN dbo.OrgAddress AS SendingAgentAddress ON SendingAgentAddress.OA_PK = JK_OA_SendingForwarderAddress
                    --LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON SendingAgent.OH_PK = SendingAgentAddress.OA_OH
                    LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON CAST(SendingAgent.OH_PK AS Varchar(100)) = SendingAgentAddress.OA_OH

                    LEFT JOIN dbo.OrgAddress AS ReceivingAgentAddress ON ReceivingAgentAddress.OA_PK = JK_OA_ReceivingForwarderAddress
                    LEFT JOIN dbo.OrgHeader  AS ReceivingAgent        ON CAST(ReceivingAgent.OH_PK AS Varchar(100)) = ReceivingAgentAddress.OA_OH

                    Left Join (select  JobShipment.JS_PK,JK_PK,MIN(JW_ETD) As ETD,MAX(JW_ETA) As ETA,MIN(JW_ATD)As ATD ,MAX(JW_ATA) As ATA
                                FROM JobShipment
                                                                    LEFT JOIN JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                                LEFT JOIN JOBCONSOL JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                                INNER JOIN --[EXKPRD.WISEGRID.NET].[OdysseyEXKPRD].[dbo].
                                                                    [vw_ConsolTransports]  ON vw_ConsolTransports.JW_JK = JOBCONSOL.JK_PK
                                group by JOBCONSOL.JK_PK,JobShipment.JS_PK)jwtransport on jwtransport.JS_PK = JobShipment.JS_PK

                    LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                           from JobShipment
                           LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                           LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                           left join JobConsolTransport on JW_ParentGUID = JK_PK
                           where JW_LegOrder = '1' ) det1 on det1.JS_PK= JobShipment.JS_PK
                                LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='2' ) det2 on det2.JS_PK= JobShipment.JS_PK
                                LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='3' ) det3 on det3.JS_PK= JobShipment.JS_PK
                               LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='4' ) det4 on det4.JS_PK= JobShipment.JS_PK

                    --Left join (Select JS_PK,count(JC_ContainerNum) as ConCount1--,Containercount1.ConCount1 as Containercount
                    --FROM dbo.JobShipment
                    --INNER JOIN dbo.JobConShipLink  ON JS_PK = JN_JS
                    --INNER JOIN dbo.JobConsol  ON JN_JK = JK_PK
                    --INNER JOIN dbo.JobContainer  ON JK_PK = JC_JK
                    --INNER JOIN dbo.RefContainer on JC_RC = RefContainer.RC_PK
                    --group by JS_PK)Containercount1 on Containercount1.JS_PK = JobShipment.JS_PK

                    where ConsigneeOrg.OH_Code in ('" . $consignee . "')
                    and JS_IsCancelled = '0'
                    and COALESCE(JobShipment.JS_SystemLastEditTimeUtc, JobConsol.JK_SystemLastEditTimeUtc) between '" . $fromDate . "' and '" . $toDate . "'
                    order by JS_UniqueConsignRef,OrderNo asc;");

    }


    /**
     * get shipment containers
     *
     * @param $shipmentNo
     * @return array
     */
    public function getContainers($shipmentNo)
    {
        try {
            return DB::connection('sqlsrv')
                ->select("select   distinct
                        JS_UniqueConsignRef,       -- Shipmentlevel xml-- --Worked one
                        isnull(JS_SystemCreateTimeUtc, '') as JS_SystemCreateTimeUtc,
                        isnull(Container.JC_ContainerNum, '') as ContainerNum,
                        isnull(Container.JC_SealNum, '') as SealNum,
                        isnull(Container.JC_ContainerMode, '') as ContainerMode,
                        isnull(Container.RC_StorageClass, '') as StorageClass,
                        isnull(Container.JC_DeliveryMode, '') as DeliveryMode,
                        isnull(RTRIM(Container.RC_Code), '') as ContainerType,
                        ISNULL(CAST(Container.JC_GrossWeight AS varchar), '')as GrossWeight,
                        ISNULL(CAST(Container.JC_TareWeight AS varchar), '')as TareWeight,
                        isnull(Containercount1.ConCount1, '') as Containercount,

                        isnull(Container.JC_DepartureEstimatedPickup, '')as DepartureEstimatedPickup,
                        isnull(Container.JC_EmptyRequired, '')as EmptyRequired,
                        isnull(Container.JC_ContainerYardEmptyPickupGateOut, '')as ContainerYardEmptyPickupGateOut,
                        isnull(Container.JC_DepartureCartageAdvised, '')as DepartureCartageAdvised,
                        isnull(Container.JC_DepartureCartageComplete, '')as DepartureCartageComplete,
                        isnull(Container.JC_ArrivalSlotDateTime, '')as ArrivalSlotDateTime,
                        isnull(Container.JC_ArrivalEstimatedDelivery, '')as ArrivalEstimatedDelivery,
                        isnull(Container.JC_ArrivalDeliveryRequiredBy, '')as ArrivalDeliveryRequiredBy,
                        isnull(Container.JC_ArrivalCartageAdvised, '')as ArrivalCartageAdvised,
                        isnull(Container.JC_ArrivalCartageComplete, '')as ArrivalCartageComplete,
                        isnull(Container.JC_FCLWharfGateIn, '')as FCLWharfGateIn,
                        isnull(Container.JC_FCLOnBoardVessel, '')as FCLOnBoardVessel,
                        isnull(Container.JC_FCLUnloadFromVessel, '')as FCLUnloadFromVessel,
                        isnull(Container.JC_FCLAvailable, '')as FCLAvailable,
                        isnull(Container.JC_FCLWharfGateOut, '')as FCLWharfGateOut,
                        isnull(Container.JC_ArrivalCTOStorageStartDate, '')as ArrivalCTOStorageStartDate,
                        isnull(Container.JC_LCLUnpack, '')as LCLUnpack,
                        isnull(Container.JC_LCLAvailable, '')as LCLAvailable,
                        isnull(Container.JC_EmptyReadyForReturn, '')as EmptyReadyForReturn,
                        isnull(Container.JC_EmptyReturnedBy, '')as EmptyReturnedBy,
                        isnull(Container.JC_ContainerYardEmptyReturnGateIn, '')as ContainerYardEmptyReturnGateIn

                        from jobshipment

                        Left join(SELECT
                              JS_PK                = E2_ParentID,
                              OH_Code              = OH_Code,
                              OH_PK                = OH_PK,
                                      deliveryunloco        =OH_RL_NKClosestPort,
                              FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                              City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                              [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                              PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                              Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                              Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                              Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                        FROM dbo.JobDocAddress

                        Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                        LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                        LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                            WHERE
                               E2_AddressSequence = '0'
                               AND E2_AddressType = 'CRD' ----Consignor
                               AND E2_ParentTableCode = 'JS'

                        )ConsignorOrg on ConsignorOrg.JS_PK =     CAST(JobShipment.JS_PK AS Varchar(100))

                        Left join(SELECT
                              JS_PK                = E2_ParentID,

                              OH_Code                    = OH_Code,
                              OH_PK                = OH_PK,
                                      deliveryunloco        =OH_RL_NKClosestPort,
                              FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                              City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                              [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                              PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                              Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                              Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                              Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END


                        FROM dbo.JobDocAddress

                        Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                        LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                        LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                        WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CED' --Consignee
                           AND E2_ParentTableCode = 'JS' )ConsigneeOrg on ConsigneeOrg.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100))

                        Left join (
                        Select JS_PK,JK_UniqueConsignRef,JK_PK,JC_ContainerNum,JC_SealNum,JC_DeliveryMode,JC_ContainerMode,RC_StorageClass,RC_code,JC_GrossWeight,JC_TareWeight--,Containercount1.ConCount1 as Containercount
                        ,JC_DepartureEstimatedPickup,
                        JC_EmptyRequired,
                        JC_ContainerYardEmptyPickupGateOut,
                        JC_DepartureCartageAdvised,
                        JC_DepartureCartageComplete,
                        JC_ArrivalSlotDateTime,
                        JC_ArrivalEstimatedDelivery,
                        JC_ArrivalDeliveryRequiredBy,
                        JC_ArrivalCartageAdvised,
                        JC_ArrivalCartageComplete,
                        JC_FCLWharfGateIn,
                        JC_FCLOnBoardVessel,
                        JC_FCLUnloadFromVessel,
                        JC_FCLAvailable,
                        JC_FCLWharfGateOut,
                        JC_ArrivalCTOStorageStartDate,
                        JC_LCLUnpack,
                        JC_LCLAvailable,
                        JC_EmptyReadyForReturn,
                        JC_EmptyReturnedBy,
                        JC_ContainerYardEmptyReturnGateIn

                        FROM dbo.JobShipment
                        INNER JOIN dbo.JobConShipLink  ON JS_PK = JN_JS
                        INNER JOIN dbo.JobConsol  ON JN_JK = JK_PK
                        INNER JOIN dbo.JobContainer  ON JK_PK = JC_JK
                        INNER JOIN dbo.RefContainer on JC_RC = RefContainer.RC_PK
                        )Container on Container.JS_PK = JobShipment.JS_PK

                        Left join (Select JS_PK,count(JC_ContainerNum) as ConCount1--,Containercount1.ConCount1 as Containercount
                        FROM dbo.JobShipment
                        INNER JOIN dbo.JobConShipLink  ON JS_PK = JN_JS
                        INNER JOIN dbo.JobConsol  ON JN_JK = JK_PK
                        INNER JOIN dbo.JobContainer  ON JK_PK = JC_JK
                        INNER JOIN dbo.RefContainer on JC_RC = RefContainer.RC_PK
                        group by JS_PK)Containercount1 on Containercount1.JS_PK = JobShipment.JS_PK

                        where JS_UniqueConsignRef = '" . $shipmentNo . "'
                        order by JS_UniqueConsignRef asc;");

        } catch (Exception $exception) {
            Log::error('Exception in getContainers SyncFromSQLDbRepository', [$exception->getMessage()]);
        }
    }

    /**
     * get shipment events
     * @param $shipmentNo
     * @return array
     */
    public function getEvents($shipmentNo)
    {
        try {
            return DB::connection('sqlsrv')
                ->select("select   distinct
                        JS_UniqueConsignRef,       -- Shipmentlevel xml-- --Worked one

                        isnull(ARVEvData.ARVCount, '')as ArrivalEventCount,
                        Trim(coalesce(ARVEvData.EventPost_Date, ''))as ArrivalEventdate,
                        Trim(coalesce(ARVEvData.ArrivalEvent, ''))as ArrivalEventDetail,

                        ----CAVEvData.CargoAvEvent    --CAVEvData.EventPost_Date
                        isnull(CAVEvData.CAVCount, '')as CargoAvailableEventCount,
                        Trim(coalesce(CAVEvData.EventPost_Date, ''))as CargoAvailableEventdate,
                        Trim(coalesce(CAVEvData.CargoAvEvent, ''))as CargoAvailableEventDetail,

                        --CutoffData.CutoffEvent    --CutoffData.EventPost_Date
                        isnull(CutoffData.COFCount, '')as CutOffDateEventCount,
                        Trim(coalesce(CutoffData.EventPost_Date, ''))as CutOffDateEventdate,
                        Trim(coalesce(CutoffData.CutoffEvent, ''))as CutOffDateEventDetail,

                        --DEPEvData.DepaturEvent    --DEPEvData.EventPost_Date
                        isnull(DEPEvData.DEPCount, '')as DepartureEventCount,
                        Trim(coalesce(DEPEvData.EventPost_Date, ''))as DepartureEventdate,
                        Trim(coalesce(DEPEvData.DepaturEvent, ''))as DepartureEventDetail,

                        --DHREvData.DHREvent    --DHREvData.EventPost_Date
                        isnull(DHREvData.DHRCount, '')as DehireEventCount,
                        --coalesce( JS_InterimReceipt, '')as DehireEventCount
                        --Trim(coalesce(DHREvData.EventPost_Date, '')) as DehireEventCount,
                        Trim(coalesce(DHREvData.EventPost_Date, ''))as DehireEventdate,
                        Trim(coalesce(DHREvData.DHREvent, ''))as DehireEventDetail,

                        --DLVEvData.DeliveryEvent    --DLVEvData.EventPost_Date
                        isnull(DLVEvData.DLVCount, '')as DeliveredEventCount,
                        Trim(coalesce(DLVEvData.EventPost_Date, ''))as DeliveredEventdate,
                        Trim(coalesce(DLVEvData.DeliveryEvent, ''))as DeliveredEventDetail,

                        --FLOEvData.FlightloadEvent    --FLOEvData.EventPost_Date
                        isnull(FLOEvData.FLOCount, '')as FreightLoadedCount,
                        Trim(coalesce(FLOEvData.EventPost_Date, ''))as FreightLoadedEventdate,
                        Trim(coalesce(FLOEvData.FlightloadEvent , ''))as FreightLoadedEventDetail,

                        --FULEvData.FreightUnLoEvent    --FULEvData.EventPost_Date
                        isnull(FULEvData.FULCount, '')as FreightUnloadedEventCount,
                        Trim(coalesce(FULEvData.EventPost_Date, ''))as FreightUnloadedEventdate,
                        Trim(coalesce(FULEvData.FreightUnLoEvent, ''))as FreightUnloadedEventDetail,

                        isnull(GIEvData.GINCount, '')as GateInEventCount,
                        Trim(coalesce(GIEvData.EventPost_Date, ''))as GateInEventdate,
                        Trim(coalesce(GIEvData.GateinEvent, ''))as GateInEventDetail,

                        --GOUEvData.GateoutEvent    --GOUEvData.EventPost_Date
                        isnull(GOUEvData.GOUCount, '')as GateOutEventCount,
                        Trim(coalesce(GOUEvData.EventPost_Date, ''))as GateOutEventdate,
                        Trim(coalesce(GOUEvData.GateoutEvent, ''))as GateOutEventDetail,

                        --HNVEvData.HandoverEvent    --HNVEvData.EventPost_Date
                        isnull(HNVEvData.HNVCount, '')as HandedOverEventCount,
                        Trim(coalesce(HNVEvData.EventPost_Date, ''))as HandedOverEventdate,
                        Trim(coalesce(HNVEvData.HandoverEvent, ''))as HandedOverEventDetail,

                        --QTVEvData.QtyVeEvent    --QTVEvData.EventPost_Date
                        isnull(QTVEvData.QTVCount, '')as QuantityVerifiedEventCount,
                        Trim(coalesce(QTVEvData.EventPost_Date, ''))as QuantityVerifiedEventdate,
                        Trim(coalesce(QTVEvData.QtyVeEvent , ''))as QuantityVerifiedEventDetail,

                        --RCVEvData.ReceivedEvent    --RCVEvData.EventPost_Date
                        isnull(RCVEvData.RCVCount, '')as ReceivedEventCount,
                        Trim(coalesce(RCVEvData.EventPost_Date, ''))as ReceivedEventdate,
                        Trim(coalesce(RCVEvData.ReceivedEvent, ''))as ReceivedEventDetail,

                        --RLSEvData.ReleaseEvent    --RLSEvData.EventPost_Date
                        isnull(RLSEvData.RLSCount, '')as ReleasedEventCount,
                        Trim(coalesce(RLSEvData.EventPost_Date, ''))as ReleasedEventdate,
                        Trim(coalesce(RLSEvData.ReleaseEvent, ''))as ReleasedEventDetail,

                        --SCMEvData.ClearanceconfEvent    --SCMEvData.EventPost_Date
                        isnull(SCMEvData.SCMCount, '')as ClearanceCompletedEventCount,
                        Trim(coalesce(SCMEvData.EventPost_Date, ''))as ClearanceCompletedEventdate,
                        Trim(coalesce(SCMEvData.ClearanceconfEvent, ''))as ClearanceCompletedEventDetail,

                        --SHLEvData.heldEvent    --SHLEvData.EventPost_Date
                        isnull(SHLEvData.SHLCount, '')as HeldEventCount,    --
                        Trim(coalesce(SHLEvData.EventPost_Date, ''))as HeldEventdate,
                        Trim(coalesce(SHLEvData.heldEvent, ''))as HeldEventDetail

                        --INTO dbo.NewTest_Account_ConsigneeOrG_BRAAPPCMB1_WithPO

                        from jobshipment
                        LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                        LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                        left join JobConsolTransport on JW_ParentGUID = JK_PK
                        LEFT JOIN JobHeader on JH_ParentID = JS_PK
                        LEFT JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                        LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK

                        Left join cvw_JobShipmentOrgs JobShipmentOrgs on JobShipmentOrgs.JS_PK =  JobShipment.JS_PK

                        Left join(SELECT
                                      JS_PK                = E2_ParentID,

                                      OH_Code                    = OH_Code,
                                      OH_PK                = OH_PK,
                                              deliveryunloco        =OH_RL_NKClosestPort,
                                      FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                      City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                      [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                      PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                      Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                      Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                      Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                        FROM
                              dbo.JobDocAddress

                        Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                        LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                        LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                        WHERE
                               E2_AddressSequence = '0'
                               AND E2_AddressType = 'CRD' ----Consignor
                               AND E2_ParentTableCode = 'JS'

                        )ConsignorOrg on ConsignorOrg.JS_PK =     CAST(JobShipment.JS_PK AS Varchar(100))

                        Left join(SELECT
                                      JS_PK                = E2_ParentID,

                                      OH_Code                    = OH_Code,
                                      OH_PK                = OH_PK,
                                              deliveryunloco        =OH_RL_NKClosestPort,
                                      FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                      City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                      [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                      PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                      Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                      Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                      Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                        FROM
                               dbo.JobDocAddress

                        Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                        LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                        LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                        WHERE
                               E2_AddressSequence = '0'
                               AND E2_AddressType = 'CED' --Consignee
                               AND E2_ParentTableCode = 'JS' )ConsigneeOrg on ConsigneeOrg.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100))

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date,--,
                        Jobconsol.JK_PK
                          -- ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) + '  |  ' + substring(SL_Reference,  PATINDEX('%|VFL=%',SL_Reference)+5, 6), ''))) as ArrivalEvent

                        ,case when JS_TransportMode = 'AIR' Then  max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) + '  |  ' + substring(SL_Reference,  PATINDEX('%|VFL=%',SL_Reference)+5, 6), '')))
                         when JS_TransportMode = 'SEA' Then  max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))
                        end as ArrivalEvent ,
                        ARVco.ARVCount1   as ARVCount
                        --,count (ARVco.ARVCount1) as ARVCount
                        FROM StmALog AS T
                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (ARVco1.EventPost_Date) as ARVCount1
                        from JobShipment

                        Inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date

                                                                FROM StmALog AS T
                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        where T.SL_Table in  ('JobConsoltransport','JobConsol')and T.SL_SE_NKEvent = 'ARV' and T.SL_IsEstimate ='N'  and T.SL_Reference NOT LIKE '%propagated%' --and JS_UniqueConsignRef = 'S12220124162'
                        group by JobShipment.JS_PK,SL_PostedTimeUtc
                                                                      --  ,SL_PK
                                   )ARVco1 on ARVco1.JS_PK = JobShipment.JS_PK
                                   -- where JS_UniqueConsignRef = 'S12220124162'
                        group by JobShipment.JS_PK)ARVco on ARVco.JS_PK = JobShipment.JS_PK


                        where  T.SL_Table in  ('JobConsoltransport','JobConsol')and T.SL_SE_NKEvent = 'ARV' and T.SL_IsEstimate ='N'  and T.SL_Reference NOT LIKE '%propagated%'  --and JS_UniqueConsignRef = 'S12220124162'
                                                                        --and JobShipment.JS_UniqueConsignRef  in ('S12220103863')
                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,JS_TransportMode,ARVCount1

                        ) ARVEvData on ARVEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                            ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), ''))) as CargoAvEvent
                                        ,CAVco.CAVCount1   as CAVCount
                                       -- ,count (CAVco.Datecount) as CAVCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (CAVco1.EventPost_Date) as CAVCount1
                        from JobShipment
                        inner join (select Distinct JobShipment.JS_PK
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent
                                        where T.SL_Table in  ('JobConsoltransport','JobConsol')and T.SL_SE_NKEvent = 'CAV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                        group by JobShipment.JS_PK,SL_PostedTimeUtc
                        )CAVco1 on CAVco1.JS_PK = JobShipment.JS_PK

                        group by JobShipment.JS_PK)CAVco on CAVco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table in  ('JobConsoltransport','JobConsol')and T.SL_SE_NKEvent = 'CAV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,CAVco.CAVCount1
                                        ) CAVEvData on CAVEvData.JS_PK = JobShipment.JS_PK

                        LEFT Join(      --Done
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), ''))) as CutoffEvent
                        ,COFco.COFCount1   as COFCount
                        --,count (COFco.Datecount) as COFCount
                                    FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (COFco1.EventPost_Date) as COFCount1
                        from JobShipment

                       inner Join (select Distinct JobShipment.JS_PK
                       --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                       ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                       FROM StmALog AS T
                       Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                       LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                       Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                       Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                       LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                       inner join StmEvent on SE_Code = T.SL_SE_NKEvent
                       where  T.SL_SE_NKEvent = 'COF' and T.SL_IsEstimate ='N'  and T.SL_Reference NOT LIKE '%propagated%'
                       group by JobShipment.JS_PK,SL_PostedTimeUtc
                                  )COFco1 on COFco1.JS_PK = JobShipment.JS_PK

                                  group by JobShipment.JS_PK)COFco on COFco.JS_PK = JobShipment.JS_PK

                        where  T.SL_SE_NKEvent = 'COF' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                         group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,COFco.COFCount1
                        ) CutoffData on CutoffData.JS_PK = JobShipment.JS_PK

                        left join(    --Done
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                           -- ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), ''))) as DepaturEvent
                          -- ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) + '  |  ' + substring(SL_Reference,  PATINDEX('%|VFL=%',SL_Reference)+5, 6), ''))) as DepaturEvent

                        ,case when JS_TransportMode = 'AIR' Then  max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) + '  |  ' + substring(SL_Reference,  PATINDEX('%|VFL=%',SL_Reference)+5, 6), '')))
                         when JS_TransportMode = 'SEA' Then  max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))
                        end as DepaturEvent
                       ,DEPco.DEPCount1 as DEPCount
                       --,count (DEPco.Datecount) as DEPCount
                          FROM StmALog AS T
                            Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                            Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                            LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                            Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                            Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                             LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                             inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (DEPco1.EventPost_Date) as DEPCount1
                        from JobShipment
                        inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                                                                FROM StmALog AS T
                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                         inner join StmEvent on SE_Code = SL_SE_NKEvent

                        where  T.SL_SE_NKEvent = 'DEP' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                        group by JobShipment.JS_PK,SL_PostedTimeUtc

                                                                         )DEPco1 on DEPco1.JS_PK = JobShipment.JS_PK
                                                                        group by JobShipment.JS_PK)DEPco on DEPco.JS_PK = JobShipment.JS_PK

                        where  T.SL_SE_NKEvent = 'DEP' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,JS_TransportMode,DEPco.DEPCount1
                        --order by JobShipment.JS_PK
                        )DEPEvData on DEPEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(  --Done
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                           -- ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), ''))) as DHREvent
                        ,'Equipment '  +   max((isnull(SE_Desc, '') + ' At Contanier Yard '  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), ''))) as DHREvent
                        ,DHRco.DHRCount1 as DHRCount
                          --,count (DHRco.Datecount) as DHRCount
                        FROM StmALog AS T
                            Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                            Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                            LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                            Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                            Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                             LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                             inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (DHRco1.EventPost_Date) as DHRCount1
                        from JobShipment

                        inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                            Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                            Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                            LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                            Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                            Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                             LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                             inner join StmEvent on SE_Code = SL_SE_NKEvent
                             where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'DHR' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                             group by JobShipment.JS_PK,SL_PostedTimeUtc
                                                                            )DHRco1 on DHRco1.JS_PK = JobShipment.JS_PK
                                                                            group by JobShipment.JS_PK)DHRco on DHRco.JS_PK = JobShipment.JS_PK

                            where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'DHR' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                            group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,DHRco.DHRCount1
                                                                            ) DHREvData on DHREvData.JS_PK = JobShipment.JS_PK

                        LEFT join(    --Done
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                           ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as DeliveryEvent
                          ,DLVco.DLVCount1 as DLVCount
                          --,count (DLVco.Datecount) as DLVCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent



                        inner join(
                        select JobShipment.JS_PK, count (DLVco1.EventPost_Date) as DLVCount1
                        from JobShipment

                        inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                 LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                 inner join StmEvent on SE_Code = SL_SE_NKEvent
                                where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'DLV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                group by JobShipment.JS_PK,SL_PostedTimeUtc
                                                     )DLVco1 on DLVco1.JS_PK = JobShipment.JS_PK

                        group by JobShipment.JS_PK
                                                            )DLVco on DLVco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'DLV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,DLVco.DLVCount1 ) DLVEvData on DLVEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(    --Done
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                           ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as FlightloadEvent
                           ,FLOco.FLOCount1 as FLOCount
                           --,count (FLOco.Datecount) as FLOCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (FLOco1.EventPost_Date) as FLOCount1
                        from JobShipment
                        Left Join(select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                                                                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent
                                                                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'FLO' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                                                                        group by JobShipment.JS_PK,SL_PostedTimeUtc )FLOco1 on FLOco1.JS_PK = JobShipment.JS_PK

                        group by JobShipment.JS_PK)FLOco on FLOco.JS_PK = JobShipment.JS_PK

                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'FLO' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,FLOco.FLOCount1

                        ) FLOEvData on FLOEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                           ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as FreightUnLoEvent
                           ,FULco.FULCount1 as FULCount
                           --,count (FULco.Datecount) as FULCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent


                        inner join(
                        select JobShipment.JS_PK, count (FULco1.EventPost_Date) as FULCount1
                        from JobShipment

                        Join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                                                                                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                       Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent


                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'FUL' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,SL_PostedTimeUtc)FULco1 on FULco1.JS_PK = JobShipment.JS_PK

                                        group by JobShipment.JS_PK)FULco on FULco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'FUL' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,FULco.FULCount1
                                                                                        ) FULEvData on FULEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(   --Done
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        -- ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as GateinEvent
                          ,case when substring(SL_Reference,  PATINDEX('%|FAC=%',SL_Reference)+5, 3) = 'CTO' Then  Max((isnull(SE_Desc, '') +'  |  '+  'Terminal'  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  --as FlightloadEvent
                          when substring(SL_Reference,  PATINDEX('%|FAC=%',SL_Reference)+5, 2) = 'CY' Then  Max((isnull(SE_Desc, '') +'  |  '+  'Container'  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  --as FlightloadEvent
                        end as GateinEvent
                        ,GINco.GINCount1 as GINCount
                        -- ,count (GINco.Datecount) as GINCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (GINco1.EventPost_Date) as GINCount1
                        from JobShipment

                        Inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                      FROM StmALog AS T
                                Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                 LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                 inner join StmEvent on SE_Code = SL_SE_NKEvent
                                where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'GIN' and T.SL_IsEstimate ='N'  and T.SL_Reference NOT LIKE '%propagated%'
                                group by JobShipment.JS_PK,SL_PostedTimeUtc)GINco1 on GINco1.JS_PK = JobShipment.JS_PK

                                                                                group by JobShipment.JS_PK)GINco on GINco.JS_PK = JobShipment.JS_PK

                                where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'GIN' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,SL_Reference,GINco.GINCount1
                                                                                ) GIEvData on GIEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(

                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                          --,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as GateoutEvent
                        ,case when substring(SL_Reference,  PATINDEX('%|FAC=%',SL_Reference)+5, 3) = 'CTO' Then  Max((isnull(SE_Desc, '') +'  |  '+  'Terminal'  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  --as FlightloadEvent
                          when substring(SL_Reference,  PATINDEX('%|FAC=%',SL_Reference)+5, 2) = 'CY' Then  Max((isnull(SE_Desc, '') +'  |  '+  'Container'  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  --as FlightloadEvent
                        end as GateoutEvent
                        ,GOUco.GOUCount1 as GOUCount
                        --  ,count (GOUco.Datecount) as GOUCount
                        --,GOUco.Datecount as GOUCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (GOUco1.EventPost_Date) as GOUCount1
                        from JobShipment

                        Inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                      FROM StmALog AS T
                                Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                 LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                 inner join StmEvent on SE_Code = SL_SE_NKEvent
                                where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'GOU' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                group by JobShipment.JS_PK,SL_PostedTimeUtc)GOUco1 on GOUco1.JS_PK = JobShipment.JS_PK

                                                                                group by JobShipment.JS_PK)GOUco on GOUco.JS_PK = JobShipment.JS_PK

                                where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'GOU' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                                                                --and  JobShipment.JS_PK =  'F674215A-5CE2-4858-90F9-0005F86273D3'
                                group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,SL_Reference,GOUco.GOUCount1--,GOUco.Datecount

                        ) GOUEvData on GOUEvData.JS_PK = JobShipment.JS_PK

                                LEFT join(

                                SELECT Distinct JobShipment.JS_PK,
                                Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                                FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                                ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as HandoverEvent
                                ,HNVco.HNVCount1 as HNVCount
                                --,count (HNVco.Datecount) as HNVCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK


                                         LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                         inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (HNVco1.EventPost_Date) as HNVCount1
                        from JobShipment
                        inner join (select Distinct JobShipment.JS_PK
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent
                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'HNV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,SL_PostedTimeUtc)HNVco1 on HNVco1.JS_PK = JobShipment.JS_PK

                                        group by JobShipment.JS_PK)HNVco on HNVco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'HNV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,HNVco.HNVCount1
                                        ) HNVEvData on HNVEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        --,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as QtyVeEvent

                        ,substring(SL_Reference,  PATINDEX('%|TYP=%',SL_Reference)+5, 12) +'  ' + max((isnull(SE_Desc, '') + isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as QtyVeEvent
                        ,QTVco.QTVCount1 as QTVCount
                        --,count (QTVco.Datecount) as QTVCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (QTVco1.EventPost_Date) as QTVCount1
                        from JobShipment

                        Inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent
                                        where T.SL_Table = 'JobConsol' and T.SL_SE_NKEvent = 'QTV' and T.SL_IsEstimate ='N'  and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,SL_PostedTimeUtc
                                                )QTVco1 on QTVco1.JS_PK = JobShipment.JS_PK

                                        group by JobShipment.JS_PK)QTVco on QTVco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table = 'JobConsol' and T.SL_SE_NKEvent = 'QTV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,SL_Reference,QTVco.QTVCount1) QTVEvData on QTVEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        ,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as ReceivedEvent
                        ,RCVco.RCVCount1 as RCVCount
                        --,count (RCVco.Datecount) as RCVCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (RCVco1.EventPost_Date) as RCVCount1
                        from JobShipment

                        inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent
                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'RCV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,SL_PostedTimeUtc)RCVco1 on RCVco1.JS_PK = JobShipment.JS_PK

                                        group by JobShipment.JS_PK)RCVco on RCVco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'RCV' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,RCVco.RCVCount1) RCVEvData on RCVEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        --,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as ReleaseEvent
                        ,case when substring(SL_Reference,  PATINDEX('%|FAC=%',SL_Reference)+5, 3) = 'CTO' Then  Max((isnull(SE_Desc, '') +'  |  '+  'From'  +' '+  'Terminal'  +'  '+  'By'  +' '+ substring(SL_Reference,  PATINDEX('%|DEP=%',SL_Reference)+5, 7)  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  --as FlightloadEvent
                              when substring(SL_Reference,  PATINDEX('%|FAC=%',SL_Reference)+5, 2) = 'CY' Then  Max((isnull(SE_Desc, '') +'  |  '+  'From'  +' '+  'Container'  +'  '+  'By'  +' '+ substring(SL_Reference,  PATINDEX('%|DEP=%',SL_Reference)+5, 7)  +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  --as FlightloadEvent
                        end as ReleaseEvent
                        ,RLSco.RLSCount1 as RLSCount
                        -- ,count (RLSco.Datecount) as RLSCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (RLSco1.EventPost_Date) as RLSCount1
                        from JobShipment

                        inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent
                                        where SL_Table in  ('JobConsoltransport','JobConsol') and SL_SE_NKEvent = 'RLS' and SL_IsEstimate ='N' and SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,SL_PostedTimeUtc)RLSco1 on RLSco1.JS_PK = JobShipment.JS_PK

                                        group by JobShipment.JS_PK )RLSco on RLSco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table in  ('JobConsoltransport','JobConsol') and T.SL_SE_NKEvent = 'RLS' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,SL_Reference,RLSco.RLSCount1
                                        ) RLSEvData on RLSEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        --,(isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')) as ClearanceconfEvent
                        ,(isnull(SE_Desc, '') +' By ' + substring(SL_Reference,  PATINDEX('%|DEP=%',SL_Reference)+5, 7) +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')) + substring(SL_Reference,1, 10) as ClearanceconfEvent
                        ,SCMco.SCMCount1 as SCMCount
                        --,count (SCMco.Datecount) as SCMCount
                            FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (SCMco1.EventPost_Date) as SCMCount1
                        from JobShipment

                        inner join (select Distinct JobShipment.JS_PK--,
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        FROM StmALog AS T
                                Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                inner join StmEvent on SE_Code = SL_SE_NKEvent
                                where  T.SL_SE_NKEvent = 'SCM' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                               group by JobShipment.JS_PK,SL_PostedTimeUtc
                               )SCMco1 on SCMco1.JS_PK = JobShipment.JS_PK

                               group by JobShipment.JS_PK
                               )SCMco on SCMco.JS_PK = JobShipment.JS_PK

                                where  T.SL_SE_NKEvent = 'SCM'   and T.SL_IsEstimate ='N'  and T.SL_Reference NOT LIKE '%propagated%'
                                group by JobShipment.JS_PK,Jobconsol.JK_PK,SL_PostedTimeUtc,SE_Desc,RL_PortName,SL_Reference,SCMco.SCMCount1
                                ) SCMEvData on SCMEvData.JS_PK = JobShipment.JS_PK

                        LEFT join(
                        SELECT Distinct JobShipment.JS_PK,
                        Jobconsol.JK_PK,--CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)) as EventPost_Date--,
                        FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date
                        ,max((isnull(SE_Desc, '') +' by ' + substring(SL_Reference,  PATINDEX('%|DEP=%',SL_Reference)+5, 7) +'  |  '+ isnull(RL_PortName, '')+'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as heldEvent
                        --,max((isnull(SE_Desc, '') +'  |  '+ isnull(RL_PortName, '')  +'  |  '+ isnull(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc)), '')))  as heldEvent
                        ,SHLco.SHLCount1 as SHLCount
                        --,count (SHLco.Datecount) as SHLCount
                        FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent

                        inner join(
                        select JobShipment.JS_PK, count (SHLco1.EventPost_Date) as SHLCount1
                        from JobShipment

                        inner join (select Distinct JobShipment.JS_PK--,count(CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))) as Datecount
                        --,CONVERT(nchar(20), TRY_CONVERT(DateTime, SL_PostedTimeUtc))  as Datecount
                        ,FORMAT(SL_PostedTimeUtc, 'yyyy-MM-ddThh:mm:00Z') as EventPost_Date                                                                FROM StmALog AS T
                                        Left JOIN JobConsoltransport on JW_PK = T.SL_Parent and T.SL_Table = 'JobConsoltransport'
                                                                                        Left JOIN Jobconsol consolEvent on consolEvent.JK_PK = T.SL_Parent  and T.SL_Table = 'JobConsol'

                                        LEFT JOIN Jobconsol on JobConsoltransport.JW_ParentGUID = Jobconsol.JK_PK --or JK_PK = T.SL_Parent and SL_Table = 'JobConsol'

                                        Left JOIN dbo.JobConShipLink  ON Jobconsol.JK_PK = JobConShipLink.JN_JK or consolEvent.JK_PK = JobConShipLink.JN_JK
                                        Inner JOIN JobShipment ON JobConShipLink.JN_JS = JobShipment.JS_PK

                                        LEFT join RefUNLOCO on RL_Code = substring(SL_Reference,  PATINDEX('%|LOC=%',SL_Reference)+5, 5)
                                        inner join StmEvent on SE_Code = SL_SE_NKEvent
                                        where T.SL_Table = 'JobConsol' and T.SL_SE_NKEvent = 'SHL' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,SL_PostedTimeUtc  )SHLco1 on SHLco1.JS_PK = JobShipment.JS_PK

                                        group by JobShipment.JS_PK)SHLco on SHLco.JS_PK = JobShipment.JS_PK

                                        where T.SL_Table = 'JobConsol' and T.SL_SE_NKEvent = 'SHL' and T.SL_IsEstimate ='N' and T.SL_Reference NOT LIKE '%propagated%'
                                        group by JobShipment.JS_PK,Jobconsol.JK_PK,Jobconsol.JK_UniqueConsignRef,SL_PostedTimeUtc,SE_Desc,RL_PortName,SHLco.SHLCount1
                                        ) SHLEvData on SHLEvData.JS_PK = JobShipment.JS_PK       --    SHLEvData.SHLCount

                        where JS_UniqueConsignRef = '" . $shipmentNo . "'
                        and JS_IsCancelled = '0'
                        order by JS_UniqueConsignRef asc;");

        } catch (Exception $exception) {
            Log::error('Exception in getEvents SyncFromSQLDbRepository', [$exception->getMessage()]);
        }
    }


    public function getSingleShipment($shipmentNo)
    {
        try {
            return DB::connection('sqlsrv')
                ->select("select   distinct
                        JS_UniqueConsignRef,          -- Shipmentlevel xml-- --Worked one

                        isnull(Joborderheader.JD_OrderNumber, '' ) as OrderNo,
                        isnull(jobOrgheader.OH_Code, '') as BuyerCode,
                        isnull(OrgHeaderSupplier.OH_Code, '') as SupplierCode,
                        isnull(JD_BookingConfRef, '') as ConfirmNo,
                        isnull(JD_InvoiceNumber, '') as InvoiceNo,
                        isnull(JD_OrderDate, '') as OrderNoDate,
                        isnull(JD_InvoiceDate, '') as InvoiceNoDate,
                        isnull(JD_BookingConfDate, '') as ConfirmNoDate,
                        isnull(JD_FollowUpDate, '') as Followupdate,
                        isnull(JD_DeliveryRequiredBy, '') as Requiredinstoredate,
                        ISNULL(CAST(JD_OrderStatus AS varchar), '')as OrderStatus,
                        isnull(JD_ExWorksRequiredBy, '') as ReqExWorkDate,
                        isnull(JD_OrderGoodsDescription, '') as OrderGoodsDescription,
                        isnull(JD_RX_NKOrderCurrency, '') as Currency,
                        isnull(JD_RS_NKServiceLevel_NI, '') as OrderServiceLevel,
                        ----isnull(JD_IncoTerm, '') as OrderINCOTerm,
                        ISNULL(CAST(JD_IncoTerm AS varchar), '')as OrderINCOTerm,
                        isnull(JD_AdditionalTerms, '') as AdditionalTerms,
                        ISNULL(CAST(JD_TransportMode AS varchar), '')as OrderTransMode,
                        isnull(JD_RN_NKCountryOfSupply, '') as OrderCountryOrigin,

                        isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then Ordercount.OrdercountOrder
                        when Joborderheader.JD_OrderNumber IS NULL then PackingOrder.ContainerPackingOrder
                        end ), '') as OrderLineCount,

                        --isnull(PackingOrder.ContainerPackingOrder, '') as OrderLineCount,
                        ----PackingLine

                        isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then CAST(JO_PK AS varchar (max))
                        when Joborderheader.JD_OrderNumber IS NULL then CAST(PackingLine.JL_PK AS varchar (max))
                        end ), '') as JL_PK,

                        --ISNULL(CAST(PackingLine.JL_PK AS varchar (max)), '')as JL_PK,
                        ----ISNULL(CAST(PackingLine.JL_PK AS varchar), '')as JL_PK,
                        ----isnull(PackingLine.JL_PK, '') as JL_PK,

                        isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_Description
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Description
                        end ), '') as OrderLineDescription,

                        --isnull(PackingLine.JL_Description, '') as OrderLineDescription,

                        isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacks
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_PackageCount
                        end )AS NUMERIC(10,2)), 0)  as OrderLineOutterPacks,

                        ----isnull(JO_OuterPacks, NULL) as OrderLineOutterPacks,   isnull(CAST('JO_OuterPacks' AS DECIMAL(22,8)), '')
                        ----isnull(JO_OuterPacks, 0) as OrderLineOutterPacks,
                        --isnull(PackingLine.JL_PackageCount, 0) as OrderLineOutterPacks,     --JL_PackageCount

                        isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacksUQ
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_F3_NKPackType
                        end ), '') as OrderLineType,

                        isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualWeight
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeight
                        end )AS DECIMAL(18, 2)), 0) as OrderLineActualWeight,

                        isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfWeight
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeightUQ
                        end ), '') as OrderLineUnitofweight,

                        isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualVolume
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolume
                        end )AS DECIMAL(18, 2)), 0) as OrderLineVolume,

                        isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfVolume
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolumeUQ
                        end ), '') as OrderLineUnitofVolume,

                        isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackWidth
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Width
                        end )AS DECIMAL(18, 2)), 0) as OrderLineWidth,

                        isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackHeight
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Height
                        end )AS DECIMAL(18, 2)), 0) as OrderLineHeight,

                        isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackLength
                        when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Length
                        end )AS DECIMAL(18, 2)), 0) as OrderLineLength,

                        isnull((case when JS_IsForwardRegistered = 'True' then 'true'
                        when JS_IsForwardRegistered = 'true' then 'true'
                        when JS_IsForwardRegistered = 'False' then 'false'
                        when JS_IsForwardRegistered = 'false' then 'false'
                        end ), '') as JS_IsForwardRegistered,

                        ----JS_OuterPacks AS BookedPallets,
                        ----JS_ActualWeight AS BookedWeight1,
                        isnull(Bkdqty.Quantity1, '') as BookedPallets,
                        ----Bkdwgt.Weight1 as BookedWeight,
                        ----JS_ActualWeight,
                        isnull(Case when Bkdqty.Quantity1 IS NULL then JS_ActualWeight
                        when Bkdqty.Quantity1 IS NOT NULL then Bkdqty.Quantity1
                        end, 0) as BookedWeight,

                        isnull(JS_ActualVolume , 0) as  JS_ActualVolume,   --BookedVolume,
                        isnull(JS_UnitOfVolume, 0) as JS_UnitOfVolume,

                        ISNULL(CAST('1' AS varchar), '')as OrganizationType,

                        ISNULL(CAST(JS_TransportMode AS varchar), '')as JS_TransportMode,
                        isnull(JS_RS_NKServiceLevel, '') as JS_RS_NKServiceLevel,
                        isnull(JS_ShipmentType, '') as JS_ShipmentType,
                        isnull(JS_HouseBill, '') as HAWB,
                        isnull(JS_SystemLastEditTimeUtc, '') as JS_SystemLastEditTimeUtc,
                        ISNULL(CAST(JS_INCO AS varchar), '')as JS_INCO,
                        ISNULL(CAST(JS_PackingMode AS varchar), '')as JS_PackingMode,
                        isnull(JS_GoodsDescription, '') AS GoodsDescription,
                        isnull(SaleRep.GS_FullName, '') as ExpoRepresentative,
                        isnull(SaleRep.GS_EmailAddress, '') as ExpoRepresentativeEmail,
                        isnull(TerminalUser.OC_ContactName, '') as ShipperRepresentative,
                        isnull(TerminalUser.OC_Email, '') as ShipperRepresentativeEmail,
                        isnull(JS_RL_NKOrigin, '') as JS_RL_NKOrigin,
                        isnull(JS_RL_NKDestination, '') as JS_RL_NKDestination,
                        isnull(JS_RL_NKLoadPort, '') As LoadPort,
                        isnull(JS_RL_NKDischargePort, '') As DischargePort,

                        isnull(JV_RV_NKVessel, '') AS Vessel,
                        isnull(JV_VoyageFlight, '') AS VoyageFlight,

                        ----isnull(JS_E_DEP, '') as ETD,
                        ----isnull(JS_E_ARV, '') As ETA,

                        ----isnull(jwtransport.ETD, '') as ETD,
                        ----isnull(jwtransport.ETA, '') As ETA,
                        ----isnull(jwtransport.ATD, '') As ATD,
                        ----isnull(jwtransport.ATA, '') As ATA,

                        isnull(case when JS_IsForwardRegistered = '0' then JS_E_DEP
                        when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NOT NULL  then jwtransport.ETD
                        when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NULL Then JS_E_DEP
                        end, '') as ETD,

                        isnull(case when JS_IsForwardRegistered = '0' then JS_E_ARV
                        when JS_IsForwardRegistered = '1' and  jwtransport.ETA IS NOT NULL then jwtransport.ETA
                        when JS_IsForwardRegistered = '1'  and jwtransport.ETA IS NULL then JS_E_ARV
                        end, '') as ETA,

                        isnull(jwtransport.ATD, '') As ATD,
                        isnull(jwtransport.ATA, '') As ATA,

                        isnull(orgHeaderCarrier.OH_Code, '') AS Carrier_CODE,
                        isnull(orgHeaderCarrier.OH_FullName, '') AS CarrierName,

                        isnull(SendingAgent.OH_Code, '') AS SendAgentCode,
                        isnull(SendingAgent.OH_FullName, '') AS SendAgentName,

                        isnull(ReceivingAgent.OH_Code, '') AS RecvAgentCode,
                        isnull(ReceivingAgent.OH_FullName, '') AS RecvAgentName,

                        isnull(JS_ActualWeight, 0) as    JS_ActualWeight,  --TotalWeight,
                        isnull(JS_UnitOfWeight, 0) as JS_UnitOfWeight,
                        isnull(JS_ActualChargeable, 0) as JS_ActualChargeable,
                        isnull(case when JS_TransportMode = 'AIR' then JS_UnitOfWeight
                        when JS_TransportMode = 'SEA' then JS_UnitOfVolume
                        end, 0) as JS_UnitOfActualChargeable,
                        isnull(JS_OuterPacks, '') AS JS_OuterPacks,      --TotalPallets,
                        isnull(JS_F3_NKPackType, '') AS JS_F3_NKPackType,
                        isnull(JS_BookingReference, '') as ShippersRef,
                        isnull(CusEntryNum.CE_EntryNum, '') as VehicleNo,
                        isnull(DRCusEn.CE_EntryNum, '') as DriverName,

                        isnull(JS_A_RCV, '') as Received_Date,
                        isnull(CustomsCheck1.completedDate, '') as CustomsCheck,
                        isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Terminal App Remarks'), '') As Remarks,
                        isnull((Select ST_NoteText from StmNote where ST_ParentID = JobConsol.JK_PK and ST_Table = 'JobConsol' AND ST_Description = 'Terminal App Note'), '') As ConfirmatAirline,
                        isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Marks & Numbers'), '')  As MarksNumber ,
                        ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Quantity')  As Booked_Quantity,
                        ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Weight')  As Booked_Weight,

                        isnull(JK_MasterBillNum, '') as JK_MasterBillNum,

                        isnull (CAST(CASE
                            WHEN JS_PackingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                            WHEN JS_PAckingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                            WHEN JS_PackingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                            WHEN JS_PAckingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                            WHEN JS_PackingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                            WHEN JS_PAckingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                            WHEN JS_PackingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                            WHEN JS_PAckingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                            WHEN JS_PackingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                            WHEN JS_PAckingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                            Else  NULL
                        END AS DECIMAL(18, 2)), 0) As TEU,

                        ----isnull(JobShipmentOrgs.JS_E2_OA_OH_NKConsignor, '') As ConsignorCode,
                        isnull(ConsignorOrg.OH_Code, '') as ConsignorCode,
                        isnull(ConsignorOrg.FullName, '') as ConsignorFullName,
                        isnull(ConsignorOrg.Address1, '') as ConsignorFullAddress,
                        isnull(ConsignorOrg.City, '') as ConsignorCity,
                        isnull(ConsignorOrg.State, '') as ConsignorState,
                        isnull(ConsignorOrg.PostCode, '') as ConsignorPostCode,

                        isnull(ConsigneeOrg.OH_Code, '') as ConsigneeCode,
                        isnull(ConsigneeOrg.FullName, '') as ConsigneeFullName,
                        isnull(ConsigneeOrg.Address1, '') as ConsigneeFullAddress,
                        isnull(ConsigneeOrg.City, '') as ConsigneeCity,
                        isnull(ConsigneeOrg.State, '') as ConsigneeState,
                        isnull(ConsigneeOrg.PostCode, '') as ConsigneePostCode,

                        isnull('', '')as LocalClientCode,
                        isnull('', '')as LocalClientFullName,
                        isnull('', '')as LocalClientFullAddress,
                        isnull('', '')as LocalClientCity,
                        isnull('', '')as LocalClientState,
                        isnull('', '')as LocalClientPostCode,

                        isnull(JS_SystemCreateTimeUtc, '') as JS_SystemCreateTimeUtc,

                        isnull(det1.JW_LegOrder, 0) as Leg_Order1,
                        ISNULL(CAST(det1.JW_TransportMode AS varchar), '')as Mode1,
                        isnull(det1.JW_VoyageFlight, '') as VoyageFlight1,   --JW_Vessel
                        isnull(det1.JW_Vessel, '') as Vessel1,
                        --isnull(det1.JW_ETD, '') as legETD1,

                        isnull(FORMAT(convert(datetime2,det1.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD1,

                        --isnull(det1.JW_ETA, '') as legETA1,
                        isnull(FORMAT(convert(datetime2,det1.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA1,
                        --isnull(det1.JW_ATD, '') as legATD1,
                        isnull(FORMAT(convert(datetime2,det1.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD1,
                        --isnull(det1.JW_ATA, '') as legATA1,
                        isnull(FORMAT(convert(datetime2,det1.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA1,
                        isnull(det1.JW_RL_NKLoadPort, '') as leg1load,
                        isnull(det1.JW_RL_NKDiscPort, '') as leg1disc,
                        --isnull(det1.JW_LegNotes, '') as leg1note,

                        isnull(det2.JW_LegOrder, 0) as Leg_Order2,
                        ISNULL(CAST(det2.JW_TransportMode AS varchar), '')as Mode2,
                        isnull(det2.JW_VoyageFlight, '') as VoyageFlight2,
                        isnull(det2.JW_Vessel, '') as Vessel2,
                        --isnull(det2.JW_ETD, '') as legETD2,
                        isnull(FORMAT(convert(datetime2,det2.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD2,
                        --isnull(det2.JW_ETA, '') as legETA2,
                        isnull(FORMAT(convert(datetime2,det2.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA2,
                        --isnull(det2.JW_ATD, '') as legATD2,
                        isnull(FORMAT(convert(datetime2,det2.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD2,
                        --isnull(det2.JW_ATA, '') as legATA2,
                        isnull(FORMAT(convert(datetime2,det2.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA2,
                        isnull(det2.JW_RL_NKLoadPort, '') as leg2load,
                        isnull(det2.JW_RL_NKDiscPort, '') as leg2disc,
                        --isnull(det2.JW_LegNotes, '') as leg2note,

                        isnull(det3.JW_LegOrder, 0) as Leg_Order3,
                        ISNULL(CAST(det3.JW_TransportMode AS varchar), '')as Mode3,
                        isnull(det3.JW_VoyageFlight, '') as VoyageFlight3,
                        isnull(det3.JW_Vessel, '') as Vessel3,
                        --isnull(det3.JW_ETD, '') as legETD3,
                        isnull(FORMAT(convert(datetime2,det3.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD3,
                        --isnull(det3.JW_ETA, '') as legETA3,
                        isnull(FORMAT(convert(datetime2,det3.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA3,
                        --isnull(det3.JW_ATD, '') as legATD3,
                        isnull(FORMAT(convert(datetime2,det3.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD3,
                        --isnull(det3.JW_ATA, '') as legATA3,
                        isnull(FORMAT(convert(datetime2,det3.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA3,
                        isnull(det3.JW_RL_NKLoadPort, '') as leg3load,
                        isnull(det3.JW_RL_NKDiscPort, '') as leg3disc,
                        --isnull(det3.JW_LegNotes, '') as leg3note,

                        isnull(det4.JW_LegOrder, 0) as Leg_Order4,
                        ISNULL(CAST(det4.JW_TransportMode AS varchar), '')as Mode4,
                        isnull(det4.JW_VoyageFlight, '') as VoyageFlight4,
                        isnull(det4.JW_Vessel, '') as Vessel4,
                        --isnull(det4.JW_ETD, '') as legETD4,
                        isnull(FORMAT(convert(datetime2,det4.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD4,
                        --isnull(det4.JW_ETA, '') as legETA4,
                        isnull(FORMAT(convert(datetime2,det4.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA4,
                        --isnull(det4.JW_ATD, '') as legATD4,
                        isnull(FORMAT(convert(datetime2,det4.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD4,
                        --isnull(det4.JW_ATA, '') as legATA4,
                        isnull(FORMAT(convert(datetime2,det4.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA4,
                        isnull(det4.JW_RL_NKLoadPort, '') as leg4load,
                        isnull(det4.JW_RL_NKDiscPort, '') as leg4disc  --S17119077583,S17119077599
                        ----isnull(det4.JW_LegNotes, '') as leg4note

                        --INTO dbo.Final_BulkAccount_MACWELVNS_Destination    --LULUSAGVP

                        --INTO dbo.NewTest_Account_ConsigneeOrG_DKIKA_withPO_22

                        --select top 100 JobShipment.JS_UniqueConsignRef
                        from jobshipment
                        LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                        LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                        left join JobConsolTransport on JW_ParentGUID = JK_PK
                        LEFT JOIN JobHeader on JH_ParentID = JS_PK
                        left JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                        LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK

                        Left join cvw_JobShipmentOrgs JobShipmentOrgs on JobShipmentOrgs.JS_PK =  JobShipment.JS_PK

                        Left join(SELECT
                              JS_PK                = E2_ParentID,

                              OH_Code                    = OH_Code,
                              OH_PK                = OH_PK,
                                      deliveryunloco        =OH_RL_NKClosestPort,
                              FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                              City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                              [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                              PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                              Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                              Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                              Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END


                        FROM
                            dbo.JobDocAddress

                            Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                            LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                            LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                            WHERE
                               E2_AddressSequence = '0'
                               AND E2_AddressType = 'CRD' ----Consignor
                               AND E2_ParentTableCode = 'JS'

                            )ConsignorOrg on ConsignorOrg.JS_PK =     CAST(JobShipment.JS_PK AS Varchar(100))

                        Left join(SELECT
                              JS_PK                = E2_ParentID,

                              OH_Code                    = OH_Code,
                              OH_PK                = OH_PK,
                                      deliveryunloco        =OH_RL_NKClosestPort,
                              FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                              City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                              [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                              PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                              Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                              Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                              Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                        FROM
                            dbo.JobDocAddress

                        Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                        LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                        LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                        WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CED' --Consignee
                           AND E2_ParentTableCode = 'JS' )ConsigneeOrg on ConsigneeOrg.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100))

                        Left join (select JS_PK,
                        JL_PK,
                        JL_Description,
                        JL_PackageCount,
                        JL_F3_NKPackType,
                        JL_ActualWeight,
                        JL_ActualWeightUQ,
                        JL_ActualVolume,
                        JL_ActualVolumeUQ,
                        JL_Width,
                        JL_Height,
                        JL_Length
                        FROM dbo.JobShipment

                        inner Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                        --where JS_UniqueConsignRef = 'S10220122137'
                        )as PackingLine on  PackingLine.JS_PK = JobShipment.JS_PK


                        Left join (select JS_PK,count(JL_PK) as ContainerPackingOrder      --  PackingOrder.ContainerPackingOrder  ConCount1
                        from JobShipment
                        inner Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                        --where JS_UniqueConsignRef     = 'S13820078187'
                        group by JS_PK)as PackingOrder on  PackingOrder.JS_PK = JobShipment.JS_PK


                        Left join (select JS_PK,JD_OrderNumber,count(JO_PK) as OrdercountOrder      --  Ordercount.OrdercountOrder  as orderslevelCount
                        from JobShipment
                        Inner JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                        LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK
                        --where JS_UniqueConsignRef = 'S18020038197'
                        group by JS_PK,JD_OrderNumber)as Ordercount on  Ordercount.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100)) and Joborderheader.JD_OrderNumber = Ordercount.JD_OrderNumber


                        Left Join CusEntryNum on CE_ParentID = JobShipment.JS_PK and CE_ParentTable = 'JobShipment' and CE_EntryType = 'VEH'
                        Left Join CusEntryNum DRCusEn on DRCusEn.CE_ParentID = JobShipment.JS_PK and DRCusEn.CE_ParentTable = 'JobShipment' and DRCusEn.CE_EntryType = 'DRV'


                        LEFT JOIN OrgAddress orgAddressCarrier ON orgAddressCarrier.OA_PK = JobConsol.JK_OA_ShippingLineAddress
                        --LEFT JOIN OrgHeader orgHeaderCarrier ON orgHeaderCarrier.OH_PK = orgAddressCarrier.OA_OH
                        LEFT JOIN OrgHeader orgHeaderCarrier ON CAST(orgHeaderCarrier.OH_PK AS Varchar(100)) = orgAddressCarrier.OA_OH

                        --LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CarrierOrgHeader.OH_PK
                        LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CAST(CarrierOrgHeader.OH_PK AS Varchar(100))

                        LEFT JOIN dbo.JobSailing  on JobShipment.JS_JX = JobSailing.JX_PK
                        LEFT JOIN dbo.JobVoyOrigin  on JobSailing.JX_JA = JobVoyOrigin.JA_PK
                        LEFT JOIN dbo.JobVoyDestination  on JobSailing.JX_JB = JobVoyDestination.JB_PK
                        LEFT JOIN dbo.JobVoyage  on JobVoyOrigin.JA_JV = JobVoyage.JV_PK and JobVoyDestination.JB_JV = JobVoyage.JV_PK


                        left join
                        (Select JS_PK,MAX(ES_Completed) As completedDate FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                        INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                        INNER JOIN dbo.JobService  ON ES_ParentID = JP_PK
                        Where ES_ServiceCode = 'CUC'   --CUC
                        group by JS_PK) CustomsCheck1 ON CustomsCheck1.JS_PK = JobShipment.JS_PK

                        left join
                        (select Distinct JS_PK,JS_E2_OA_OH_Consignee,GS_FullName,GS_EmailAddress     --SaleRep.GS_FullName as ExpoRepresentative
                        from OrgStaffAssignments
                        Inner JOIN cvw_JobShipmentOrgs ON CAST(O8_OH AS Varchar(100)) = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                        Inner join GlbStaff on GlbStaff.GS_Code = O8_GS_NKPersonResponsible
                        where O8_Role = 'CUS') SaleRep on SaleRep.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                        left join
                        (select Distinct JS_PK,JS_E2_OA_OH_Consignee,OC_ContactName,OC_Email     --TerminalUser.OC_ContactName as ShipperRepresentative
                        from OrgContact
                        LEFT JOIN cvw_JobShipmentOrgs ON OC_OH = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                        where OC_Title = 'Terminal App') TerminalUser on TerminalUser.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                        Left join (select Distinct jobshipment.JS_PK, XV_Data as Quantity1    --Bkdqty.Quantity1 as BookedQuantity
                        from Jobshipment
                        inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Quantity') Bkdqty on Bkdqty.JS_PK = jobshipment.JS_PK

                        Left join (select Distinct jobshipment.JS_PK, XV_Data as Weight1                --Bkdwgt.Weight1 as BookedWeight
                        from Jobshipment
                        inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Weight')  Bkdwgt on Bkdwgt.JS_PK = jobshipment.JS_PK


                        Left JOIN OrgAddress jobOrgAddress on jobOrgAddress.OA_PK = JobOrderHeader.JD_OA_BuyerAddress
                        LEFT JOIN OrgHeader jobOrgheader on jobOrgheader.OH_PK = jobOrgAddress.OA_OH

                        LEFT JOIN OrgAddress OrgAddressSupplier on OrgAddressSupplier.OA_PK = JobOrderHeader.JD_OA_SupplierAddress
                        LEFT JOIN OrgHeader OrgHeaderSupplier on OrgHeaderSupplier.OH_PK = OrgAddressSupplier.OA_OH

                        LEFT JOIN dbo.OrgAddress AS SendingAgentAddress ON SendingAgentAddress.OA_PK = JK_OA_SendingForwarderAddress
                        --LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON SendingAgent.OH_PK = SendingAgentAddress.OA_OH
                        LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON CAST(SendingAgent.OH_PK AS Varchar(100)) = SendingAgentAddress.OA_OH

                        LEFT JOIN dbo.OrgAddress AS ReceivingAgentAddress ON ReceivingAgentAddress.OA_PK = JK_OA_ReceivingForwarderAddress
                        LEFT JOIN dbo.OrgHeader  AS ReceivingAgent        ON CAST(ReceivingAgent.OH_PK AS Varchar(100)) = ReceivingAgentAddress.OA_OH


                        Left Join (select  JobShipment.JS_PK,JK_PK,MIN(JW_ETD) As ETD,MAX(JW_ETA) As ETA,MIN(JW_ATD)As ATD ,MAX(JW_ATA) As ATA
                            FROM JobShipment
                                                                LEFT JOIN JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                            LEFT JOIN JOBCONSOL JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                            INNER JOIN --[EXKPRD.WISEGRID.NET].[OdysseyEXKPRD].[dbo].
                                                                [vw_ConsolTransports]  ON vw_ConsolTransports.JW_JK = JOBCONSOL.JK_PK
                            group by JOBCONSOL.JK_PK,JobShipment.JS_PK)jwtransport on jwtransport.JS_PK = JobShipment.JS_PK

                        LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                               from JobShipment
                               LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                               LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                               left join JobConsolTransport on JW_ParentGUID = JK_PK
                               where JW_LegOrder = '1' ) det1 on det1.JS_PK= JobShipment.JS_PK
                                           LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                               from JobShipment
                               LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                               LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                               left join JobConsolTransport on JW_ParentGUID = JK_PK
                        where JW_LegOrder ='2' ) det2 on det2.JS_PK= JobShipment.JS_PK
                                           LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                               from JobShipment
                               LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                               LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                               left join JobConsolTransport on JW_ParentGUID = JK_PK
                                           where JW_LegOrder ='3' ) det3 on det3.JS_PK= JobShipment.JS_PK
                                           LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                               from JobShipment
                               LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                               LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                               left join JobConsolTransport on JW_ParentGUID = JK_PK
                                           where JW_LegOrder ='4' ) det4 on det4.JS_PK= JobShipment.JS_PK

                        where JS_UniqueConsignRef = '" . $shipmentNo . "'
                        and JS_IsCancelled = '0'
                        --and JS_SystemLastEditTimeUtc between '2020-07-05 00:00:00' and '2020-10-06 00:00:00'

                        order by JS_UniqueConsignRef,OrderNo asc;");

        } catch (Exception $exception) {
            Log::error('Exception in getSingleShipment SyncFromSQLDbRepository', [$exception->getMessage()]);
            return $exception->getMessage();
        }
    }

    public function getSingleShipmentByNewQuery($shipmentNo)
    {
        try {
            return DB::connection('sqlsrv')
                ->select("select   distinct
                    JS_UniqueConsignRef,          -- Shipmentlevel xml-- --Worked one

                    isnull(Joborderheader.JD_OrderNumber, '' ) as OrderNo,
                    isnull(jobOrgheader.OH_Code, '') as BuyerCode,
                    isnull(OrgHeaderSupplier.OH_Code, '') as SupplierCode,
                    isnull(JD_BookingConfRef, '') as ConfirmNo,
                    isnull(JD_InvoiceNumber, '') as InvoiceNo,
                    isnull(JD_OrderDate, '') as OrderNoDate,
                    isnull(JD_InvoiceDate, '') as InvoiceNoDate,
                    isnull(JD_BookingConfDate, '') as ConfirmNoDate,
                    isnull(JD_FollowUpDate, '') as Followupdate,
                    isnull(JD_DeliveryRequiredBy, '') as Requiredinstoredate,
                    ISNULL(CAST(JD_OrderStatus AS varchar), '')as OrderStatus,
                    isnull(JD_ExWorksRequiredBy, '') as ReqExWorkDate,
                    isnull(JD_OrderGoodsDescription, '') as OrderGoodsDescription,
                    isnull(JD_RX_NKOrderCurrency, '') as Currency,
                    isnull(JD_RS_NKServiceLevel_NI, '') as OrderServiceLevel,
                    ----isnull(JD_IncoTerm, '') as OrderINCOTerm,
                    ISNULL(CAST(JD_IncoTerm AS varchar), '')as OrderINCOTerm,
                    isnull(JD_AdditionalTerms, '') as AdditionalTerms,
                    ISNULL(CAST(JD_TransportMode AS varchar), '')as OrderTransMode,
                    isnull(JD_RN_NKCountryOfSupply, '') as OrderCountryOrigin,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then Ordercount.OrdercountOrder
                    when Joborderheader.JD_OrderNumber IS NULL then PackingOrder.ContainerPackingOrder
                    end ), '') as OrderLineCount,

                    --isnull(PackingOrder.ContainerPackingOrder, '') as OrderLineCount,
                    ----PackingLine

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then CAST(JO_PK AS varchar (max))
                    when Joborderheader.JD_OrderNumber IS NULL then CAST(PackingLine.JL_PK AS varchar (max))
                    end ), '') as JL_PK,

                    --ISNULL(CAST(PackingLine.JL_PK AS varchar (max)), '')as JL_PK,
                    ----ISNULL(CAST(PackingLine.JL_PK AS varchar), '')as JL_PK,
                    ----isnull(PackingLine.JL_PK, '') as JL_PK,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_Description
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Description
                    end ), '') as OrderLineDescription,

                    --isnull(PackingLine.JL_Description, '') as OrderLineDescription,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacks
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_PackageCount
                    end )AS NUMERIC(10,2)), 0)  as OrderLineOutterPacks,

                    ----isnull(JO_OuterPacks, NULL) as OrderLineOutterPacks,   isnull(CAST('JO_OuterPacks' AS DECIMAL(22,8)), '')
                    ----isnull(JO_OuterPacks, 0) as OrderLineOutterPacks,
                    --isnull(PackingLine.JL_PackageCount, 0) as OrderLineOutterPacks,     --JL_PackageCount

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPacksUQ
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_F3_NKPackType
                    end ), '') as OrderLineType,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualWeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeight
                    end )AS DECIMAL(18, 2)), 0) as OrderLineActualWeight,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfWeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualWeightUQ
                    end ), '') as OrderLineUnitofweight,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_ActualVolume
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolume
                    end )AS DECIMAL(18, 2)), 0) as OrderLineVolume,

                    isnull((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_UnitOfVolume
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_ActualVolumeUQ
                    end ), '') as OrderLineUnitofVolume,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackWidth
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Width
                    end )AS DECIMAL(18, 2)), 0) as OrderLineWidth,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackHeight
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Height
                    end )AS DECIMAL(18, 2)), 0) as OrderLineHeight,

                    isnull(CAST((case when Joborderheader.JD_OrderNumber IS NOT NULL then JO_OuterPackLength
                    when Joborderheader.JD_OrderNumber IS NULL then PackingLine.JL_Length
                    end )AS DECIMAL(18, 2)), 0) as OrderLineLength,

                    isnull(JC_ContainerNum, '') as Pack_JC_ContainerNum,
                    isnull(JC_SealNum, '') as Pack_JC_SealNum,
                    isnull(RC_Code, '') as Pack_RC_Code,

                    isnull((case when JS_IsForwardRegistered = 'True' then 'true'
                    when JS_IsForwardRegistered = 'true' then 'true'
                    when JS_IsForwardRegistered = 'False' then 'false'
                    when JS_IsForwardRegistered = 'false' then 'false'
                    end ), '') as JS_IsForwardRegistered,

                    ----JS_OuterPacks AS BookedPallets,
                    ----JS_ActualWeight AS BookedWeight1,
                    isnull(Bkdqty.Quantity1, '') as BookedPallets,
                    ----Bkdwgt.Weight1 as BookedWeight,
                    ----JS_ActualWeight,
                    isnull(Case when Bkdqty.Quantity1 IS NULL then JS_ActualWeight
                    when Bkdqty.Quantity1 IS NOT NULL then Bkdqty.Quantity1
                    end, 0) as BookedWeight,

                    isnull(JS_ActualVolume , 0) as  JS_ActualVolume,   --BookedVolume,
                    isnull(JS_UnitOfVolume, 0) as JS_UnitOfVolume,

                    ISNULL(CAST('1' AS varchar), '')as OrganizationType,

                    ISNULL(CAST(JS_TransportMode AS varchar), '')as JS_TransportMode,
                    isnull(JS_RS_NKServiceLevel, '') as JS_RS_NKServiceLevel,
                    isnull(JS_ShipmentType, '') as JS_ShipmentType,
                    isnull(JS_HouseBill, '') as HAWB,
                    isnull(JS_SystemLastEditTimeUtc, '') as JS_SystemLastEditTimeUtc,
                    ISNULL(CAST(JS_INCO AS varchar), '')as JS_INCO,
                    ISNULL(CAST(JS_PackingMode AS varchar), '')as JS_PackingMode,
                    isnull(JS_GoodsDescription, '') AS GoodsDescription,
                    isnull(SaleRep.GS_FullName, '') as ExpoRepresentative,
                    isnull(SaleRep.GS_EmailAddress, '') as ExpoRepresentativeEmail,
                    isnull(TerminalUser.OC_ContactName, '') as ShipperRepresentative,
                    isnull(TerminalUser.OC_Email, '') as ShipperRepresentativeEmail,
                    isnull(JS_RL_NKOrigin, '') as JS_RL_NKOrigin,
                    isnull(JS_RL_NKDestination, '') as JS_RL_NKDestination,
                    isnull(JS_RL_NKLoadPort, '') As LoadPort,
                    isnull(JS_RL_NKDischargePort, '') As DischargePort,

                    isnull(JV_RV_NKVessel, '') AS Vessel,
                    isnull(JV_VoyageFlight, '') AS VoyageFlight,

                    isnull(ActualDelivery.CartageCompleted, '') as ActualDeliverydate,

                    ----isnull(JS_E_DEP, '') as ETD,
                    ----isnull(JS_E_ARV, '') As ETA,

                    ----isnull(jwtransport.ETD, '') as ETD,
                    ----isnull(jwtransport.ETA, '') As ETA,
                    ----isnull(jwtransport.ATD, '') As ATD,
                    ----isnull(jwtransport.ATA, '') As ATA,

                    isnull(case when JS_IsForwardRegistered = '0' then JS_E_DEP
                    when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NOT NULL  then jwtransport.ETD
                    when JS_IsForwardRegistered = '1' and jwtransport.ETD IS NULL Then JS_E_DEP
                    end, '') as ETD,

                    isnull(case when JS_IsForwardRegistered = '0' then JS_E_ARV
                    when JS_IsForwardRegistered = '1' and  jwtransport.ETA IS NOT NULL then jwtransport.ETA
                    when JS_IsForwardRegistered = '1'  and jwtransport.ETA IS NULL then JS_E_ARV
                    end, '') as ETA,

                    isnull(jwtransport.ATD, '') As ATD,
                    isnull(jwtransport.ATA, '') As ATA,

                    isnull(orgHeaderCarrier.OH_Code, '') AS Carrier_CODE,
                    isnull(orgHeaderCarrier.OH_FullName, '') AS CarrierName,

                    isnull(SendingAgent.OH_Code, '') AS SendAgentCode,
                    isnull(SendingAgent.OH_FullName, '') AS SendAgentName,

                    isnull(ReceivingAgent.OH_Code, '') AS RecvAgentCode,
                    isnull(ReceivingAgent.OH_FullName, '') AS RecvAgentName,

                    isnull(JS_ActualWeight, 0) as    JS_ActualWeight,  --TotalWeight,
                    isnull(JS_UnitOfWeight, 0) as JS_UnitOfWeight,
                    isnull(JS_ActualChargeable, 0) as JS_ActualChargeable,
                    isnull(case when JS_TransportMode = 'AIR' then JS_UnitOfWeight
                    when JS_TransportMode = 'SEA' then JS_UnitOfVolume
                    end, 0) as JS_UnitOfActualChargeable,
                    isnull(JS_OuterPacks, '') AS JS_OuterPacks,      --TotalPallets,
                    isnull(JS_F3_NKPackType, '') AS JS_F3_NKPackType,
                    isnull(JS_BookingReference, '') as ShippersRef,
                    isnull(CusEntryNum.CE_EntryNum, '') as VehicleNo,
                    isnull(DRCusEn.CE_EntryNum, '') as DriverName,

                    isnull(JS_A_RCV, '') as Received_Date,
                    isnull(CustomsCheck1.completedDate, '') as CustomsCheck,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Terminal App Remarks'), '') As Remarks,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobConsol.JK_PK and ST_Table = 'JobConsol' AND ST_Description = 'Terminal App Note'), '') As ConfirmatAirline,
                    isnull((Select ST_NoteText from StmNote where ST_ParentID = JobShipment.JS_PK and ST_Table = 'JobShipment' AND ST_Description = 'Marks & Numbers'), '')  As MarksNumber ,
                    ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Quantity')  As Booked_Quantity,
                    ----dbo.GetCustomFieldByName(jobshipment.JS_PK, 'Booked Weight')  As Booked_Weight,

                    isnull(JK_MasterBillNum, '') as JK_MasterBillNum,

                    isnull (CAST(CASE
                        WHEN JS_PackingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'LCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'FCL' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BCN' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BBK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        WHEN JS_PackingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) = 0 then 0
                        WHEN JS_PAckingMode = 'BLK' AND CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) <> 0 then CAST(JS_ACTUALVOLUME AS DECIMAL(18, 4)) / 27
                        Else  NULL
                    END AS DECIMAL(18, 2)), 0) As TEU,

                    ----isnull(JobShipmentOrgs.JS_E2_OA_OH_NKConsignor, '') As ConsignorCode,
                    isnull(ConsignorOrg.OH_Code, '') as ConsignorCode,
                    isnull(ConsignorOrg.FullName, '') as ConsignorFullName,
                    isnull(ConsignorOrg.Address1, '') as ConsignorFullAddress,
                    isnull(ConsignorOrg.City, '') as ConsignorCity,
                    isnull(ConsignorOrg.State, '') as ConsignorState,
                    isnull(ConsignorOrg.PostCode, '') as ConsignorPostCode,

                    isnull(ConsigneeOrg.OH_Code, '') as ConsigneeCode,
                    isnull(ConsigneeOrg.FullName, '') as ConsigneeFullName,
                    isnull(ConsigneeOrg.Address1, '') as ConsigneeFullAddress,
                    isnull(ConsigneeOrg.City, '') as ConsigneeCity,
                    isnull(ConsigneeOrg.State, '') as ConsigneeState,
                    isnull(ConsigneeOrg.PostCode, '') as ConsigneePostCode,

                    isnull('', '')as LocalClientCode,
                    isnull('', '')as LocalClientFullName,
                    isnull('', '')as LocalClientFullAddress,
                    isnull('', '')as LocalClientCity,
                    isnull('', '')as LocalClientState,
                    isnull('', '')as LocalClientPostCode,

                    isnull(JS_SystemCreateTimeUtc, '') as JS_SystemCreateTimeUtc,

                    isnull(det1.JW_LegOrder, 0) as Leg_Order1,
                    ISNULL(CAST(det1.JW_TransportMode AS varchar), '')as Mode1,
                    isnull(det1.JW_VoyageFlight, '') as VoyageFlight1,   --JW_Vessel
                    isnull(det1.JW_Vessel, '') as Vessel1,
                    --isnull(det1.JW_ETD, '') as legETD1,

                    isnull(FORMAT(convert(datetime2,det1.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD1,

                    --isnull(det1.JW_ETA, '') as legETA1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA1,
                    --isnull(det1.JW_ATD, '') as legATD1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD1,
                    --isnull(det1.JW_ATA, '') as legATA1,
                    isnull(FORMAT(convert(datetime2,det1.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA1,
                    isnull(det1.JW_RL_NKLoadPort, '') as leg1load,
                    isnull(det1.JW_RL_NKDiscPort, '') as leg1disc,
                    --isnull(det1.JW_LegNotes, '') as leg1note,

                    isnull(det2.JW_LegOrder, 0) as Leg_Order2,
                    ISNULL(CAST(det2.JW_TransportMode AS varchar), '')as Mode2,
                    isnull(det2.JW_VoyageFlight, '') as VoyageFlight2,
                    isnull(det2.JW_Vessel, '') as Vessel2,
                    --isnull(det2.JW_ETD, '') as legETD2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD2,
                    --isnull(det2.JW_ETA, '') as legETA2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA2,
                    --isnull(det2.JW_ATD, '') as legATD2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD2,
                    --isnull(det2.JW_ATA, '') as legATA2,
                    isnull(FORMAT(convert(datetime2,det2.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA2,
                    isnull(det2.JW_RL_NKLoadPort, '') as leg2load,
                    isnull(det2.JW_RL_NKDiscPort, '') as leg2disc,
                    --isnull(det2.JW_LegNotes, '') as leg2note,

                    isnull(det3.JW_LegOrder, 0) as Leg_Order3,
                    ISNULL(CAST(det3.JW_TransportMode AS varchar), '')as Mode3,
                    isnull(det3.JW_VoyageFlight, '') as VoyageFlight3,
                    isnull(det3.JW_Vessel, '') as Vessel3,
                    --isnull(det3.JW_ETD, '') as legETD3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD3,
                    --isnull(det3.JW_ETA, '') as legETA3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA3,
                    --isnull(det3.JW_ATD, '') as legATD3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD3,
                    --isnull(det3.JW_ATA, '') as legATA3,
                    isnull(FORMAT(convert(datetime2,det3.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA3,
                    isnull(det3.JW_RL_NKLoadPort, '') as leg3load,
                    isnull(det3.JW_RL_NKDiscPort, '') as leg3disc,
                    --isnull(det3.JW_LegNotes, '') as leg3note,

                    isnull(det4.JW_LegOrder, 0) as Leg_Order4,
                    ISNULL(CAST(det4.JW_TransportMode AS varchar), '')as Mode4,
                    isnull(det4.JW_VoyageFlight, '') as VoyageFlight4,
                    isnull(det4.JW_Vessel, '') as Vessel4,
                    --isnull(det4.JW_ETD, '') as legETD4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ETD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETD4,
                    --isnull(det4.JW_ETA, '') as legETA4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ETA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legETA4,
                    --isnull(det4.JW_ATD, '') as legATD4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ATD,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATD4,
                    --isnull(det4.JW_ATA, '') as legATA4,
                    isnull(FORMAT(convert(datetime2,det4.JW_ATA,126), 'yyyy-MM-ddThh:mm:00Z'), '') as legATA4,
                    isnull(det4.JW_RL_NKLoadPort, '') as leg4load,
                    isnull(det4.JW_RL_NKDiscPort, '') as leg4disc  --S17119077583,S17119077599
                    ----isnull(det4.JW_LegNotes, '') as leg4note

                    --INTO dbo.Final_BulkAccount_MACWELVNS_Destination    --LULUSAGVP

                    --INTO dbo.NewTest_Account_ConsigneeOrG_DKIKA_withPO_22

                    --select top 100 JobShipment.JS_UniqueConsignRef
                    from jobshipment
                    LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                    LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                    left join JobConsolTransport on JW_ParentGUID = JK_PK
                    LEFT JOIN JobHeader on JH_ParentID = JS_PK
                    left JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                    LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK

                    Left join cvw_JobShipmentOrgs JobShipmentOrgs on JobShipmentOrgs.JS_PK =  JobShipment.JS_PK

                    Left join(SELECT
                                  JS_PK                = E2_ParentID,

                                  OH_Code                    = OH_Code,
                                  OH_PK                = OH_PK,
                                          deliveryunloco        =OH_RL_NKClosestPort,
                                  FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                  City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                  [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                  PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                  Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                  Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                  Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                    FROM
                          dbo.JobDocAddress

                    Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                    LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                    LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                    WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CRD' ----Consignor
                           AND E2_ParentTableCode = 'JS'

                           )ConsignorOrg on ConsignorOrg.JS_PK =     CAST(JobShipment.JS_PK AS Varchar(100))

                    Left join(SELECT
                                  JS_PK                = E2_ParentID,

                                  OH_Code                    = OH_Code,
                                  OH_PK                = OH_PK,
                                          deliveryunloco        =OH_RL_NKClosestPort,
                                  FullName             = CASE E2_AddressOverride WHEN '1' THEN E2_CompanyName ELSE OH_FullName END,
                                  City                 = CASE E2_AddressOverride WHEN '1' THEN E2_City        ELSE OA_City END,
                                  [State]                    = CASE E2_AddressOverride WHEN '1' THEN E2_State       ELSE OA_State END,
                                  PostCode             = CASE E2_AddressOverride WHEN '1' THEN E2_Postcode    ELSE OA_Postcode END,
                                  Address1             = CASE E2_AddressOverride WHEN '1' THEN E2_Address1  ELSE OA_Address1 END,
                                  Address2             = CASE E2_AddressOverride WHEN '1' THEN E2_Address2  ELSE OA_Address2 END,
                                  Country_Code         = CASE E2_AddressOverride WHEN '1' THEN E2_rn_nkcountrycode ELSE Oa_rn_nkcountrycode END

                    FROM
                           dbo.JobDocAddress

                    Left Join Jobshipment on Jobshipment.JS_PK      = E2_ParentID
                    LEFT JOIN dbo.OrgAddress ON OA_PK = E2_OA_Address --AND E2_AddressOverride = 0
                    LEFT JOIN dbo.OrgHeader ON CAST(OH_PK AS Varchar(100)) = OA_OH
                    WHERE
                           E2_AddressSequence = '0'
                           AND E2_AddressType = 'CED' --Consignee
                           AND E2_ParentTableCode = 'JS' )ConsigneeOrg on ConsigneeOrg.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100))

                    Left join (select JS_PK,
                    JL_PK,
                    JL_Description,
                    JL_PackageCount,
                    JL_F3_NKPackType,
                    JL_ActualWeight,
                    JL_ActualWeightUQ,
                    JL_ActualVolume,
                    JL_ActualVolumeUQ,
                    JL_Width,
                    JL_Height,
                    JL_Length,
                    JC_ContainerNum,
                    JC_SealNum,
                    RC_Code

                    FROM dbo.JobShipment

                    Left Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                    Left JOIN JobContainerPackPivot ON JL_PK = J6_JL
                    Left JOIN JobContainer ON JC_PK = JobContainerPackPivot.J6_JC
                    Left JOIN dbo.RefContainer ON RC_PK = JobContainer.JC_RC
                    --where JS_UniqueConsignRef = 'S12221003242'
                    )as PackingLine on  PackingLine.JS_PK = JobShipment.JS_PK

                    Left join (select JS_PK,count(JL_PK) as ContainerPackingOrder      --  PackingOrder.ContainerPackingOrder  ConCount1
                    from JobShipment
                    inner Join JobPackLines on JobShipment.JS_PK= JobPackLines.JL_JS and JL_RH_NKCommodityCode != ''
                    --where JS_UniqueConsignRef     = 'S13820078187'
                    group by JS_PK)as PackingOrder on  PackingOrder.JS_PK = JobShipment.JS_PK

                    Left join (select JS_PK,JD_OrderNumber,count(JO_PK) as OrdercountOrder      --  Ordercount.OrdercountOrder  as orderslevelCount
                    from JobShipment
                    Inner JOIN Joborderheader on Joborderheader.JD_JS = JobShipment.JS_PK
                    LEFT join JobOrderLine on  JO_JD = JobOrderHeader.JD_PK
                    --where JS_UniqueConsignRef = 'S18020038197'
                    group by JS_PK,JD_OrderNumber)as Ordercount on  Ordercount.JS_PK = CAST(JobShipment.JS_PK AS Varchar(100)) and Joborderheader.JD_OrderNumber = Ordercount.JD_OrderNumber

                    Left Join CusEntryNum on CE_ParentID = JobShipment.JS_PK and CE_ParentTable = 'JobShipment' and CE_EntryType = 'VEH'
                    Left Join CusEntryNum DRCusEn on DRCusEn.CE_ParentID = JobShipment.JS_PK and DRCusEn.CE_ParentTable = 'JobShipment' and DRCusEn.CE_EntryType = 'DRV'

                    LEFT JOIN OrgAddress orgAddressCarrier ON orgAddressCarrier.OA_PK = JobConsol.JK_OA_ShippingLineAddress
                    --LEFT JOIN OrgHeader orgHeaderCarrier ON orgHeaderCarrier.OH_PK = orgAddressCarrier.OA_OH
                    LEFT JOIN OrgHeader orgHeaderCarrier ON CAST(orgHeaderCarrier.OH_PK AS Varchar(100)) = orgAddressCarrier.OA_OH

                    --LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CarrierOrgHeader.OH_PK
                    LEFT JOIN dbo.OrgHeader as CarrierOrgHeader  on JobShipment.JS_OH_BookedShippingLine = CAST(CarrierOrgHeader.OH_PK AS Varchar(100))

                    LEFT JOIN dbo.JobSailing  on JobShipment.JS_JX = JobSailing.JX_PK
                    LEFT JOIN dbo.JobVoyOrigin  on JobSailing.JX_JA = JobVoyOrigin.JA_PK
                    LEFT JOIN dbo.JobVoyDestination  on JobSailing.JX_JB = JobVoyDestination.JB_PK
                    LEFT JOIN dbo.JobVoyage  on JobVoyOrigin.JA_JV = JobVoyage.JV_PK and JobVoyDestination.JB_JV = JobVoyage.JV_PK

                    left join
                    (Select JS_PK,JP_DeliveryCartageCompleted   as CartageCompleted   -- ActualDelivery.CartageCompleted as ActualDeliverydate
                    FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                    INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                    ) ActualDelivery ON ActualDelivery.JS_PK = JobShipment.JS_PK

                    left join
                    (Select JS_PK,MAX(ES_Completed) As completedDate FROM dbo.JobShipment    --CustomsCheck1.completedDate as CustomsCheck
                    INNER JOIN dbo.JobDocsAndCartage  ON  JS_PK = JP_ParentID
                    INNER JOIN dbo.JobService  ON ES_ParentID = JP_PK
                    Where ES_ServiceCode = 'CUC'   --CUC
                    group by JS_PK) CustomsCheck1 ON CustomsCheck1.JS_PK = JobShipment.JS_PK

                    left join
                    (select Distinct JS_PK,JS_E2_OA_OH_Consignee,GS_FullName,GS_EmailAddress     --SaleRep.GS_FullName as ExpoRepresentative
                    from OrgStaffAssignments
                    Inner JOIN cvw_JobShipmentOrgs ON CAST(O8_OH AS Varchar(100)) = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                    Inner join GlbStaff on GlbStaff.GS_Code = O8_GS_NKPersonResponsible
                    where O8_Role = 'CUS') SaleRep on SaleRep.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                    left join
                    (select Distinct JS_PK,JS_E2_OA_OH_Consignee,OC_ContactName,OC_Email     --TerminalUser.OC_ContactName as ShipperRepresentative
                    from OrgContact
                    LEFT JOIN cvw_JobShipmentOrgs ON OC_OH = cvw_JobShipmentOrgs.JS_E2_OA_OH_Consignee
                    where OC_Title = 'Terminal App') TerminalUser on TerminalUser.JS_E2_OA_OH_Consignee = JobShipmentOrgs.JS_PK

                    Left join (select Distinct jobshipment.JS_PK, XV_Data as Quantity1    --Bkdqty.Quantity1 as BookedQuantity
                    from Jobshipment
                    inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Quantity') Bkdqty on Bkdqty.JS_PK = jobshipment.JS_PK

                    Left join (select Distinct jobshipment.JS_PK, XV_Data as Weight1                --Bkdwgt.Weight1 as BookedWeight
                    from Jobshipment
                    inner join  GenCustomAddOnValue on XV_ParentID = jobshipment.JS_PK and XV_Name = 'Booked Weight')  Bkdwgt on Bkdwgt.JS_PK = jobshipment.JS_PK

                    Left JOIN OrgAddress jobOrgAddress on jobOrgAddress.OA_PK = JobOrderHeader.JD_OA_BuyerAddress
                    LEFT JOIN OrgHeader jobOrgheader on jobOrgheader.OH_PK = jobOrgAddress.OA_OH

                    LEFT JOIN OrgAddress OrgAddressSupplier on OrgAddressSupplier.OA_PK = JobOrderHeader.JD_OA_SupplierAddress
                    LEFT JOIN OrgHeader OrgHeaderSupplier on OrgHeaderSupplier.OH_PK = OrgAddressSupplier.OA_OH

                    LEFT JOIN dbo.OrgAddress AS SendingAgentAddress ON SendingAgentAddress.OA_PK = JK_OA_SendingForwarderAddress
                    --LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON SendingAgent.OH_PK = SendingAgentAddress.OA_OH
                    LEFT JOIN dbo.OrgHeader  AS SendingAgent        ON CAST(SendingAgent.OH_PK AS Varchar(100)) = SendingAgentAddress.OA_OH

                    LEFT JOIN dbo.OrgAddress AS ReceivingAgentAddress ON ReceivingAgentAddress.OA_PK = JK_OA_ReceivingForwarderAddress
                    LEFT JOIN dbo.OrgHeader  AS ReceivingAgent        ON CAST(ReceivingAgent.OH_PK AS Varchar(100)) = ReceivingAgentAddress.OA_OH

                    Left Join (select  JobShipment.JS_PK,JK_PK,MIN(JW_ETD) As ETD,MAX(JW_ETA) As ETA,MIN(JW_ATD)As ATD ,MAX(JW_ATA) As ATA
                                FROM JobShipment
                                                                    LEFT JOIN JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                                LEFT JOIN JOBCONSOL JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                                INNER JOIN --[EXKPRD.WISEGRID.NET].[OdysseyEXKPRD].[dbo].
                                                                    [vw_ConsolTransports]  ON vw_ConsolTransports.JW_JK = JOBCONSOL.JK_PK
                                group by JOBCONSOL.JK_PK,JobShipment.JS_PK)jwtransport on jwtransport.JS_PK = JobShipment.JS_PK

                    LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                           from JobShipment
                           LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                           LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                           left join JobConsolTransport on JW_ParentGUID = JK_PK
                           where JW_LegOrder = '1' ) det1 on det1.JS_PK= JobShipment.JS_PK
                                LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='2' ) det2 on det2.JS_PK= JobShipment.JS_PK
                                LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='3' ) det3 on det3.JS_PK= JobShipment.JS_PK
                               LEFT JOIN (select JS_PK,JW_LegOrder,JW_TransportMode,JW_VoyageFlight,JW_Vessel,JW_ETD,JW_ETA,JW_ATA,JW_ATD,JW_RL_NKLoadPort,JW_RL_NKDiscPort,JW_LegNotes
                          from JobShipment
                          LEFT JOIN dbo.JobConShipLink  ON JobConShipLink.JN_JS = JobShipment.JS_PK
                          LEFT JOIN dbo.JOBCONSOL  ON JOBCONSOL.JK_PK = JobConShipLink.JN_JK
                          left join JobConsolTransport on JW_ParentGUID = JK_PK
                          where JW_LegOrder ='4' ) det4 on det4.JS_PK= JobShipment.JS_PK

                    --Left join (Select JS_PK,count(JC_ContainerNum) as ConCount1--,Containercount1.ConCount1 as Containercount
                    --FROM dbo.JobShipment
                    --INNER JOIN dbo.JobConShipLink  ON JS_PK = JN_JS
                    --INNER JOIN dbo.JobConsol  ON JN_JK = JK_PK
                    --INNER JOIN dbo.JobContainer  ON JK_PK = JC_JK
                    --INNER JOIN dbo.RefContainer on JC_RC = RefContainer.RC_PK
                    --group by JS_PK)Containercount1 on Containercount1.JS_PK = JobShipment.JS_PK

                    where JS_UniqueConsignRef = '" . $shipmentNo . "'
                    and JS_IsCancelled = '0'
                    order by JS_UniqueConsignRef,OrderNo asc;");

        } catch (Exception $exception) {
            Log::error('Exception in getSingleShipmentByNewQuery SyncFromSQLDbRepository', [$exception->getMessage()]);
            return $exception->getMessage();
        }
    }


    public function getActiveConsignees()
    {
        //get all active consignees
        $activeConsigneeIds = CustomerUsers::distinct()->pluck('orgPkId')->toArray();
        //get consignee codes by this ids
        return Customers::whereIn('id', $activeConsigneeIds)
            ->where('is_syncable', 1)
            ->pluck('orgCode')
            ->toArray();
    }
}
