<?php


namespace App\Repository;


use App\Repository\Booking\MapBookingEventsDataToRepository;
use App\Repository\Booking\MapBookingLegDataToRepository;
use App\Repository\Booking\MapBookingLineDataToRepository;
use App\Repository\Booking\MapBookingPurchaseOrderDataToRepository;
use App\Repository\Booking\MapBookingShipmentDataToRepository;
use Illuminate\Support\Facades\DB;

class BookingShipmentDataStoreRepository
{

    private $mainBookingShipmentData;
    private $bookingLegData;
    private $bookingPurchaseOrderData;
    private $bookingLineData;
    private $bookingEventData;
    private $bookingNo;

    public function __construct($bookingNo, $mainBookingShipmentData, $bookingLegData, $bookingPurchaseOrderData, $bookingLineData, $bookingEventData)
    {
        $this->bookingNo = $bookingNo;
        $this->mainBookingShipmentData = $mainBookingShipmentData;
        $this->bookingLegData = $bookingLegData;
        $this->bookingPurchaseOrderData = $bookingPurchaseOrderData;
        $this->bookingLineData = $bookingLineData;
        $this->bookingEventData = $bookingEventData;
    }

    public function saveBookingShipment()
    {
        DB::transaction(function () {
            //save booking shipment basic data
            $shipmentId = (new MapBookingShipmentDataToRepository($this->mainBookingShipmentData))->updateOrCreate();

            //save booking shipment leg data
            (new MapBookingLegDataToRepository($this->bookingLegData, $shipmentId, $this->bookingNo))->updateOrCreate();

            //save purchase order & line data
            $purchaseOrderId = null;
            if (count($this->bookingPurchaseOrderData) > 0) {
                foreach ($this->bookingPurchaseOrderData as $po) {
                    $purchaseOrderId = (new MapBookingPurchaseOrderDataToRepository($po, $shipmentId, $this->bookingNo))->updateOrCreate();
                    //save line data
                    if (count($this->bookingLineData) > 0) {
                        foreach ($this->bookingLineData as $line) {
                            (new MapBookingLineDataToRepository($line, $purchaseOrderId, $this->bookingNo))->updateOrCreate();
                        }
                    }
                }
            }

            //save booking event data
            if (count($this->bookingEventData) > 0) {
                foreach ($this->bookingEventData as $events) {
                    if (count($events) > 0) {
                        foreach ($events as $event) {
                            (new MapBookingEventsDataToRepository($event, $shipmentId, $this->bookingNo))->updateOrCreate();
                        }
                    }
                }
            }
        });
    }

}
