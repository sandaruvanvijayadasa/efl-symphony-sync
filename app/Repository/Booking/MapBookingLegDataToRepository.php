<?php


namespace App\Repository\Booking;


use App\Models\Booking\BookingLegs;
use App\Models\DataSyncModels\Booking\BookingLegSynchronization;
use Exception;
use Illuminate\Support\Facades\Log;

class MapBookingLegDataToRepository
{

    private $bookingLegData;
    private $shipmentId;
    private $bookingNo;

    public function __construct(BookingLegSynchronization $bookingLegSynchronization, $shipmentId, $bookingNo)
    {
        $this->bookingLegData = $bookingLegSynchronization;
        $this->shipmentId = $shipmentId;
        $this->bookingNo = $bookingNo;
    }

    public function updateOrCreate()
    {
        try {
            $leg = BookingLegs::updateOrCreate(
                ['bookingShippingsId' => $this->shipmentId],
                [
                    'bookingShippingsId' => $this->shipmentId,
                    'Leg_Order1' => $this->bookingLegData->getLegOrder1(),
                    'VoyageFlight1' => $this->bookingLegData->getVoyageFlight1(),
                    'Vessel1' => $this->bookingLegData->getVessel1(),
                    'legETD1' => $this->bookingLegData->getLegETD1(),
                    'legETA1' => $this->bookingLegData->getLegETA1(),
                    'legATD1' => $this->bookingLegData->getLegATD1(),
                    'legATA1' => $this->bookingLegData->getLegATA1(),
                    'leg1load' => $this->bookingLegData->getLeg1load(),
                    'leg1disc' => $this->bookingLegData->getLeg1disc(),
                    'mode1' => $this->bookingLegData->getMode1(),
                    'Leg_Order2' => $this->bookingLegData->getLegOrder2(),
                    'VoyageFlight2' => $this->bookingLegData->getVoyageFlight2(),
                    'Vessel2' => $this->bookingLegData->getVessel2(),
                    'legETD2' => $this->bookingLegData->getLegETD2(),
                    'legETA2' => $this->bookingLegData->getLegETA2(),
                    'legATD2' => $this->bookingLegData->getLegATD2(),
                    'legATA2' => $this->bookingLegData->getLegATA2(),
                    'leg2load' => $this->bookingLegData->getLeg2load(),
                    'leg2disc' => $this->bookingLegData->getLeg2disc(),
                    'mode2' => $this->bookingLegData->getMode2(),
                    'Leg_Order3' => $this->bookingLegData->getLegOrder3(),
                    'VoyageFlight3' => $this->bookingLegData->getVoyageFlight3(),
                    'Vessel3' => $this->bookingLegData->getVessel3(),
                    'legETD3' => $this->bookingLegData->getLegETD3(),
                    'legETA3' => $this->bookingLegData->getLegETA3(),
                    'legATD3' => $this->bookingLegData->getLegATD3(),
                    'legATA3' => $this->bookingLegData->getLegATA3(),
                    'leg3load' => $this->bookingLegData->getLeg3load(),
                    'leg3disc' => $this->bookingLegData->getLeg3disc(),
                    'mode3' => $this->bookingLegData->getMode3(),
                    'Leg_Order4' => $this->bookingLegData->getLegOrder4(),
                    'VoyageFlight4' => $this->bookingLegData->getVoyageFlight4(),
                    'Vessel4' => $this->bookingLegData->getVessel4(),
                    'legETD4' => $this->bookingLegData->getLegETD4(),
                    'legETA4' => $this->bookingLegData->getLegETA4(),
                    'legATD4' => $this->bookingLegData->getLegATD4(),
                    'legATA4' => $this->bookingLegData->getLegATA4(),
                    'leg4load' => $this->bookingLegData->getLeg4load(),
                    'leg4disc' => $this->bookingLegData->getLeg4disc(),
                    'mode4' => $this->bookingLegData->getMode4(),
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Booking shipment leg data updateOrCreate', [$this->bookingNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }
}
