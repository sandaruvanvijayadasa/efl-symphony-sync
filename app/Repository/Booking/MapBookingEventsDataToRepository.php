<?php


namespace App\Repository\Booking;


use App\Models\Booking\BookingEvents;
use Illuminate\Support\Facades\Log;
use Exception;

class MapBookingEventsDataToRepository
{

    private $bookingEventData;
    private $shipmentId;
    private $bookingNo;

    public function __construct($bookingEventsSynchronization, $shipmentId, $bookingNo)
    {
        $this->bookingEventData = $bookingEventsSynchronization;
        $this->shipmentId = $shipmentId;
        $this->bookingNo = $bookingNo;
    }

    public function updateOrCreate()
    {
        try {
            BookingEvents::updateOrCreate(
                [
                    'bookingShippingsId' => $this->shipmentId,
                    'evenType' => $this->bookingEventData['event_type'],
                    'eventDate' => $this->bookingEventData['event_date'],
                    'eventDetail' => $this->bookingEventData['event_detail']
                ],
                [
                    'bookingShippingsId' => $this->shipmentId,
                    'evenType' => $this->bookingEventData['event_type'],
                    'eventDate' => $this->bookingEventData['event_date'],
                    'eventDetail' => $this->bookingEventData['event_detail']
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Booking shipment events data updateOrCreate', [$this->bookingNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }
}
