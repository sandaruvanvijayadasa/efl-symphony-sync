<?php


namespace App\Repository\Booking;


use App\Models\Booking\BookingLines;
use App\Models\DataSyncModels\Booking\BookingLinesSynchronization;
use Illuminate\Support\Facades\Log;
use Exception;

class MapBookingLineDataToRepository
{

    private $bookingLineData;
    private $poNumber;
    private $bookingNo;

    public function __construct(BookingLinesSynchronization $bookingLinesSynchronization, $poNumber, $bookingNo)
    {
        $this->bookingLineData = $bookingLinesSynchronization;
        $this->poNumber = $poNumber;
        $this->bookingNo = $bookingNo;
    }

    public function updateOrCreate()
    {
        try {
            $lineNo = null;
            $lastLineByPo = BookingLines::where([['poNumber', $this->poNumber]])->orderBy('id', 'DESC')->first();
            $sameLine = BookingLines::where([['jlPk', $this->bookingLineData->getJlPk()]])->first();
            if (!is_null($lastLineByPo)) {
                if (is_null($sameLine))
                    $lineNo = intval($lastLineByPo->lineNumber) + 1;
                else
                    $lineNo = $sameLine->lineNumber;
            } else {
                $lineNo = 1;
            }
            $line = BookingLines::updateOrCreate(
                ['poNumber' => $this->poNumber, 'jlPk' => $this->bookingLineData->getJlPk()],
                [
                    'poNumber' => $this->poNumber,
                    'jlPk' => $this->bookingLineData->getJlPk(),
                    'lineNumber' => $lineNo,
                    'lineDescription' => $this->bookingLineData->getLineDescription(),
                    'lineOuterPacks' => doubleval($this->bookingLineData->getLineOuterPacks()),
                    'lineType' => $this->bookingLineData->getLineType(),
                    'LineActualWeight' => doubleval($this->bookingLineData->getLineActualWeight()),
                    'LineUnitofweight' => $this->bookingLineData->getLineUnitofweight(),
                    'LineVolume' => doubleval($this->bookingLineData->getLineVolume()),
                    'LineUnitofVolume' => $this->bookingLineData->getLineUnitofVolume(),
                    'LineWidth' => doubleval($this->bookingLineData->getLineWidth()),
                    'LineHeight' => doubleval($this->bookingLineData->getLineHeight()),
                    'LineLength' => doubleval($this->bookingLineData->getLineLength()),
                    'PackJCContainerNum' => $this->bookingLineData->getPackJCContainerNum(),
                    'PackJCSealNum' => $this->bookingLineData->getPackJCSealNum(),
                    'PackRCCode' => $this->bookingLineData->getPackRCCode(),
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Booking shipment line data updateOrCreate', [$this->bookingNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
