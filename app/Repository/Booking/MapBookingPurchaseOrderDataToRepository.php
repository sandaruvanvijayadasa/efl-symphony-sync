<?php


namespace App\Repository\Booking;


use App\Models\Booking\BookingPurchaseOrders;
use App\Models\DataSyncModels\Booking\BookingPurchaseOrderSynchronization;
use Illuminate\Support\Facades\Log;
use Exception;

class MapBookingPurchaseOrderDataToRepository
{

    private $bookingShipmentPurchaseOrderData;
    private $shipmentId;
    private $bookingNo;

    public function __construct(BookingPurchaseOrderSynchronization $bookingPurchaseOrderSynchronization, $shipmentId, $bookingNo)
    {
        $this->bookingShipmentPurchaseOrderData = $bookingPurchaseOrderSynchronization;
        $this->shipmentId = $shipmentId;
        $this->bookingNo = $bookingNo;
    }

    public function updateOrCreate()
    {
        try {
            $purchaseOrder = BookingPurchaseOrders::updateOrCreate(
                ['bookingShippingsId' => $this->shipmentId, 'orderNumber' => $this->bookingShipmentPurchaseOrderData->getOrderNumber()],
                [
                    'bookingShippingsId' => $this->shipmentId,
                    'orderNumber' => $this->bookingShipmentPurchaseOrderData->getOrderNumber(),
                    'buyerCode' => $this->bookingShipmentPurchaseOrderData->getBuyerCode(),
                    'supplierCode' => $this->bookingShipmentPurchaseOrderData->getSupplierCode(),
                    'confirmNumber' => $this->bookingShipmentPurchaseOrderData->getConfirmNumber(),
                    'invoiceNumber' => $this->bookingShipmentPurchaseOrderData->getInvoiceNumber(),
                    'oderNumberDate' => $this->bookingShipmentPurchaseOrderData->getOderNumberDate(),
                    'invoiceNumberDate' => $this->bookingShipmentPurchaseOrderData->getInvoiceNumberDate(),
                    'confirmNumberDate' => $this->bookingShipmentPurchaseOrderData->getConfirmNumberDate(),
                    'followUpDate' => $this->bookingShipmentPurchaseOrderData->getFollowUpDate(),
                    'requireInstoDate' => $this->bookingShipmentPurchaseOrderData->getRequireInstoDate(),
                    'orderStatus' => $this->bookingShipmentPurchaseOrderData->getOrderStatus(),
                    'requireExWorkDate' => $this->bookingShipmentPurchaseOrderData->getRequireExWorkDate(),
                    'orderGoodDescription' => $this->bookingShipmentPurchaseOrderData->getOrderGoodDescription(),
                    'currency' => $this->bookingShipmentPurchaseOrderData->getCurrency(),
                    'orderServiceLevel' => $this->bookingShipmentPurchaseOrderData->getOrderServiceLevel(),
                    'orderINCOTerm' => $this->bookingShipmentPurchaseOrderData->getOrderINCOTerm(),
                    'AdditionalTerms' => $this->bookingShipmentPurchaseOrderData->getAdditionalTerms(),
                    'OrderTransMode' => $this->bookingShipmentPurchaseOrderData->getOrderTransMode(),
                    'OrderCountryOrigin' => $this->bookingShipmentPurchaseOrderData->getOrderCountryOrigin()
                ]
            );
            return $purchaseOrder->id;
        } catch (Exception $e) {
            Log::error('Exception in Booking shipment purchase order data updateOrCreate', [$this->bookingNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
