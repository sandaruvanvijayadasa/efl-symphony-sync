<?php


namespace App\Repository\Containers;


use App\Models\Containers\Containers;
use App\Models\DataSyncModels\Containers\ContainersSynchronization;
use Illuminate\Support\Facades\Log;
use Exception;

class MapContainerDataToRepository
{

    private $containerData;
    private $shipmentId;
    private $shipmentNo;

    public function __construct(ContainersSynchronization $containersSynchronization, $shipmentId, $shipmentNo)
    {
        $this->containerData = $containersSynchronization;
        $this->shipmentId = $shipmentId;
        $this->shipmentNo = $shipmentNo;
    }

    public function updateOrCreate()
    {
        try {
            Containers::updateOrCreate(
                ['shippingsId' => $this->shipmentId, 'ContainerNum' => $this->containerData->getContainerNum()],
                [
                    'shippingsId' => $this->shipmentId,
                    'Containercount' => $this->containerData->getContainerCount(),
                    'Container_Line_No' => $this->containerData->getContainerLineNo(),
                    'ContainerNum' => $this->containerData->getContainerNum(),
                    'SealNum' => $this->containerData->getSealNum(),
                    'ContainerMode' => $this->containerData->getContainerMode(),
                    'StorageClass' => $this->containerData->getStorageClass(),
                    'DeliveryMode' => $this->containerData->getDeliveryMode(),
                    'ContainerType' => $this->containerData->getContainerType(),
                    'GrossWeight' => $this->containerData->getGrossWeight(),
                    'TareWeight' => $this->containerData->getTareWeight(),
                    'DepartureEstimatedPickup' => $this->containerData->getDepartureEstimatedPickup(),
                    'EmptyRequired' => $this->containerData->getEmptyRequired(),
                    'ContainerYardEmptyPickupGateOut' => $this->containerData->getContainerYardEmptyPickupGateOut(),
                    'DepartureCartageAdvised' => $this->containerData->getDepartureCartageAdvised(),
                    'DepartureCartageComplete' => $this->containerData->getDepartureCartageComplete(),
                    'ArrivalSlotDateTime' => $this->containerData->getArrivalSlotDateTime(),
                    'ArrivalEstimatedDelivery' => $this->containerData->getArrivalEstimatedDelivery(),
                    'ArrivalDeliveryRequiredBy' => $this->containerData->getArrivalDeliveryRequiredBy(),
                    'ArrivalCartageAdvised' => $this->containerData->getArrivalCartageAdvised(),
                    'ArrivalCartageComplete' => $this->containerData->getArrivalCartageComplete(),
                    'FCLWharfGateIn' => $this->containerData->getFclWharfGateIn(),
                    'FCLOnBoardVessel' => $this->containerData->getFclOnBoardVessel(),
                    'FCLUnloadFromVessel' => $this->containerData->getFclUnloadFromVessel(),
                    'FCLAvailable' => $this->containerData->getFclAvailable(),
                    'FCLWharfGateOut' => $this->containerData->getFclWharfGateOut(),
                    'ArrivalCTOStorageStartDate' => $this->containerData->getArrivalCTOStorageStartDate(),
                    'LCLUnpack' => $this->containerData->getLclUnpack(),
                    'LCLAvailable' => $this->containerData->getLclAvailable(),
                    'EmptyReadyForReturn' => $this->containerData->getEmptyReadyForReturn(),
                    'EmptyReturnedBy' => $this->containerData->getEmptyReturnedBy(),
                    'ContainerYardEmptyReturnGateIn' => $this->containerData->getContainerYardEmptyReturnGateIn()
                ]
            );
        } catch (Exception $e) {
            Log::error('Exception in Container data updateOrCreate', [$this->shipmentNo, $e->getMessage()]);
            return $e->getMessage();
        }
    }

}
