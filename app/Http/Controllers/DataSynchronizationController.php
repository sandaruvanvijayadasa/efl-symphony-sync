<?php

namespace App\Http\Controllers;

use App\Helpers\VariableHelper;
use App\Jobs\ProcessSynchronization;
use App\Jobs\ShipmentSyncingProcess;
use App\Models\Shipments\ShipmentLegs;
use App\Repository\SyncFromSQLDbRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class DataSynchronizationController extends Controller
{

    private $syncFromSQLDbRepository;

    public function __construct(SyncFromSQLDbRepository $syncFromSQLDbRepository)
    {
        $this->syncFromSQLDbRepository = $syncFromSQLDbRepository;
    }

    /**
     * sync shipment & bookings to mysql db
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncDataFromSQLDb(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'consignee' => 'required|string',
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $customer = $request->get('consignee');
        if ($request->has('from_date'))
            $fromDate = $request->get('from_date');
        else
            $fromDate = null;

        if ($request->has('to_date'))
            $toDate = $request->get('to_date');
        else
            $toDate = null;

        $queue = 'shipping_' . $customer;

        ProcessSynchronization::dispatch($customer, $fromDate, $toDate)->onQueue($queue);
    }

    public function syncDataFromSQLDbToSymphony(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'consignee' => 'required|string',
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $customer = $request->get('consignee');
        if ($request->has('from_date'))
            $fromDate = $request->get('from_date');
        else
            $fromDate = null;

        if ($request->has('to_date'))
            $toDate = $request->get('to_date');
        else
            $toDate = null;

        $timeStarted = microtime(true);
        Log::info("Started to process root query | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $result = (new SyncFromSQLDbRepository())->getShipmentHeaderList($customer, $fromDate, $toDate);
        Log::info("Finishing the query execution  | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'records_captured' => count($result)]);
        $timeEnded = microtime(true);

        $queryExecutionTime = ($timeEnded - $timeStarted);

        //pass the data into dispatcher
        if (!is_null($result) && count($result) > 0) {
            foreach ($result as $item) {
                ShipmentSyncingProcess::dispatch($item)->onQueue('shipment-process-queue');
            }
        }

        $responseObject = [
            'message' => 'Dispatch the shipments for processing',
            'shipment_count' => count($result),
            'execution_time' => $queryExecutionTime,
            'status' => 'success'
        ];

        //return the HTTP response
        return response()->json($responseObject, Response::HTTP_OK);
    }

    /**
     * Retrieve container data by given shipment no
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncContainerDataFromSQLDb(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shipment_no' => 'required|string',
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $shipmentNo = $request->get('shipment_no');

        $timeStarted = microtime(true);
        Log::info("Started to retrieving container data | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $response = $this->syncFromSQLDbRepository->getContainers($shipmentNo);
        Log::info("Finishing the retrieving container data | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => count($response)]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['records_count'] = count($response);
        VariableHelper::$success['data'] = $response;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);

    }

    /**
     * Retrieve all events data by given shipment no
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncEventsDataFromSQLDb(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shipment_no' => 'required|string',
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $shipmentNo = $request->get('shipment_no');

        $timeStarted = microtime(true);
        Log::info("Started to retrieving event data | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $response = $this->syncFromSQLDbRepository->getEvents($shipmentNo);
        Log::info("Finishing the retrieving event data | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => count($response)]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['records_count'] = count($response);
        VariableHelper::$success['data'] = $response;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }

    /**
     * Retrieve all data by given shipment no (old header query)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function syncSingleShipment(Request $request)
    {
        set_time_limit(1000);
        $validator = Validator::make($request->all(), [
            'shipment_no' => 'required|string'
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $shipmentNo = $request->get('shipment_no');

        $timeStarted = microtime(true);
        Log::info("Started to retrieving single shipment data by old query | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $response = $this->syncFromSQLDbRepository->getSingleShipment($shipmentNo);
        Log::info("Finishing the retrieving single shipment data by old query | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => count($response)]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['records_count'] = count($response);
        VariableHelper::$success['data'] = $response;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }

    /**
     * Retrieve all data by given shipment no (new header query)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSingleShipment(Request $request)
    {
        set_time_limit(1000);
        $validator = Validator::make($request->all(), [
            'shipment_no' => 'required|string'
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $shipmentNo = $request->get('shipment_no');

        $timeStarted = microtime(true);
        Log::info("Started to retrieving single shipment data by new query | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $response = $this->syncFromSQLDbRepository->getSingleShipmentByNewQuery($shipmentNo);
        Log::info("Finishing the retrieving single shipment data by new query | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => count($response)]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['records_count'] = count($response);
        VariableHelper::$success['data'] = $response;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }

    public function mockTest()
    {
        $from =  date('2021-01-25');
        $to =  date('2021-01-29');

        $legValue = 'legATA4';
        $allLegIds = ShipmentLegs::with([])
            ->whereBetween('updated_at', [$from, $to])
            ->get();

        $wrongMapping = [];
        $wrongMappingIds = [];
        foreach ($allLegIds as $leg){
            if(Carbon::parse($leg[$legValue])->format('Y-m-d H:i') == Carbon::parse($leg->updated_at)->format('Y-m-d H:i')){
                array_push($wrongMapping, $leg);
                array_push($wrongMappingIds, $leg->id);
            }
        }

        $updateResult = ShipmentLegs::with([])
            ->whereIn('id', $wrongMappingIds)
            ->update([
                $legValue => Carbon::parse('1900-01-01')->format('Y-m-d H:i:s')
            ]);

        return [$wrongMapping, $wrongMappingIds, $updateResult];
        Log::info("Finishing the retrieving single shipment data by new query | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => count($response)]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['records_count'] = count($response);
        VariableHelper::$success['data'] = $response;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }

    /**
     * retrieve all shipment & bookings by given consignee & date range
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllShipmentByConsignee(Request $request)
    {
        set_time_limit(3600);
        $validator = Validator::make($request->all(), [
            'consignee' => 'required|string',
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $customer = $request->get('consignee');
        if ($request->has('from_date'))
            $fromDate = $request->get('from_date');
        else
            $fromDate = null;

        if ($request->has('to_date'))
            $toDate = $request->get('to_date');
        else
            $toDate = null;

        $timeStarted = microtime(true);
        Log::info("Started to retrieving all shipment & booking data | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $response = $this->syncFromSQLDbRepository->getAllShipmentsByConsigneeAndDateRange($customer, $fromDate, $toDate);
        Log::info("Finishing the retrieving all shipment & booking data  | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => count($response)]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['records_count'] = count($response);
        VariableHelper::$success['data'] = $response;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }

    /**
     * retrieve all shipment & bookings by given date range
     * get all active consignees inside the function
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllShipmentByDateRange(Request $request)
    {
        set_time_limit(7200);
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        if ($request->has('from_date'))
            $fromDate = $request->get('from_date');
        else
            $fromDate = null;

        if ($request->has('to_date'))
            $toDate = $request->get('to_date');
        else
            $toDate = null;

        $fromDate = Carbon::parse($fromDate)->format('Y-m-d H:i:s');
        if ($toDate == null)
            $toDate = Carbon::now()->format('Y-m-d H:i:s');
        else
            $toDate = Carbon::parse($toDate)->format('Y-m-d H:i:s');

        $timeStarted = microtime(true);
        Log::info("Started to retrieving all shipment & booking data by given date range | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $consignees = $this->syncFromSQLDbRepository->getActiveConsignees();
        $totalRecordsCount = 0;
        $response = null;
        $consigneeResponseData = array();
        foreach ($consignees as $consignee) {
            //get shipment by date range
            $response = $this->syncFromSQLDbRepository->getShipmentsByDateRange($consignee, $fromDate, $toDate);
            $consigneeHasRecordsCount = count($response);
            $totalRecordsCount = $totalRecordsCount + $consigneeHasRecordsCount;
            $consigneeResponseData[$consignee] = [
                'records_count' => $consigneeHasRecordsCount,
                'records' => $response
            ];
        }
        Log::info("Finishing the retrieving all shipment & booking data by given date range | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => $totalRecordsCount]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['consignee_count'] = count($consignees);
        VariableHelper::$success['records_count'] = $totalRecordsCount;
        VariableHelper::$success['data'] = $consigneeResponseData;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }

    /**
     * retrieve all shipment & bookings by given date range & multiple consignees
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllShipmentsByGivenArrayOfConsignees(Request $request)
    {
        set_time_limit(3600);
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'consignees' => 'required|array'
        ]);

        if ($validator->fails()) {
            Log::error('Request parameter validation error', [$validator->errors()]);
            VariableHelper::$response['message'] = $validator->errors();
            return response()->json(VariableHelper::$response, Response::HTTP_BAD_REQUEST);
        }

        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        $consignees = $request->get('consignees');

        $fromDate = Carbon::parse($fromDate)->format('Y-m-d H:i:s');
        if ($toDate == "")
            $toDate = Carbon::now()->format('Y-m-d H:i:s');
        else
            $toDate = Carbon::parse($toDate)->format('Y-m-d H:i:s');

        $timeStarted = microtime(true);
        Log::info("Started to retrieving all shipment & booking data by given multiple consignees | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s')]);
        $totalRecordsCount = 0;
        $response = null;
        $consigneeResponseData = array();
        foreach ($consignees as $consignee) {
            //get shipment by date range
            $response = $this->syncFromSQLDbRepository->getShipmentsByDateRange($consignee, $fromDate, $toDate);
            $consigneeHasRecordsCount = count($response);
            $totalRecordsCount = $totalRecordsCount + $consigneeHasRecordsCount;
            $consigneeResponseData[$consignee] = [
                'records_count' => $consigneeHasRecordsCount,
                'records' => $response
            ];
        }
        Log::info("Finishing the retrieving all shipment & booking data by given multiple consignees | ", ['timestamp' => Carbon::now()->format('Y-m-d H:i:s'), 'response_count' => $totalRecordsCount]);
        $timeEnded = microtime(true);

        $totalTime = ($timeEnded - $timeStarted);

        VariableHelper::$success['message'] = 'Query execution success!';
        VariableHelper::$success['execution_time'] = $totalTime;
        VariableHelper::$success['consignee_count'] = count($consignees);
        VariableHelper::$success['records_count'] = $totalRecordsCount;
        VariableHelper::$success['data'] = $consigneeResponseData;

        return response()->json(VariableHelper::$success, Response::HTTP_OK);
    }
}
