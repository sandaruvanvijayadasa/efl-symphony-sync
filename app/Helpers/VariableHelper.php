<?php


namespace App\Helpers;


use Symfony\Component\HttpFoundation\Response;

class VariableHelper
{
    static $success = ['status_code' => Response::HTTP_OK, 'status' => 'success', 'error' => false];
    static $error = ['status_code' => Response::HTTP_NOT_FOUND, 'status' => 'error', 'error' => true];
    static $response = ['status_code' => Response::HTTP_BAD_REQUEST, 'status' => 'error', 'error' => true];
    static $exist = ['status_code' => Response::HTTP_ALREADY_REPORTED, 'status' => 'error', 'error' => true];
    static $forbidden = ['status_code' => Response::HTTP_FORBIDDEN, 'status' => 'error', 'error' => true];
    static $unauthorized = ['status_code' => Response::HTTP_UNAUTHORIZED, 'status' => 'error', 'error' => true];
}
