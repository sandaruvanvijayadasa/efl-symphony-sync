<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'customers';

    protected $fillable = [
        'orgPrimaryKey',
        'orgCode',
        'orgName',
        'orgIsActive',
        'orgIsConsignee',
        'orgIsConsignor',
        'orgIsForwarder',
        'orgIsAgentWise',
        'orgAddressLine1',
        'orgAddressLine2',
        'orgCity',
        'orgPostalCode',
        'orgUnloco',
        'orgState',
        'orgPhone',
        'orgMobile',
        'orgFax',
        'orgEmail',
        'orgWeb',
        'orgLanguage',
        'orgCountryCode',
        'orgCountryName',
        'orgRegistrationNumber',
        'orgSalePerson',
        'orgSalePersonEmail',
        'orgSalePersonMobile',
        'orgAccountPerson',
        'orgAccountEmail',
        'orgAccountMobile',
        'orgLogo',
        'orgLastEditDateTimeUTC',
        'orgCustomerFlag',
        'orgEflFlag',
        'orgCwFlag',
        'orgStatus',
    ];
}
