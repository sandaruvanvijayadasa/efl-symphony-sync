<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerUsers extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'customer_users';
}
