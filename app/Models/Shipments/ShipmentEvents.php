<?php

namespace App\Models\Shipments;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentEvents extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'shipment_events';

    protected $fillable = [
        'shippingsId',
        'evenType',
        'eventDate',
        'eventDetail'
    ];
}
