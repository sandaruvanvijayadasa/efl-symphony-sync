<?php

namespace App\Models\Shipments;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentLegs extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'shipment_legs';

    protected $fillable = [
        'shippingsId',
        'Leg_Order1',
        'VoyageFlight1',
        'Vessel1',
        'legETD1',
        'legETA1',
        'legATD1',
        'legATA1',
        'leg1load',
        'leg1disc',
        'mode1',
        'Leg_Order2',
        'VoyageFlight2',
        'Vessel2',
        'legETD2',
        'legETA2',
        'legATD2',
        'legATA2',
        'leg2load',
        'leg2disc',
        'mode2',
        'Leg_Order3',
        'VoyageFlight3',
        'Vessel3',
        'legETD3',
        'legETA3',
        'legATD3',
        'legATA3',
        'leg3load',
        'leg3disc',
        'mode3',
        'Leg_Order4',
        'VoyageFlight4',
        'Vessel4',
        'legETD4',
        'legETA4',
        'legATD4',
        'legATA4',
        'leg4load',
        'leg4disc',
        'mode4',
    ];
}
