<?php

namespace App\Models\Shipments;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentLines extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'shipment_lines';

    protected $fillable = [
        'poNumber',
        'jlPk',
        'lineNumber',
        'lineDescription',
        'lineOuterPacks',
        'lineType',
        'LineActualWeight',
        'LineUnitofweight',
        'LineVolume',
        'LineUnitofVolume',
        'LineWidth',
        'LineHeight',
        'LineLength',
        'PackJCContainerNum',
        'PackJCSealNum',
        'PackRCCode'
    ];
}
