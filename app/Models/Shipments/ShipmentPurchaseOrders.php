<?php

namespace App\Models\Shipments;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentPurchaseOrders extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'shipment_purchase_orders';

    protected $fillable = [
        'shippingsId',
        'orderNumber',
        'buyerCode',
        'supplierCode',
        'confirmNumber',
        'invoiceNumber',
        'oderNumberDate',
        'invoiceNumberDate',
        'confirmNumberDate',
        'followUpDate',
        'requireInstoDate',
        'orderStatus',
        'requireExWorkDate',
        'orderGoodDescription',
        'currency',
        'orderServiceLevel',
        'orderINCOTerm',
        'AdditionalTerms',
        'OrderTransMode',
        'OrderCountryOrigin',
    ];
}
