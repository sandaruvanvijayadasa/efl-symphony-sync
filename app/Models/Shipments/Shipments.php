<?php

namespace App\Models\Shipments;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipments extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'shipments';

    protected $fillable = [
        'JS_UniqueConsignRef',
        'JS_IsForwardRegistered',
        'BookedPallets',
        'BookedWeight',
        'JS_TransportMode',
        'JS_UnitOfWeight',
        'JS_UnitOfVolume',
        'JS_UnitOfActualChargeable',
        'JS_F3_NKPackType',
        'JS_RS_NKServiceLevel',
        'LoadPort',
        'DischargePort',
        'JS_SystemLastEditTimeUtc',
        'JS_INCO',
        'JS_PackingMode',
        'JS_ShipmentType',
        'Carrier_CODE',
        'GoodsDescription',
        'Voyage_Flight',
        'vessels',
        'ETD',
        'ETA',
        'ExpoRepresentative',
        'ShipperRepresentative',
        'JS_RL_NKOrigin',
        'JS_RL_NKDestination',
        'JS_ActualChargeable',
        'BookedTEU',
        'ShippersRef',
        'VehicleNo',
        'DriverName',
        'Received_Date',
        'CustomsCheck',
        'MarksNumber',
        'Remarks',
        'JD_OrderNumber',
        'JK_MasterBillNum',
        'ConsignorCode',
        'LocalClient',
        'ConsigneeCode',
        'ATD',
        'ATA',
        'ConfirmatAirline',
        'JS_ActualWeight',
        'JS_ActualVolume',
        'JS_OuterPacks',
        'HAWB',
        'is_active',
        'JS_SystemCreateTimeUtc',
        'CarrierName',
        'SendAgentCode',
        'SendAgentName',
        'RecvAgentCode',
        'RecvAgentName',
        'ActualDeliverydate'
    ];
}
