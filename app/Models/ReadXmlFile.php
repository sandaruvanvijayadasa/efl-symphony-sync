<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadXmlFile extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'read_xml_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fileName',
        'readStatus'
    ];
}
