<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingLines extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'booking_lines';

    protected $fillable = [
        'poNumber',
        'jlPk',
        'lineNumber',
        'lineDescription',
        'lineOuterPacks',
        'lineType',
        'LineActualWeight',
        'LineUnitofweight',
        'LineVolume',
        'LineUnitofVolume',
        'LineWidth',
        'LineHeight',
        'LineLength',
        'PackJCContainerNum',
        'PackJCSealNum',
        'PackRCCode'
    ];
}
