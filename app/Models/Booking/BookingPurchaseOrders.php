<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingPurchaseOrders extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'booking_purchase_orders';

    protected $fillable = [
        'bookingShippingsId',
        'orderNumber',
        'buyerCode',
        'supplierCode',
        'confirmNumber',
        'invoiceNumber',
        'oderNumberDate',
        'invoiceNumberDate',
        'confirmNumberDate',
        'followUpDate',
        'requireInstoDate',
        'orderStatus',
        'requireExWorkDate',
        'orderGoodDescription',
        'currency',
        'orderServiceLevel',
        'orderINCOTerm',
        'AdditionalTerms',
        'OrderTransMode',
        'OrderCountryOrigin',
    ];
}
