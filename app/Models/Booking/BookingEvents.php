<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingEvents extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'booking_events';

    protected $fillable = [
        'bookingShippingsId',
        'evenType',
        'eventDate',
        'eventDetail'
    ];
}
