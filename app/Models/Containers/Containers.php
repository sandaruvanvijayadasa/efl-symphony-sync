<?php

namespace App\Models\Containers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Containers extends Model
{
    use HasFactory;

    protected $connection = "mysql";
    protected $table = 'containers';

    protected $fillable = [
        'shippingsId',
        'Containercount',
        'Container_Line_No',
        'ContainerNum',
        'SealNum',
        'ContainerMode',
        'StorageClass',
        'DeliveryMode',
        'ContainerType',
        'GrossWeight',
        'TareWeight',
        'DepartureEstimatedPickup',
        'EmptyRequired',
        'ContainerYardEmptyPickupGateOut',
        'DepartureCartageAdvised',
        'DepartureCartageComplete',
        'ArrivalSlotDateTime',
        'ArrivalEstimatedDelivery',
        'ArrivalDeliveryRequiredBy',
        'ArrivalCartageAdvised',
        'ArrivalCartageComplete',
        'FCLWharfGateIn',
        'FCLOnBoardVessel',
        'FCLUnloadFromVessel',
        'FCLAvailable',
        'FCLWharfGateOut',
        'ArrivalCTOStorageStartDate',
        'LCLUnpack',
        'LCLAvailable',
        'EmptyReadyForReturn',
        'EmptyReturnedBy',
        'ContainerYardEmptyReturnGateIn',
    ];
}
