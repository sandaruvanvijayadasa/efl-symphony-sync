<?php


namespace App\Models\DataSyncModels\Booking;


use Carbon\Carbon;

class BookingShipmentsSynchronization
{

    protected $uniqueConsignRef;
    protected $isForwardRegistered;
    protected $bookedPallets;
    protected $bookedWeight;
    protected $transportMode;
    protected $unitOfWeight;
    protected $unitOfVolume;
    protected $unitOfActualChargeable;
    protected $f3NkPackType;
    protected $rsNkServiceLevel;
    protected $loadPort;
    protected $dischargePort;
    protected $systemLastEditTimeUtc;
    protected $incoTerm;
    protected $packingMode;
    protected $shipmentType;
    protected $carrierCode;
    protected $goodsDescription;
    protected $voyageFlight;
    protected $vessels;
    protected $ETD;
    protected $ETA;
    protected $expoRepresentative;
    protected $shipperRepresentative;
    protected $rlNkOrigin;
    protected $rlNkDestination;
    protected $actualChargeable;
    protected $bookedTEU;
    protected $shippersRef;
    protected $vehicleNo;
    protected $driverName;
    protected $receivedDate;
    protected $customsCheck;
    protected $marksNumber;
    protected $remarks;
    protected $orderNumber;
    protected $masterBillNum;
    protected $consignorCode;
    protected $localClient;
    protected $consigneeCode;
    protected $ATD;
    protected $ATA;
    protected $confirmatAirline;
    protected $actualWeight;
    protected $actualVolume;
    protected $outerPacks;
    protected $HAWB;
    protected $carrierName;
    protected $sendAgentCode;
    protected $sendAgentName;
    protected $recvAgentCode;
    protected $recvAgentName;
    protected $systemCreateTimeUtc;
    protected $actualDeliveryDate;

    /**
     * @return mixed
     */
    public function getUniqueConsignRef()
    {
        if (is_array($this->uniqueConsignRef) && count($this->uniqueConsignRef) == 0) {
            return null;
        } else {
            return $this->uniqueConsignRef;
        }
    }

    /**
     * @param mixed $uniqueConsignRef
     */
    public function setUniqueConsignRef($uniqueConsignRef): void
    {
        $this->uniqueConsignRef = $uniqueConsignRef;
    }

    /**
     * @return mixed
     */
    public function getIsForwardRegistered()
    {
        if (is_array($this->isForwardRegistered) && count($this->isForwardRegistered) == 0) {
            return null;
        } else {
            return $this->isForwardRegistered;
        }
    }

    /**
     * @param mixed $isForwardRegistered
     */
    public function setIsForwardRegistered($isForwardRegistered): void
    {
        if ($isForwardRegistered == "false") {
            $this->isForwardRegistered = false;
        } else {
            $this->isForwardRegistered = true;
        }
    }

    /**
     * @return mixed
     */
    public function getBookedPallets()
    {
        if (is_array($this->bookedPallets) && count($this->bookedPallets) == 0) {
            return null;
        } else {
            return $this->bookedPallets;
        }
    }

    /**
     * @param mixed $bookedPallets
     */
    public function setBookedPallets($bookedPallets): void
    {
        $this->bookedPallets = $bookedPallets;
    }

    /**
     * @return mixed
     */
    public function getBookedWeight()
    {
        if (is_array($this->bookedWeight) && count($this->bookedWeight) == 0) {
            return null;
        } else {
            return $this->bookedWeight;
        }
    }

    /**
     * @param mixed $bookedWeight
     */
    public function setBookedWeight($bookedWeight): void
    {
        $this->bookedWeight = $bookedWeight;
    }

    /**
     * @return mixed
     */
    public function getTransportMode()
    {
        if (is_array($this->transportMode) && count($this->transportMode) == 0) {
            return null;
        } else {
            return $this->transportMode;
        }
    }

    /**
     * @param mixed $transportMode
     */
    public function setTransportMode($transportMode): void
    {
        $this->transportMode = $transportMode;
    }

    /**
     * @return mixed
     */
    public function getUnitOfWeight()
    {
        if (is_array($this->unitOfWeight) && count($this->unitOfWeight) == 0) {
            return null;
        } else {
            return $this->unitOfWeight;
        }
    }

    /**
     * @param mixed $unitOfWeight
     */
    public function setUnitOfWeight($unitOfWeight): void
    {
        $this->unitOfWeight = $unitOfWeight;
    }

    /**
     * @return mixed
     */
    public function getUnitOfVolume()
    {
        if (is_array($this->unitOfVolume) && count($this->unitOfVolume) == 0) {
            return null;
        } else {
            return $this->unitOfVolume;
        }
    }

    /**
     * @param mixed $unitOfVolume
     */
    public function setUnitOfVolume($unitOfVolume): void
    {
        $this->unitOfVolume = $unitOfVolume;
    }

    /**
     * @return mixed
     */
    public function getUnitOfActualChargeable()
    {
        if (is_array($this->unitOfActualChargeable) && count($this->unitOfActualChargeable) == 0) {
            return null;
        } else {
            return $this->unitOfActualChargeable;
        }
    }

    /**
     * @param mixed $unitOfActualChargeable
     */
    public function setUnitOfActualChargeable($unitOfActualChargeable): void
    {
        $this->unitOfActualChargeable = $unitOfActualChargeable;
    }

    /**
     * @return mixed
     */
    public function getF3NkPackType()
    {
        if (is_array($this->f3NkPackType) && count($this->f3NkPackType) == 0) {
            return null;
        } else {
            return $this->f3NkPackType;
        }
    }

    /**
     * @param mixed $f3NkPackType
     */
    public function setF3NkPackType($f3NkPackType): void
    {
        $this->f3NkPackType = $f3NkPackType;
    }

    /**
     * @return mixed
     */
    public function getRsNkServiceLevel()
    {
        if (is_array($this->rsNkServiceLevel) && count($this->rsNkServiceLevel) == 0) {
            return null;
        } else {
            return $this->rsNkServiceLevel;
        }
    }

    /**
     * @param mixed $rsNkServiceLevel
     */
    public function setRsNkServiceLevel($rsNkServiceLevel): void
    {
        $this->rsNkServiceLevel = $rsNkServiceLevel;
    }

    /**
     * @return mixed
     */
    public function getLoadPort()
    {
        if (is_array($this->loadPort) && count($this->loadPort) == 0) {
            return null;
        } else {
            return $this->loadPort;
        }
    }

    /**
     * @param mixed $loadPort
     */
    public function setLoadPort($loadPort): void
    {
        $this->loadPort = $loadPort;
    }

    /**
     * @return mixed
     */
    public function getDischargePort()
    {
        if (is_array($this->dischargePort) && count($this->dischargePort) == 0) {
            return null;
        } else {
            return $this->dischargePort;
        }
    }

    /**
     * @param mixed $dischargePort
     */
    public function setDischargePort($dischargePort): void
    {
        $this->dischargePort = $dischargePort;
    }

    /**
     * @return mixed
     */
    public function getSystemLastEditTimeUtc()
    {
        if (is_array($this->systemLastEditTimeUtc) && count($this->systemLastEditTimeUtc) == 0) {
            return null;
        } else {
            return Carbon::parse($this->systemLastEditTimeUtc)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $systemLastEditTimeUtc
     */
    public function setSystemLastEditTimeUtc($systemLastEditTimeUtc): void
    {
        $this->systemLastEditTimeUtc = $systemLastEditTimeUtc;
    }

    /**
     * @return mixed
     */
    public function getIncoTerm()
    {
        if (is_array($this->incoTerm) && count($this->incoTerm) == 0) {
            return null;
        } else {
            return $this->incoTerm;
        }
    }

    /**
     * @param mixed $incoTerm
     */
    public function setIncoTerm($incoTerm): void
    {
        $this->incoTerm = $incoTerm;
    }

    /**
     * @return mixed
     */
    public function getPackingMode()
    {
        if (is_array($this->packingMode) && count($this->packingMode) == 0) {
            return null;
        } else {
            return $this->packingMode;
        }
    }

    /**
     * @param mixed $packingMode
     */
    public function setPackingMode($packingMode): void
    {
        $this->packingMode = $packingMode;
    }

    /**
     * @return mixed
     */
    public function getShipmentType()
    {
        if (is_array($this->shipmentType) && count($this->shipmentType) == 0) {
            return null;
        } else {
            return $this->shipmentType;
        }
    }

    /**
     * @param mixed $shipmentType
     */
    public function setShipmentType($shipmentType): void
    {
        $this->shipmentType = $shipmentType;
    }

    /**
     * @return mixed
     */
    public function getCarrierCode()
    {
        if (is_array($this->carrierCode) && count($this->carrierCode) == 0) {
            return null;
        } else {
            return $this->carrierCode;
        }
    }

    /**
     * @param mixed $carrierCode
     */
    public function setCarrierCode($carrierCode): void
    {
        $this->carrierCode = $carrierCode;
    }

    /**
     * @return mixed
     */
    public function getGoodsDescription()
    {
        if (is_array($this->goodsDescription) && count($this->goodsDescription) == 0) {
            return null;
        } else {
            return $this->goodsDescription;
        }
    }

    /**
     * @param mixed $goodsDescription
     */
    public function setGoodsDescription($goodsDescription): void
    {
        $this->goodsDescription = $goodsDescription;
    }

    /**
     * @return mixed
     */
    public function getVoyageFlight()
    {
        if (is_array($this->voyageFlight) && count($this->voyageFlight) == 0) {
            return null;
        } else {
            return $this->voyageFlight;
        }
    }

    /**
     * @param mixed $voyageFlight
     */
    public function setVoyageFlight($voyageFlight): void
    {
        $this->voyageFlight = $voyageFlight;
    }

    /**
     * @return mixed
     */
    public function getVessels()
    {
        if (is_array($this->vessels) && count($this->vessels) == 0) {
            return null;
        } else {
            return $this->vessels;
        }
    }

    /**
     * @param mixed $vessels
     */
    public function setVessels($vessels): void
    {
        $this->vessels = $vessels;
    }

    /**
     * @return mixed
     */
    public function getETD()
    {
        if (is_array($this->ETD) && count($this->ETD) == 0) {
            return null;
        } else {
            return Carbon::parse($this->ETD)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $ETD
     */
    public function setETD($ETD): void
    {
        $this->ETD = $ETD;
    }

    /**
     * @return mixed
     */
    public function getETA()
    {
        if (is_array($this->ETA) && count($this->ETA) == 0) {
            return null;
        } else {
            return Carbon::parse($this->ETA)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $ETA
     */
    public function setETA($ETA): void
    {
        $this->ETA = $ETA;
    }

    /**
     * @return mixed
     */
    public function getExpoRepresentative()
    {
        if (is_array($this->expoRepresentative) && count($this->expoRepresentative) == 0) {
            return null;
        } else {
            return $this->expoRepresentative;
        }
    }

    /**
     * @param mixed $expoRepresentative
     */
    public function setExpoRepresentative($expoRepresentative): void
    {
        $this->expoRepresentative = $expoRepresentative;
    }

    /**
     * @return mixed
     */
    public function getShipperRepresentative()
    {
        if (is_array($this->shipperRepresentative) && count($this->shipperRepresentative) == 0) {
            return null;
        } else {
            return $this->shipperRepresentative;
        }
    }

    /**
     * @param mixed $shipperRepresentative
     */
    public function setShipperRepresentative($shipperRepresentative): void
    {
        $this->shipperRepresentative = $shipperRepresentative;
    }

    /**
     * @return mixed
     */
    public function getRlNkOrigin()
    {
        if (is_array($this->rlNkOrigin) && count($this->rlNkOrigin) == 0) {
            return null;
        } else {
            return $this->rlNkOrigin;
        }
    }

    /**
     * @param mixed $rlNkOrigin
     */
    public function setRlNkOrigin($rlNkOrigin): void
    {
        $this->rlNkOrigin = $rlNkOrigin;
    }

    /**
     * @return mixed
     */
    public function getRlNkDestination()
    {
        if (is_array($this->rlNkDestination) && count($this->rlNkDestination) == 0) {
            return null;
        } else {
            return $this->rlNkDestination;
        }
    }

    /**
     * @param mixed $rlNkDestination
     */
    public function setRlNkDestination($rlNkDestination): void
    {
        $this->rlNkDestination = $rlNkDestination;
    }

    /**
     * @return mixed
     */
    public function getActualChargeable()
    {
        if (is_array($this->actualChargeable) && count($this->actualChargeable) == 0) {
            return null;
        } else {
            return $this->actualChargeable;
        }
    }

    /**
     * @param mixed $actualChargeable
     */
    public function setActualChargeable($actualChargeable): void
    {
        $this->actualChargeable = $actualChargeable;
    }

    /**
     * @return mixed
     */
    public function getBookedTEU()
    {
        if (is_array($this->bookedTEU) && count($this->bookedTEU) == 0) {
            return null;
        } else {
            return $this->bookedTEU;
        }
    }

    /**
     * @param mixed $bookedTEU
     */
    public function setBookedTEU($bookedTEU): void
    {
        $this->bookedTEU = $bookedTEU;
    }

    /**
     * @return mixed
     */
    public function getShippersRef()
    {
        if (is_array($this->shippersRef) && count($this->shippersRef) == 0) {
            return null;
        } else {
            return $this->shippersRef;
        }
    }

    /**
     * @param mixed $shippersRef
     */
    public function setShippersRef($shippersRef): void
    {
        $this->shippersRef = $shippersRef;
    }

    /**
     * @return mixed
     */
    public function getVehicleNo()
    {
        if (is_array($this->vehicleNo) && count($this->vehicleNo) == 0) {
            return null;
        } else {
            return $this->vehicleNo;
        }
    }

    /**
     * @param mixed $vehicleNo
     */
    public function setVehicleNo($vehicleNo): void
    {
        $this->vehicleNo = $vehicleNo;
    }

    /**
     * @return mixed
     */
    public function getDriverName()
    {
        if (is_array($this->driverName) && count($this->driverName) == 0) {
            return null;
        } else {
            return $this->driverName;
        }
    }

    /**
     * @param mixed $driverName
     */
    public function setDriverName($driverName): void
    {
        $this->driverName = $driverName;
    }

    /**
     * @return mixed
     */
    public function getReceivedDate()
    {
        if (is_array($this->receivedDate) && count($this->receivedDate) == 0) {
            return null;
        } else {
            return Carbon::parse($this->receivedDate)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $receivedDate
     */
    public function setReceivedDate($receivedDate): void
    {
        $this->receivedDate = $receivedDate;
    }

    /**
     * @return mixed
     */
    public function getCustomsCheck()
    {
        if (is_array($this->customsCheck) && count($this->customsCheck) == 0) {
            return null;
        } else {
            return Carbon::parse($this->customsCheck)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $customsCheck
     */
    public function setCustomsCheck($customsCheck): void
    {
        $this->customsCheck = $customsCheck;
    }

    /**
     * @return mixed
     */
    public function getMarksNumber()
    {
        if (is_array($this->marksNumber) && count($this->marksNumber) == 0) {
            return null;
        } else {
            return $this->marksNumber;
        }
    }

    /**
     * @param mixed $marksNumber
     */
    public function setMarksNumber($marksNumber): void
    {
        $this->marksNumber = $marksNumber;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        if (is_array($this->remarks) && count($this->remarks) == 0) {
            return null;
        } else {
            return $this->remarks;
        }
    }

    /**
     * @param mixed $remarks
     */
    public function setRemarks($remarks): void
    {
        $this->remarks = $remarks;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        if (is_array($this->orderNumber) && count($this->orderNumber) == 0) {
            return null;
        } else {
            return $this->orderNumber;
        }
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getMasterBillNum()
    {
        if (is_array($this->masterBillNum) && count($this->masterBillNum) == 0) {
            return null;
        } else {
            return $this->masterBillNum;
        }
    }

    /**
     * @param mixed $masterBillNum
     */
    public function setMasterBillNum($masterBillNum): void
    {
        $this->masterBillNum = $masterBillNum;
    }

    /**
     * @return mixed
     */
    public function getConsignorCode()
    {
        if (is_array($this->consignorCode) && count($this->consignorCode) == 0) {
            return null;
        } else {
            return $this->consignorCode;
        }
    }

    /**
     * @param mixed $consignorCode
     */
    public function setConsignorCode($consignorCode): void
    {
        $this->consignorCode = $consignorCode;
    }

    /**
     * @return mixed
     */
    public function getLocalClient()
    {
        if (is_array($this->localClient) && count($this->localClient) == 0) {
            return null;
        } else {
            return $this->localClient;
        }
    }

    /**
     * @param mixed $localClient
     */
    public function setLocalClient($localClient): void
    {
        $this->localClient = $localClient;
    }

    /**
     * @return mixed
     */
    public function getConsigneeCode()
    {
        if (is_array($this->consigneeCode) && count($this->consigneeCode) == 0) {
            return null;
        } else {
            return $this->consigneeCode;
        }
    }

    /**
     * @param mixed $consigneeCode
     */
    public function setConsigneeCode($consigneeCode): void
    {
        $this->consigneeCode = $consigneeCode;
    }

    /**
     * @return mixed
     */
    public function getATD()
    {
        if (is_array($this->ATD) && count($this->ATD) == 0) {
            return null;
        } else {
            return Carbon::parse($this->ATD)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $ATD
     */
    public function setATD($ATD): void
    {
        $this->ATD = $ATD;
    }

    /**
     * @return mixed
     */
    public function getATA()
    {
        if (is_array($this->ATA) && count($this->ATA) == 0) {
            return null;
        } else {
            return Carbon::parse($this->ATA)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $ATA
     */
    public function setATA($ATA): void
    {
        $this->ATA = $ATA;
    }

    /**
     * @return mixed
     */
    public function getConfirmatAirline()
    {
        if (is_array($this->confirmatAirline) && count($this->confirmatAirline) == 0) {
            return null;
        } else {
            return $this->confirmatAirline;
        }
    }

    /**
     * @param mixed $confirmatAirline
     */
    public function setConfirmatAirline($confirmatAirline): void
    {
        $this->confirmatAirline = $confirmatAirline;
    }

    /**
     * @return mixed
     */
    public function getActualWeight()
    {
        if (is_array($this->actualWeight) && count($this->actualWeight) == 0) {
            return null;
        } else {
            return $this->actualWeight;
        }
    }

    /**
     * @param mixed $actualWeight
     */
    public function setActualWeight($actualWeight): void
    {
        $this->actualWeight = $actualWeight;
    }

    /**
     * @return mixed
     */
    public function getActualVolume()
    {
        if (is_array($this->actualVolume) && count($this->actualVolume) == 0) {
            return null;
        } else {
            return $this->actualVolume;
        }
    }

    /**
     * @param mixed $actualVolume
     */
    public function setActualVolume($actualVolume): void
    {
        $this->actualVolume = $actualVolume;
    }

    /**
     * @return mixed
     */
    public function getOuterPacks()
    {
        if (is_array($this->outerPacks) && count($this->outerPacks) == 0) {
            return null;
        } else {
            return $this->outerPacks;
        }
    }

    /**
     * @param mixed $outerPacks
     */
    public function setOuterPacks($outerPacks): void
    {
        $this->outerPacks = $outerPacks;
    }

    /**
     * @return mixed
     */
    public function getHAWB()
    {
        if (is_array($this->HAWB) && count($this->HAWB) == 0) {
            return null;
        } else {
            return $this->HAWB;
        }
    }

    /**
     * @param mixed $HAWB
     */
    public function setHAWB($HAWB): void
    {
        $this->HAWB = $HAWB;
    }

    /**
     * @return mixed
     */
    public function getCarrierName()
    {
        if ($this->carrierName == '') {
            return null;
        } else {
            return $this->carrierName;
        }
    }

    /**
     * @param mixed $carrierName
     */
    public function setCarrierName($carrierName): void
    {
        $this->carrierName = $carrierName;
    }

    /**
     * @return mixed
     */
    public function getSendAgentCode()
    {
        if ($this->sendAgentCode == '') {
            return null;
        } else {
            return $this->sendAgentCode;
        }
    }

    /**
     * @param mixed $sendAgentCode
     */
    public function setSendAgentCode($sendAgentCode): void
    {
        $this->sendAgentCode = $sendAgentCode;
    }

    /**
     * @return mixed
     */
    public function getSendAgentName()
    {
        if ($this->sendAgentName == '') {
            return null;
        } else {
            return $this->sendAgentName;
        }
    }

    /**
     * @param mixed $sendAgentName
     */
    public function setSendAgentName($sendAgentName): void
    {
        $this->sendAgentName = $sendAgentName;
    }

    /**
     * @return mixed
     */
    public function getRecvAgentCode()
    {
        if ($this->recvAgentCode == '') {
            return null;
        } else {
            return $this->recvAgentCode;
        }
    }

    /**
     * @param mixed $recvAgentCode
     */
    public function setRecvAgentCode($recvAgentCode): void
    {
        $this->recvAgentCode = $recvAgentCode;
    }

    /**
     * @return mixed
     */
    public function getRecvAgentName()
    {
        if ($this->recvAgentName == '') {
            return null;
        } else {
            return $this->recvAgentName;
        }
    }

    /**
     * @param mixed $recvAgentName
     */
    public function setRecvAgentName($recvAgentName): void
    {
        $this->recvAgentName = $recvAgentName;
    }

    /**
     * @return mixed
     */
    public function getSystemCreateTimeUtc()
    {
        if (is_array($this->systemCreateTimeUtc) && count($this->systemCreateTimeUtc) == 0) {
            return null;
        } else {
            return Carbon::parse($this->systemCreateTimeUtc)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $systemCreateTimeUtc
     */
    public function setSystemCreateTimeUtc($systemCreateTimeUtc): void
    {
        $this->systemCreateTimeUtc = $systemCreateTimeUtc;
    }

    /**
     * @return mixed
     */
    public function getActualDeliveryDate()
    {
        if (is_array($this->actualDeliveryDate) && count($this->actualDeliveryDate) == 0) {
            return null;
        } else {
            return Carbon::parse($this->actualDeliveryDate)->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $actualDeliveryDate
     */
    public function setActualDeliveryDate($actualDeliveryDate): void
    {
        $this->actualDeliveryDate = $actualDeliveryDate;
    }

}
