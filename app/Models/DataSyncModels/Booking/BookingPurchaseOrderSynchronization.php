<?php


namespace App\Models\DataSyncModels\Booking;


use Carbon\Carbon;

class BookingPurchaseOrderSynchronization
{

    private $bookingShippingsId;
    private $orderNumber;
    private $buyerCode;
    private $supplierCode;
    private $confirmNumber;
    private $invoiceNumber;
    private $oderNumberDate;
    private $invoiceNumberDate;
    private $confirmNumberDate;
    private $followUpDate;
    private $requireInstoDate;
    private $orderStatus;
    private $requireExWorkDate;
    private $orderGoodDescription;
    private $currency;
    private $orderServiceLevel;
    private $orderINCOTerm;
    private $additionalTerms;
    private $orderTransMode;
    private $orderCountryOrigin;

    /**
     * @return mixed
     */
    public function getBookingShippingsId()
    {
        return $this->bookingShippingsId;
    }

    /**
     * @param mixed $bookingShippingsId
     */
    public function setBookingShippingsId($bookingShippingsId): void
    {
        $this->bookingShippingsId = $bookingShippingsId;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getBuyerCode()
    {
        return $this->buyerCode;
    }

    /**
     * @param mixed $buyerCode
     */
    public function setBuyerCode($buyerCode): void
    {
        $this->buyerCode = $buyerCode;
    }

    /**
     * @return mixed
     */
    public function getSupplierCode()
    {
        return $this->supplierCode;
    }

    /**
     * @param mixed $supplierCode
     */
    public function setSupplierCode($supplierCode): void
    {
        $this->supplierCode = $supplierCode;
    }

    /**
     * @return mixed
     */
    public function getConfirmNumber()
    {
        return $this->confirmNumber;
    }

    /**
     * @param mixed $confirmNumber
     */
    public function setConfirmNumber($confirmNumber): void
    {
        $this->confirmNumber = $confirmNumber;
    }

    /**
     * @return mixed
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param mixed $invoiceNumber
     */
    public function setInvoiceNumber($invoiceNumber): void
    {
        $this->invoiceNumber = $invoiceNumber;
    }

    /**
     * @return mixed
     */
    public function getOderNumberDate()
    {
        return Carbon::parse($this->oderNumberDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $oderNumberDate
     */
    public function setOderNumberDate($oderNumberDate): void
    {
        $this->oderNumberDate = $oderNumberDate;
    }

    /**
     * @return mixed
     */
    public function getInvoiceNumberDate()
    {
        return Carbon::parse($this->invoiceNumberDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $invoiceNumberDate
     */
    public function setInvoiceNumberDate($invoiceNumberDate): void
    {
        $this->invoiceNumberDate = $invoiceNumberDate;
    }

    /**
     * @return mixed
     */
    public function getConfirmNumberDate()
    {
        return Carbon::parse($this->confirmNumberDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $confirmNumberDate
     */
    public function setConfirmNumberDate($confirmNumberDate): void
    {
        $this->confirmNumberDate = $confirmNumberDate;
    }

    /**
     * @return mixed
     */
    public function getFollowUpDate()
    {
        return Carbon::parse($this->followUpDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $followUpDate
     */
    public function setFollowUpDate($followUpDate): void
    {
        $this->followUpDate = $followUpDate;
    }

    /**
     * @return mixed
     */
    public function getRequireInstoDate()
    {
        return Carbon::parse($this->requireInstoDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $requireInstoDate
     */
    public function setRequireInstoDate($requireInstoDate): void
    {
        $this->requireInstoDate = $requireInstoDate;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param mixed $orderStatus
     */
    public function setOrderStatus($orderStatus): void
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return mixed
     */
    public function getRequireExWorkDate()
    {
        return Carbon::parse($this->requireExWorkDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $requireExWorkDate
     */
    public function setRequireExWorkDate($requireExWorkDate): void
    {
        $this->requireExWorkDate = $requireExWorkDate;
    }

    /**
     * @return mixed
     */
    public function getOrderGoodDescription()
    {
        return $this->orderGoodDescription;
    }

    /**
     * @param mixed $orderGoodDescription
     */
    public function setOrderGoodDescription($orderGoodDescription): void
    {
        $this->orderGoodDescription = $orderGoodDescription;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getOrderServiceLevel()
    {
        return $this->orderServiceLevel;
    }

    /**
     * @param mixed $orderServiceLevel
     */
    public function setOrderServiceLevel($orderServiceLevel): void
    {
        $this->orderServiceLevel = $orderServiceLevel;
    }

    /**
     * @return mixed
     */
    public function getOrderINCOTerm()
    {
        return $this->orderINCOTerm;
    }

    /**
     * @param mixed $orderINCOTerm
     */
    public function setOrderINCOTerm($orderINCOTerm): void
    {
        $this->orderINCOTerm = $orderINCOTerm;
    }

    /**
     * @return mixed
     */
    public function getAdditionalTerms()
    {
        return $this->additionalTerms;
    }

    /**
     * @param mixed $additionalTerms
     */
    public function setAdditionalTerms($additionalTerms): void
    {
        $this->additionalTerms = $additionalTerms;
    }

    /**
     * @return mixed
     */
    public function getOrderTransMode()
    {
        return $this->orderTransMode;
    }

    /**
     * @param mixed $orderTransMode
     */
    public function setOrderTransMode($orderTransMode): void
    {
        $this->orderTransMode = $orderTransMode;
    }

    /**
     * @return mixed
     */
    public function getOrderCountryOrigin()
    {
        return $this->orderCountryOrigin;
    }

    /**
     * @param mixed $orderCountryOrigin
     */
    public function setOrderCountryOrigin($orderCountryOrigin): void
    {
        $this->orderCountryOrigin = $orderCountryOrigin;
    }
}
