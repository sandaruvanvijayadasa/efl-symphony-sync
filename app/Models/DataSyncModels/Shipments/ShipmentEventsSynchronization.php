<?php


namespace App\Models\DataSyncModels\Shipments;


use Carbon\Carbon;

class ShipmentEventsSynchronization
{

    protected $shippingsId;
    protected $evenType;
    protected $eventDate;
    protected $eventDetail;

    /**
     * @return mixed
     */
    public function getShippingsId()
    {
        return $this->shippingsId;
    }

    /**
     * @param $shippingsId
     */
    public function setShippingsId($shippingsId): void
    {
        $this->shippingsId = $shippingsId;
    }

    /**
     * @return mixed
     */
    public function getEvenType()
    {
        return $this->evenType;
    }

    /**
     * @param mixed $evenType
     */
    public function setEvenType($evenType): void
    {
        $this->evenType = $evenType;
    }

    /**
     * @return mixed
     */
    public function getEventDate(): string
    {
        if ($this->eventDate != '') {
            return Carbon::parse($this->eventDate)->format('Y-m-d H:i:s');
        } else {
            return Carbon::parse('1900-01-01T00:00:00Z')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $eventDate
     */
    public function setEventDate($eventDate): void
    {
        $this->eventDate = $eventDate;
    }

    /**
     * @return mixed
     */
    public function getEventDetail()
    {
        return $this->eventDetail;
    }

    /**
     * @param mixed $eventDetail
     */
    public function setEventDetail($eventDetail): void
    {
        $this->eventDetail = $eventDetail;
    }

}
