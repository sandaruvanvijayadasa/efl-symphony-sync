<?php


namespace App\Models\DataSyncModels\Shipments;


class ShipmentLinesSynchronization
{

    private $poNumber;
    private $lineNumber;
    private $lineDescription;
    private $lineOuterPacks;
    private $lineType;
    private $lineActualWeight;
    private $lineUnitofweight;
    private $lineVolume;
    private $lineUnitofVolume;
    private $lineWidth;
    private $lineHeight;
    private $lineLength;
    private $jlPk;
    private $packJCContainerNum;
    private $packJCSealNum;
    private $packRCCode;

    /**
     * @return mixed
     */
    public function getPoNumber()
    {
        return $this->poNumber;
    }

    /**
     * @param mixed $poNumber
     */
    public function setPoNumber($poNumber): void
    {
        $this->poNumber = $poNumber;
    }

    /**
     * @return mixed
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * @param mixed $lineNumber
     */
    public function setLineNumber($lineNumber): void
    {
        $this->lineNumber = $lineNumber;
    }

    /**
     * @return mixed
     */
    public function getLineDescription()
    {
        return $this->lineDescription;
    }

    /**
     * @param mixed $lineDescription
     */
    public function setLineDescription($lineDescription): void
    {
        $this->lineDescription = $lineDescription;
    }

    /**
     * @return mixed
     */
    public function getLineOuterPacks()
    {
        return $this->lineOuterPacks;
    }

    /**
     * @param mixed $lineOuterPacks
     */
    public function setLineOuterPacks($lineOuterPacks): void
    {
        $this->lineOuterPacks = $lineOuterPacks;
    }

    /**
     * @return mixed
     */
    public function getLineType()
    {
        return $this->lineType;
    }

    /**
     * @param mixed $lineType
     */
    public function setLineType($lineType): void
    {
        $this->lineType = $lineType;
    }

    /**
     * @return mixed
     */
    public function getLineActualWeight()
    {
        return $this->lineActualWeight;
    }

    /**
     * @param mixed $lineActualWeight
     */
    public function setLineActualWeight($lineActualWeight): void
    {
        $this->lineActualWeight = $lineActualWeight;
    }

    /**
     * @return mixed
     */
    public function getLineUnitofweight()
    {
        return $this->lineUnitofweight;
    }

    /**
     * @param mixed $lineUnitofweight
     */
    public function setLineUnitofweight($lineUnitofweight): void
    {
        $this->lineUnitofweight = $lineUnitofweight;
    }

    /**
     * @return mixed
     */
    public function getLineVolume()
    {
        return $this->lineVolume;
    }

    /**
     * @param mixed $lineVolume
     */
    public function setLineVolume($lineVolume): void
    {
        $this->lineVolume = $lineVolume;
    }

    /**
     * @return mixed
     */
    public function getLineUnitofVolume()
    {
        return $this->lineUnitofVolume;
    }

    /**
     * @param mixed $lineUnitofVolume
     */
    public function setLineUnitofVolume($lineUnitofVolume): void
    {
        $this->lineUnitofVolume = $lineUnitofVolume;
    }

    /**
     * @return mixed
     */
    public function getLineWidth()
    {
        return $this->lineWidth;
    }

    /**
     * @param mixed $lineWidth
     */
    public function setLineWidth($lineWidth): void
    {
        $this->lineWidth = $lineWidth;
    }

    /**
     * @return mixed
     */
    public function getLineHeight()
    {
        return $this->lineHeight;
    }

    /**
     * @param mixed $lineHeight
     */
    public function setLineHeight($lineHeight): void
    {
        $this->lineHeight = $lineHeight;
    }

    /**
     * @return mixed
     */
    public function getLineLength()
    {
        return $this->lineLength;
    }

    /**
     * @param mixed $lineLength
     */
    public function setLineLength($lineLength): void
    {
        $this->lineLength = $lineLength;
    }

    /**
     * @return mixed
     */
    public function getJlPk()
    {
        return $this->jlPk;
    }

    /**
     * @param mixed $jlPk
     */
    public function setJlPk($jlPk): void
    {
        $this->jlPk = $jlPk;
    }

    /**
     * @return mixed
     */
    public function getPackJCContainerNum()
    {
        return $this->packJCContainerNum;
    }

    /**
     * @param mixed $packJCContainerNum
     */
    public function setPackJCContainerNum($packJCContainerNum): void
    {
        $this->packJCContainerNum = $packJCContainerNum;
    }

    /**
     * @return mixed
     */
    public function getPackJCSealNum()
    {
        return $this->packJCSealNum;
    }

    /**
     * @param mixed $packJCSealNum
     */
    public function setPackJCSealNum($packJCSealNum): void
    {
        $this->packJCSealNum = $packJCSealNum;
    }

    /**
     * @return mixed
     */
    public function getPackRCCode()
    {
        return $this->packRCCode;
    }

    /**
     * @param mixed $packRCCode
     */
    public function setPackRCCode($packRCCode): void
    {
        $this->packRCCode = $packRCCode;
    }

}
