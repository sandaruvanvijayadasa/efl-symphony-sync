<?php


namespace App\Models\DataSyncModels\Shipments;


use Carbon\Carbon;

class ShipmentLegSynchronization
{

    private $shippingsId;
    private $legOrder1;
    private $voyageFlight1;
    private $vessel1;
    private $legETD1;
    private $legETA1;
    private $legATD1;
    private $legATA1;
    private $leg1load;
    private $leg1disc;
    private $mode1;

    private $legOrder2;
    private $voyageFlight2;
    private $vessel2;
    private $legETD2;
    private $legETA2;
    private $legATD2;
    private $legATA2;
    private $leg2load;
    private $leg2disc;
    private $mode2;

    private $legOrder3;
    private $voyageFlight3;
    private $vessel3;
    private $legETD3;
    private $legETA3;
    private $legATD3;
    private $legATA3;
    private $leg3load;
    private $leg3disc;
    private $mode3;

    private $legOrder4;
    private $voyageFlight4;
    private $vessel4;
    private $legETD4;
    private $legETA4;
    private $legATD4;
    private $legATA4;
    private $leg4load;
    private $leg4disc;
    private $mode4;

    /**
     * @return mixed
     */
    public function getShippingsId()
    {
        return $this->shippingsId;
    }

    /**
     * @param $shippingsId
     */
    public function setShippingsId($shippingsId): void
    {
        $this->shippingsId = $shippingsId;
    }

    /**
     * @return mixed
     */
    public function getLegOrder1()
    {
        return $this->legOrder1;
    }

    /**
     * @param mixed $legOrder1
     */
    public function setLegOrder1($legOrder1): void
    {
        $this->legOrder1 = $legOrder1;
    }

    /**
     * @return mixed
     */
    public function getVoyageFlight1()
    {
        return $this->voyageFlight1;
    }

    /**
     * @param mixed $voyageFlight1
     */
    public function setVoyageFlight1($voyageFlight1): void
    {
        $this->voyageFlight1 = $voyageFlight1;
    }

    /**
     * @return mixed
     */
    public function getVessel1()
    {
        return $this->vessel1;
    }

    /**
     * @param mixed $vessel1
     */
    public function setVessel1($vessel1): void
    {
        $this->vessel1 = $vessel1;
    }

    /**
     * @return mixed
     */
    public function getLegETD1()
    {
        if($this->legETD1 != ""){
            return Carbon::parse($this->legETD1)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legETD1
     */
    public function setLegETD1($legETD1): void
    {
        $this->legETD1 = $legETD1;
    }

    /**
     * @return mixed
     */
    public function getLegETA1()
    {
        if($this->legETA1 != ""){
            return Carbon::parse($this->legETA1)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legETA1
     */
    public function setLegETA1($legETA1): void
    {
        $this->legETA1 = $legETA1;
    }

    /**
     * @return mixed
     */
    public function getLegATD1()
    {
        if($this->legATD1 != ""){
            return Carbon::parse($this->legATD1)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legATD1
     */
    public function setLegATD1($legATD1): void
    {
        $this->legATD1 = $legATD1;
    }

    /**
     * @return mixed
     */
    public function getLegATA1()
    {
        if($this->legATA1 != ""){
            return Carbon::parse($this->legATA1)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legATA1
     */
    public function setLegATA1($legATA1): void
    {
        $this->legATA1 = $legATA1;
    }

    /**
     * @return mixed
     */
    public function getLeg1load()
    {
        return $this->leg1load;
    }

    /**
     * @param mixed $leg1load
     */
    public function setLeg1load($leg1load): void
    {
        $this->leg1load = $leg1load;
    }

    /**
     * @return mixed
     */
    public function getLeg1disc()
    {
        return $this->leg1disc;
    }

    /**
     * @param mixed $leg1disc
     */
    public function setLeg1disc($leg1disc): void
    {
        $this->leg1disc = $leg1disc;
    }

    /**
     * @return mixed
     */
    public function getMode1()
    {
        return $this->mode1;
    }

    /**
     * @param mixed $mode1
     */
    public function setMode1($mode1): void
    {
        $this->mode1 = $mode1;
    }

    /**
     * @return mixed
     */
    public function getLegOrder2()
    {
        return $this->legOrder2;
    }

    /**
     * @param mixed $legOrder2
     */
    public function setLegOrder2($legOrder2): void
    {
        $this->legOrder2 = $legOrder2;
    }

    /**
     * @return mixed
     */
    public function getVoyageFlight2()
    {
        return $this->voyageFlight2;
    }

    /**
     * @param mixed $voyageFlight2
     */
    public function setVoyageFlight2($voyageFlight2): void
    {
        $this->voyageFlight2 = $voyageFlight2;
    }

    /**
     * @return mixed
     */
    public function getVessel2()
    {
        return $this->vessel2;
    }

    /**
     * @param mixed $vessel2
     */
    public function setVessel2($vessel2): void
    {
        $this->vessel2 = $vessel2;
    }

    /**
     * @return mixed
     */
    public function getLegETD2()
    {
        if($this->legETD2 != ""){
            return Carbon::parse($this->legETD2)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legETD2
     */
    public function setLegETD2($legETD2): void
    {
        $this->legETD2 = $legETD2;
    }

    /**
     * @return mixed
     */
    public function getLegETA2()
    {
        if($this->legETA2 != ""){
            return Carbon::parse($this->legETA2)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legETA2
     */
    public function setLegETA2($legETA2): void
    {
        $this->legETA2 = $legETA2;
    }

    /**
     * @return mixed
     */
    public function getLegATD2()
    {
        if($this->legATD2 != ""){
            return Carbon::parse($this->legATD2)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legATD2
     */
    public function setLegATD2($legATD2): void
    {
        $this->legATD2 = $legATD2;
    }

    /**
     * @return mixed
     */
    public function getLegATA2()
    {
        if($this->legATA2 != ""){
            return Carbon::parse($this->legATA2)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legATA2
     */
    public function setLegATA2($legATA2): void
    {
        $this->legATA2 = $legATA2;
    }

    /**
     * @return mixed
     */
    public function getLeg2load()
    {
        return $this->leg2load;
    }

    /**
     * @param mixed $leg2load
     */
    public function setLeg2load($leg2load): void
    {
        $this->leg2load = $leg2load;
    }

    /**
     * @return mixed
     */
    public function getLeg2disc()
    {
        return $this->leg2disc;
    }

    /**
     * @param mixed $leg2disc
     */
    public function setLeg2disc($leg2disc): void
    {
        $this->leg2disc = $leg2disc;
    }

    /**
     * @return mixed
     */
    public function getMode2()
    {
        return $this->mode2;
    }

    /**
     * @param mixed $mode2
     */
    public function setMode2($mode2): void
    {
        $this->mode2 = $mode2;
    }

    /**
     * @return mixed
     */
    public function getLegOrder3()
    {
        return $this->legOrder3;
    }

    /**
     * @param mixed $legOrder3
     */
    public function setLegOrder3($legOrder3): void
    {
        $this->legOrder3 = $legOrder3;
    }

    /**
     * @return mixed
     */
    public function getVoyageFlight3()
    {
        return $this->voyageFlight3;
    }

    /**
     * @param mixed $voyageFlight3
     */
    public function setVoyageFlight3($voyageFlight3): void
    {
        $this->voyageFlight3 = $voyageFlight3;
    }

    /**
     * @return mixed
     */
    public function getVessel3()
    {
        return $this->vessel3;
    }

    /**
     * @param mixed $vessel3
     */
    public function setVessel3($vessel3): void
    {
        $this->vessel3 = $vessel3;
    }

    /**
     * @return mixed
     */
    public function getLegETD3()
    {
        if($this->legETD3 != ""){
            return Carbon::parse($this->legETD3)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legETD3
     */
    public function setLegETD3($legETD3): void
    {
        $this->legETD3 = $legETD3;
    }

    /**
     * @return mixed
     */
    public function getLegETA3()
    {
        if($this->legETA3 != ""){
            return Carbon::parse($this->legETA3)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legETA3
     */
    public function setLegETA3($legETA3): void
    {
        $this->legETA3 = $legETA3;
    }

    /**
     * @return mixed
     */
    public function getLegATD3()
    {
        if($this->legATD3 != ""){
            return Carbon::parse($this->legATD3)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legATD3
     */
    public function setLegATD3($legATD3): void
    {
        $this->legATD3 = $legATD3;
    }

    /**
     * @return mixed
     */
    public function getLegATA3()
    {
        if($this->legATA3 != ""){
            return Carbon::parse($this->legATA3)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legATA3
     */
    public function setLegATA3($legATA3): void
    {
        $this->legATA3 = $legATA3;
    }

    /**
     * @return mixed
     */
    public function getLeg3load()
    {
        return $this->leg3load;
    }

    /**
     * @param mixed $leg3load
     */
    public function setLeg3load($leg3load): void
    {
        $this->leg3load = $leg3load;
    }

    /**
     * @return mixed
     */
    public function getLeg3disc()
    {
        return $this->leg3disc;
    }

    /**
     * @param mixed $leg3disc
     */
    public function setLeg3disc($leg3disc): void
    {
        $this->leg3disc = $leg3disc;
    }

    /**
     * @return mixed
     */
    public function getMode3()
    {
        return $this->mode3;
    }

    /**
     * @param mixed $mode3
     */
    public function setMode3($mode3): void
    {
        $this->mode3 = $mode3;
    }

    /**
     * @return mixed
     */
    public function getLegOrder4()
    {
        return $this->legOrder4;
    }

    /**
     * @param mixed $legOrder4
     */
    public function setLegOrder4($legOrder4): void
    {
        $this->legOrder4 = $legOrder4;
    }

    /**
     * @return mixed
     */
    public function getVoyageFlight4()
    {
        return $this->voyageFlight4;
    }

    /**
     * @param mixed $voyageFlight4
     */
    public function setVoyageFlight4($voyageFlight4): void
    {
        $this->voyageFlight4 = $voyageFlight4;
    }

    /**
     * @return mixed
     */
    public function getVessel4()
    {
        return $this->vessel4;
    }

    /**
     * @param mixed $vessel4
     */
    public function setVessel4($vessel4): void
    {
        $this->vessel4 = $vessel4;
    }

    /**
     * @return mixed
     */
    public function getLegETD4()
    {
        if($this->legETD4 != ""){
            return Carbon::parse($this->legETD4)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legETD4
     */
    public function setLegETD4($legETD4): void
    {
        $this->legETD4 = $legETD4;
    }

    /**
     * @return mixed
     */
    public function getLegETA4()
    {
        if($this->legETA4 != ""){
            return Carbon::parse($this->legETA4)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legETA4
     */
    public function setLegETA4($legETA4): void
    {
        $this->legETA4 = $legETA4;
    }

    /**
     * @return mixed
     */
    public function getLegATD4()
    {
        if($this->legATD4 != ""){
            return Carbon::parse($this->legATD4)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }

    }

    /**
     * @param mixed $legATD4
     */
    public function setLegATD4($legATD4): void
    {
        $this->legATD4 = $legATD4;
    }

    /**
     * @return mixed
     */
    public function getLegATA4()
    {
        if($this->legATA4 != ""){
            return Carbon::parse($this->legATA4)->format('Y-m-d H:i:s');
        }else{
            return Carbon::parse('1900-01-01')->format('Y-m-d H:i:s');
        }
    }

    /**
     * @param mixed $legATA4
     */
    public function setLegATA4($legATA4): void
    {
        $this->legATA4 = $legATA4;
    }

    /**
     * @return mixed
     */
    public function getLeg4load()
    {
        return $this->leg4load;
    }

    /**
     * @param mixed $leg4load
     */
    public function setLeg4load($leg4load): void
    {
        $this->leg4load = $leg4load;
    }

    /**
     * @return mixed
     */
    public function getLeg4disc()
    {
        return $this->leg4disc;
    }

    /**
     * @param mixed $leg4disc
     */
    public function setLeg4disc($leg4disc): void
    {
        $this->leg4disc = $leg4disc;
    }

    /**
     * @return mixed
     */
    public function getMode4()
    {
        return $this->mode4;
    }

    /**
     * @param mixed $mode4
     */
    public function setMode4($mode4): void
    {
        $this->mode4 = $mode4;
    }

}
