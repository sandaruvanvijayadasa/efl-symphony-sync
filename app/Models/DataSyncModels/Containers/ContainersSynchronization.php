<?php


namespace App\Models\DataSyncModels\Containers;


use Carbon\Carbon;

class ContainersSynchronization
{

    protected $shippingsId;
    protected $containerCount;
    protected $containerLineNo;
    protected $containerNum;
    protected $sealNum;
    protected $containerMode;
    protected $storageClass;
    protected $deliveryMode;
    protected $containerType;
    protected $grossWeight;
    protected $tareWeight;
    protected $departureEstimatedPickup;
    protected $emptyRequired;
    protected $containerYardEmptyPickupGateOut;
    protected $departureCartageAdvised;
    protected $departureCartageComplete;
    protected $arrivalSlotDateTime;
    protected $arrivalEstimatedDelivery;
    protected $arrivalDeliveryRequiredBy;
    protected $arrivalCartageAdvised;
    protected $arrivalCartageComplete;
    protected $fclWharfGateIn;
    protected $fclOnBoardVessel;
    protected $fclUnloadFromVessel;
    protected $fclAvailable;
    protected $fclWharfGateOut;
    protected $arrivalCTOStorageStartDate;
    protected $lclUnpack;
    protected $lclAvailable;
    protected $emptyReadyForReturn;
    protected $emptyReturnedBy;
    protected $containerYardEmptyReturnGateIn;

    /**
     * @return mixed
     */
    public function getShippingsId()
    {
        return $this->shippingsId;
    }

    /**
     * @param mixed $shippingsId
     */
    public function setShippingsId($shippingsId): void
    {
        $this->shippingsId = $shippingsId;
    }

    /**
     * @return mixed
     */
    public function getContainerCount()
    {
        return $this->containerCount;
    }

    /**
     * @param mixed $containerCount
     */
    public function setContainerCount($containerCount): void
    {
        $this->containerCount = $containerCount;
    }

    /**
     * @return mixed
     */
    public function getContainerLineNo()
    {
        return $this->containerLineNo;
    }

    /**
     * @param mixed $containerLineNo
     */
    public function setContainerLineNo($containerLineNo): void
    {
        $this->containerLineNo = $containerLineNo;
    }

    /**
     * @return mixed
     */
    public function getContainerNum()
    {
        return $this->containerNum;
    }

    /**
     * @param mixed $containerNum
     */
    public function setContainerNum($containerNum): void
    {
        $this->containerNum = $containerNum;
    }

    /**
     * @return mixed
     */
    public function getSealNum()
    {
        return $this->sealNum;
    }

    /**
     * @param mixed $sealNum
     */
    public function setSealNum($sealNum): void
    {
        $this->sealNum = $sealNum;
    }

    /**
     * @return mixed
     */
    public function getContainerMode()
    {
        return $this->containerMode;
    }

    /**
     * @param mixed $containerMode
     */
    public function setContainerMode($containerMode): void
    {
        $this->containerMode = $containerMode;
    }

    /**
     * @return mixed
     */
    public function getStorageClass()
    {
        return $this->storageClass;
    }

    /**
     * @param mixed $storageClass
     */
    public function setStorageClass($storageClass): void
    {
        $this->storageClass = $storageClass;
    }

    /**
     * @return mixed
     */
    public function getDeliveryMode()
    {
        return $this->deliveryMode;
    }

    /**
     * @param mixed $deliveryMode
     */
    public function setDeliveryMode($deliveryMode): void
    {
        $this->deliveryMode = $deliveryMode;
    }

    /**
     * @return mixed
     */
    public function getContainerType()
    {
        return $this->containerType;
    }

    /**
     * @param mixed $containerType
     */
    public function setContainerType($containerType): void
    {
        $this->containerType = $containerType;
    }

    /**
     * @return mixed
     */
    public function getGrossWeight()
    {
        return $this->grossWeight;
    }

    /**
     * @param mixed $grossWeight
     */
    public function setGrossWeight($grossWeight): void
    {
        $this->grossWeight = $grossWeight;
    }

    /**
     * @return mixed
     */
    public function getTareWeight()
    {
        return $this->tareWeight;
    }

    /**
     * @param mixed $tareWeight
     */
    public function setTareWeight($tareWeight): void
    {
        $this->tareWeight = $tareWeight;
    }

    /**
     * @return mixed
     */
    public function getDepartureEstimatedPickup()
    {
        return Carbon::parse($this->departureEstimatedPickup)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $departureEstimatedPickup
     */
    public function setDepartureEstimatedPickup($departureEstimatedPickup): void
    {
        $this->departureEstimatedPickup = $departureEstimatedPickup;
    }

    /**
     * @return mixed
     */
    public function getEmptyRequired()
    {
        return Carbon::parse($this->emptyRequired)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $emptyRequired
     */
    public function setEmptyRequired($emptyRequired): void
    {
        $this->emptyRequired = $emptyRequired;
    }

    /**
     * @return mixed
     */
    public function getContainerYardEmptyPickupGateOut()
    {
        return Carbon::parse($this->containerYardEmptyPickupGateOut)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $containerYardEmptyPickupGateOut
     */
    public function setContainerYardEmptyPickupGateOut($containerYardEmptyPickupGateOut): void
    {
        $this->containerYardEmptyPickupGateOut = $containerYardEmptyPickupGateOut;
    }

    /**
     * @return mixed
     */
    public function getDepartureCartageAdvised()
    {
        return Carbon::parse($this->departureCartageAdvised)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $departureCartageAdvised
     */
    public function setDepartureCartageAdvised($departureCartageAdvised): void
    {
        $this->departureCartageAdvised = $departureCartageAdvised;
    }

    /**
     * @return mixed
     */
    public function getDepartureCartageComplete()
    {
        return Carbon::parse($this->departureCartageComplete)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $departureCartageComplete
     */
    public function setDepartureCartageComplete($departureCartageComplete): void
    {
        $this->departureCartageComplete = $departureCartageComplete;
    }

    /**
     * @return mixed
     */
    public function getArrivalSlotDateTime()
    {
        return Carbon::parse($this->arrivalSlotDateTime)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $arrivalSlotDateTime
     */
    public function setArrivalSlotDateTime($arrivalSlotDateTime): void
    {
        $this->arrivalSlotDateTime = $arrivalSlotDateTime;
    }

    /**
     * @return mixed
     */
    public function getArrivalEstimatedDelivery()
    {
        return Carbon::parse($this->arrivalEstimatedDelivery)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $arrivalEstimatedDelivery
     */
    public function setArrivalEstimatedDelivery($arrivalEstimatedDelivery): void
    {
        $this->arrivalEstimatedDelivery = $arrivalEstimatedDelivery;
    }

    /**
     * @return mixed
     */
    public function getArrivalDeliveryRequiredBy()
    {
        return Carbon::parse($this->arrivalDeliveryRequiredBy)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $arrivalDeliveryRequiredBy
     */
    public function setArrivalDeliveryRequiredBy($arrivalDeliveryRequiredBy): void
    {
        $this->arrivalDeliveryRequiredBy = $arrivalDeliveryRequiredBy;
    }

    /**
     * @return mixed
     */
    public function getArrivalCartageAdvised()
    {
        return Carbon::parse($this->arrivalCartageAdvised)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $arrivalCartageAdvised
     */
    public function setArrivalCartageAdvised($arrivalCartageAdvised): void
    {
        $this->arrivalCartageAdvised = $arrivalCartageAdvised;
    }

    /**
     * @return mixed
     */
    public function getArrivalCartageComplete()
    {
        return Carbon::parse($this->arrivalCartageComplete)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $arrivalCartageComplete
     */
    public function setArrivalCartageComplete($arrivalCartageComplete): void
    {
        $this->arrivalCartageComplete = $arrivalCartageComplete;
    }

    /**
     * @return mixed
     */
    public function getFclWharfGateIn()
    {
        return Carbon::parse($this->fclWharfGateIn)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $fclWharfGateIn
     */
    public function setFclWharfGateIn($fclWharfGateIn): void
    {
        $this->fclWharfGateIn = $fclWharfGateIn;
    }

    /**
     * @return mixed
     */
    public function getFclOnBoardVessel()
    {
        return Carbon::parse($this->fclOnBoardVessel)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $fclOnBoardVessel
     */
    public function setFclOnBoardVessel($fclOnBoardVessel): void
    {
        $this->fclOnBoardVessel = $fclOnBoardVessel;
    }

    /**
     * @return mixed
     */
    public function getFclUnloadFromVessel()
    {
        return Carbon::parse($this->fclUnloadFromVessel)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $fclUnloadFromVessel
     */
    public function setFclUnloadFromVessel($fclUnloadFromVessel): void
    {
        $this->fclUnloadFromVessel = $fclUnloadFromVessel;
    }

    /**
     * @return mixed
     */
    public function getFclAvailable()
    {
        return Carbon::parse($this->fclAvailable)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $fclAvailable
     */
    public function setFclAvailable($fclAvailable): void
    {
        $this->fclAvailable = $fclAvailable;
    }

    /**
     * @return mixed
     */
    public function getFclWharfGateOut()
    {
        return Carbon::parse($this->fclWharfGateOut)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $fclWharfGateOut
     */
    public function setFclWharfGateOut($fclWharfGateOut): void
    {
        $this->fclWharfGateOut = $fclWharfGateOut;
    }

    /**
     * @return mixed
     */
    public function getArrivalCTOStorageStartDate()
    {
        return Carbon::parse($this->arrivalCTOStorageStartDate)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $arrivalCTOStorageStartDate
     */
    public function setArrivalCTOStorageStartDate($arrivalCTOStorageStartDate): void
    {
        $this->arrivalCTOStorageStartDate = $arrivalCTOStorageStartDate;
    }

    /**
     * @return mixed
     */
    public function getLclUnpack()
    {
        return Carbon::parse($this->lclUnpack)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $lclUnpack
     */
    public function setLclUnpack($lclUnpack): void
    {
        $this->lclUnpack = $lclUnpack;
    }

    /**
     * @return mixed
     */
    public function getLclAvailable()
    {
        return Carbon::parse($this->lclAvailable)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $lclAvailable
     */
    public function setLclAvailable($lclAvailable): void
    {
        $this->lclAvailable = $lclAvailable;
    }

    /**
     * @return mixed
     */
    public function getEmptyReadyForReturn()
    {
        return Carbon::parse($this->emptyReadyForReturn)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $emptyReadyForReturn
     */
    public function setEmptyReadyForReturn($emptyReadyForReturn): void
    {
        $this->emptyReadyForReturn = $emptyReadyForReturn;
    }

    /**
     * @return mixed
     */
    public function getEmptyReturnedBy()
    {
        return Carbon::parse($this->emptyReturnedBy)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $emptyReturnedBy
     */
    public function setEmptyReturnedBy($emptyReturnedBy): void
    {
        $this->emptyReturnedBy = $emptyReturnedBy;
    }

    /**
     * @return mixed
     */
    public function getContainerYardEmptyReturnGateIn()
    {
        return Carbon::parse($this->containerYardEmptyReturnGateIn)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $containerYardEmptyReturnGateIn
     */
    public function setContainerYardEmptyReturnGateIn($containerYardEmptyReturnGateIn): void
    {
        $this->containerYardEmptyReturnGateIn = $containerYardEmptyReturnGateIn;
    }



}
