<?php


namespace App\Models\DataSyncModels\Customers;


use Carbon\Carbon;

class CustomersSynchronization
{

    protected $orgPrimaryKey;
    protected $orgCode;
    protected $orgName;
    protected $orgIsActive;
    protected $orgIsConsignee;
    protected $orgIsConsignor;
    protected $orgIsForwarder;
    protected $orgIsAgentWise;
    protected $orgAddressLine1;
    protected $orgAddressLine2;
    protected $orgCity;
    protected $orgPostalCode;
    protected $orgUnloco;
    protected $orgState;
    protected $orgPhone;
    protected $orgMobile;
    protected $orgFax;
    protected $orgEmail;
    protected $orgWeb;
    protected $orgLanguage;
    protected $orgCountryCode;
    protected $orgCountryName;
    protected $orgRegistrationNumber;
    protected $orgSalePerson;
    protected $orgSalePersonEmail;
    protected $orgSalePersonMobile;
    protected $orgAccountPerson;
    protected $orgAccountEmail;
    protected $orgAccountMobile;
    protected $orgLogo;
    protected $orgLastEditDateTimeUTC;
    protected $orgCustomerFlag;
    protected $orgEflFlag;
    protected $orgCwFlag;
    protected $orgStatus;

    /**
     * @return mixed
     */
    public function getOrgPrimaryKey()
    {
        return $this->orgPrimaryKey;
    }

    /**
     * @param mixed $orgPrimaryKey
     */
    public function setOrgPrimaryKey($orgPrimaryKey): void
    {
        $this->orgPrimaryKey = $orgPrimaryKey;
    }

    /**
     * @return mixed
     */
    public function getOrgCode()
    {
        return $this->orgCode;
    }

    /**
     * @param mixed $orgCode
     */
    public function setOrgCode($orgCode): void
    {
        $this->orgCode = $orgCode;
    }

    /**
     * @return mixed
     */
    public function getOrgName()
    {
        return $this->orgName;
    }

    /**
     * @param mixed $orgName
     */
    public function setOrgName($orgName): void
    {
        $this->orgName = $orgName;
    }

    /**
     * @return mixed
     */
    public function getOrgIsActive()
    {
        return $this->orgIsActive;
    }

    /**
     * @param mixed $orgIsActive
     */
    public function setOrgIsActive($orgIsActive): void
    {
        $this->orgIsActive = $orgIsActive;
    }

    /**
     * @return mixed
     */
    public function getOrgIsConsignee()
    {
        return $this->orgIsConsignee;
    }

    /**
     * @param mixed $orgIsConsignee
     */
    public function setOrgIsConsignee($orgIsConsignee): void
    {
        $this->orgIsConsignee = $orgIsConsignee;
    }

    /**
     * @return mixed
     */
    public function getOrgIsConsignor()
    {
        return $this->orgIsConsignor;
    }

    /**
     * @param mixed $orgIsConsignor
     */
    public function setOrgIsConsignor($orgIsConsignor): void
    {
        $this->orgIsConsignor = $orgIsConsignor;
    }

    /**
     * @return mixed
     */
    public function getOrgIsForwarder()
    {
        return $this->orgIsForwarder;
    }

    /**
     * @param mixed $orgIsForwarder
     */
    public function setOrgIsForwarder($orgIsForwarder): void
    {
        $this->orgIsForwarder = $orgIsForwarder;
    }

    /**
     * @return mixed
     */
    public function getOrgIsAgentWise()
    {
        return $this->orgIsAgentWise;
    }

    /**
     * @param mixed $orgIsAgentWise
     */
    public function setOrgIsAgentWise($orgIsAgentWise): void
    {
        $this->orgIsAgentWise = $orgIsAgentWise;
    }

    /**
     * @return mixed
     */
    public function getOrgAddressLine1()
    {
        return $this->orgAddressLine1;
    }

    /**
     * @param mixed $orgAddressLine1
     */
    public function setOrgAddressLine1($orgAddressLine1): void
    {
        $this->orgAddressLine1 = $orgAddressLine1;
    }

    /**
     * @return mixed
     */
    public function getOrgAddressLine2()
    {
        return $this->orgAddressLine2;
    }

    /**
     * @param mixed $orgAddressLine2
     */
    public function setOrgAddressLine2($orgAddressLine2): void
    {
        $this->orgAddressLine2 = $orgAddressLine2;
    }

    /**
     * @return mixed
     */
    public function getOrgCity()
    {
        return $this->orgCity;
    }

    /**
     * @param mixed $orgCity
     */
    public function setOrgCity($orgCity): void
    {
        $this->orgCity = $orgCity;
    }

    /**
     * @return mixed
     */
    public function getOrgPostalCode()
    {
        return $this->orgPostalCode;
    }

    /**
     * @param mixed $orgPostalCode
     */
    public function setOrgPostalCode($orgPostalCode): void
    {
        $this->orgPostalCode = $orgPostalCode;
    }

    /**
     * @return mixed
     */
    public function getOrgUnloco()
    {
        return $this->orgUnloco;
    }

    /**
     * @param mixed $orgUnloco
     */
    public function setOrgUnloco($orgUnloco): void
    {
        $this->orgUnloco = $orgUnloco;
    }

    /**
     * @return mixed
     */
    public function getOrgState()
    {
        return $this->orgState;
    }

    /**
     * @param mixed $orgState
     */
    public function setOrgState($orgState): void
    {
        $this->orgState = $orgState;
    }

    /**
     * @return mixed
     */
    public function getOrgPhone()
    {
        return $this->orgPhone;
    }

    /**
     * @param mixed $orgPhone
     */
    public function setOrgPhone($orgPhone): void
    {
        $this->orgPhone = $orgPhone;
    }

    /**
     * @return mixed
     */
    public function getOrgMobile()
    {
        return $this->orgMobile;
    }

    /**
     * @param mixed $orgMobile
     */
    public function setOrgMobile($orgMobile): void
    {
        $this->orgMobile = $orgMobile;
    }

    /**
     * @return mixed
     */
    public function getOrgFax()
    {
        return $this->orgFax;
    }

    /**
     * @param mixed $orgFax
     */
    public function setOrgFax($orgFax): void
    {
        $this->orgFax = $orgFax;
    }

    /**
     * @return mixed
     */
    public function getOrgEmail()
    {
        return $this->orgEmail;
    }

    /**
     * @param mixed $orgEmail
     */
    public function setOrgEmail($orgEmail): void
    {
        $this->orgEmail = $orgEmail;
    }

    /**
     * @return mixed
     */
    public function getOrgWeb()
    {
        return $this->orgWeb;
    }

    /**
     * @param mixed $orgWeb
     */
    public function setOrgWeb($orgWeb): void
    {
        $this->orgWeb = $orgWeb;
    }

    /**
     * @return mixed
     */
    public function getOrgLanguage()
    {
        return $this->orgLanguage;
    }

    /**
     * @param mixed $orgLanguage
     */
    public function setOrgLanguage($orgLanguage): void
    {
        $this->orgLanguage = $orgLanguage;
    }

    /**
     * @return mixed
     */
    public function getOrgCountryCode()
    {
        return $this->orgCountryCode;
    }

    /**
     * @param mixed $orgCountryCode
     */
    public function setOrgCountryCode($orgCountryCode): void
    {
        $this->orgCountryCode = $orgCountryCode;
    }

    /**
     * @return mixed
     */
    public function getOrgCountryName()
    {
        return $this->orgCountryName;
    }

    /**
     * @param mixed $orgCountryName
     */
    public function setOrgCountryName($orgCountryName): void
    {
        $this->orgCountryName = $orgCountryName;
    }

    /**
     * @return mixed
     */
    public function getOrgRegistrationNumber()
    {
        return $this->orgRegistrationNumber;
    }

    /**
     * @param mixed $orgRegistrationNumber
     */
    public function setOrgRegistrationNumber($orgRegistrationNumber): void
    {
        $this->orgRegistrationNumber = $orgRegistrationNumber;
    }

    /**
     * @return mixed
     */
    public function getOrgSalePerson()
    {
        return $this->orgSalePerson;
    }

    /**
     * @param mixed $orgSalePerson
     */
    public function setOrgSalePerson($orgSalePerson): void
    {
        $this->orgSalePerson = $orgSalePerson;
    }

    /**
     * @return mixed
     */
    public function getOrgSalePersonEmail()
    {
        return $this->orgSalePersonEmail;
    }

    /**
     * @param mixed $orgSalePersonEmail
     */
    public function setOrgSalePersonEmail($orgSalePersonEmail): void
    {
        $this->orgSalePersonEmail = $orgSalePersonEmail;
    }

    /**
     * @return mixed
     */
    public function getOrgSalePersonMobile()
    {
        return $this->orgSalePersonMobile;
    }

    /**
     * @param mixed $orgSalePersonMobile
     */
    public function setOrgSalePersonMobile($orgSalePersonMobile): void
    {
        $this->orgSalePersonMobile = $orgSalePersonMobile;
    }

    /**
     * @return mixed
     */
    public function getOrgAccountPerson()
    {
        return $this->orgAccountPerson;
    }

    /**
     * @param mixed $orgAccountPerson
     */
    public function setOrgAccountPerson($orgAccountPerson): void
    {
        $this->orgAccountPerson = $orgAccountPerson;
    }

    /**
     * @return mixed
     */
    public function getOrgAccountEmail()
    {
        return $this->orgAccountEmail;
    }

    /**
     * @param mixed $orgAccountEmail
     */
    public function setOrgAccountEmail($orgAccountEmail): void
    {
        $this->orgAccountEmail = $orgAccountEmail;
    }

    /**
     * @return mixed
     */
    public function getOrgAccountMobile()
    {
        return $this->orgAccountMobile;
    }

    /**
     * @param mixed $orgAccountMobile
     */
    public function setOrgAccountMobile($orgAccountMobile): void
    {
        $this->orgAccountMobile = $orgAccountMobile;
    }

    /**
     * @return mixed
     */
    public function getOrgLogo()
    {
        return $this->orgLogo;
    }

    /**
     * @param mixed $orgLogo
     */
    public function setOrgLogo($orgLogo): void
    {
        $this->orgLogo = $orgLogo;
    }

    /**
     * @return mixed
     */
    public function getOrgLastEditDateTimeUTC()
    {
        return Carbon::parse($this->orgLastEditDateTimeUTC)->format('Y-m-d H:i:s');
    }

    /**
     * @param mixed $orgLastEditDateTimeUTC
     */
    public function setOrgLastEditDateTimeUTC($orgLastEditDateTimeUTC): void
    {
        $this->orgLastEditDateTimeUTC = $orgLastEditDateTimeUTC;
    }

    /**
     * @return mixed
     */
    public function getOrgCustomerFlag()
    {
        return $this->orgCustomerFlag;
    }

    /**
     * @param mixed $orgCustomerFlag
     */
    public function setOrgCustomerFlag($orgCustomerFlag): void
    {
        $this->orgCustomerFlag = $orgCustomerFlag;
    }

    /**
     * @return mixed
     */
    public function getOrgEflFlag()
    {
        return $this->orgEflFlag;
    }

    /**
     * @param mixed $orgEflFlag
     */
    public function setOrgEflFlag($orgEflFlag): void
    {
        $this->orgEflFlag = $orgEflFlag;
    }

    /**
     * @return mixed
     */
    public function getOrgCwFlag()
    {
        return $this->orgCwFlag;
    }

    /**
     * @param mixed $orgCwFlag
     */
    public function setOrgCwFlag($orgCwFlag): void
    {
        $this->orgCwFlag = $orgCwFlag;
    }

    /**
     * @return mixed
     */
    public function getOrgStatus()
    {
        return $this->orgStatus;
    }

    /**
     * @param mixed $orgStatus
     */
    public function setOrgStatus($orgStatus): void
    {
        $this->orgStatus = $orgStatus;
    }


}
