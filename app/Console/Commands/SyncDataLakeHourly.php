<?php

namespace App\Console\Commands;

use App\Models\Customers\Customers;
use App\Models\Customers\CustomerUsers;
use App\Repository\SyncFromSQLDbRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncDataLakeHourly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve data from sql data lake by hourly.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $syncFromSQLDbRepository = new SyncFromSQLDbRepository();
        //get consignee codes
        $consignees = $syncFromSQLDbRepository->getActiveConsignees();
        //set from date
        $fromDate = Carbon::now()->subHours(6)->format('Y-m-d H:i:s');
        //retrieve data
        $syncFromSQLDbRepository->retrieveShipments($consignees, $fromDate);
    }
}
