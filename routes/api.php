<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//retrieve & sync data into mysql db
Route::get('data-sync', 'DataSynchronizationController@syncDataFromSQLDb')->name('data-sync');
//retrieve container data by given shipment no
Route::get('container-sync', 'DataSynchronizationController@syncContainerDataFromSQLDb')->name('container-sync');
//retrieve events data by given shipment no
Route::get('event-sync', 'DataSynchronizationController@syncEventsDataFromSQLDb')->name('event-sync');
//old header data query (return single shipment header details)
Route::get('shipment-sync', 'DataSynchronizationController@syncSingleShipment')->name('shipment-sync');
//new header data query
Route::get('get-single-shipment', 'DataSynchronizationController@getSingleShipment')->name('get-single-shipment');

//adding new query
Route::get('data-sync-provider', 'DataSynchronizationController@syncDataFromSQLDbToSymphony')->name('data-sync-provider');
Route::get('mock-test', 'DataSynchronizationController@mockTest')->name('mock-test');

//retrieve all shipments & bookings by given consignee & date range
Route::get('get-all-shipments', 'DataSynchronizationController@getAllShipmentByConsignee')->name('get-all-shipments');
//retrieve all shipments & bookings by given date range
Route::get('get-all-shipments-by-date-range', 'DataSynchronizationController@getAllShipmentByDateRange')->name('get-all-shipments-by-date-range');
//retrieve all shipments & bookings by given multiple consignees
Route::get('get-all-shipments-by-multiple-consignees', 'DataSynchronizationController@getAllShipmentsByGivenArrayOfConsignees')->name('get-all-shipments-by-multiple-consignees');
