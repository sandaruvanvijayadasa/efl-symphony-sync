<?php

use App\Models\Booking\BookingPurchaseOrders;
use App\Models\Booking\BookingShippingsSyncs;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('data-sync', 'DataSynchronizationController@syncDataFromSQLDb')->name('data-sync');
Route::get('container-sync', 'DataSynchronizationController@syncContainerDataFromSQLDb')->name('container-sync');
Route::get('event-sync', 'DataSynchronizationController@syncEventsDataFromSQLDb')->name('event-sync');
Route::get('shipment-sync', 'DataSynchronizationController@syncSingleShipment')->name('shipment-sync');
Route::get('get-single-shipment', 'DataSynchronizationController@getSingleShipment')->name('get-single-shipment');
